%% ==============
\chapter{Heuristics for the Wind Farm Cable Layout Problem}
\label{ch:solutions-cablelayout}
%% ==============

In \autoref{sec:cablelayoutproblem}, we introduced the Wind Farm Cable Layout Problem. We proposed an MILP which we implemented using a generic solver for our experiments. However, in this chapter we propose an alternative algorithm solving the layout problem heuristically. The first approach is based on Simulated Annealing, which we describe in the first section. The algorithm is then improved in the following sections.


\section{An Algorithm based on Simulated Annealing}

Our first algorithm which heuristically solves the \emph{Wind Farm Cable Layout Problem} efficiently is based on Simulated Annealing, a randomized search space exploration heuristic introduced in \autoref{sec:preliminaries:simulatedannealing}. The algorithm uses an indirect representation of a solution candidate, which is altered over time using a mutation scheme.

This section is structured as follows. First, the algorithm is described in details. We then evaluate it for randomly generated instances. The strengths and limits of the algorithm are discussed, before we try to fix this in the next section by an improved version.

The introduction of Simulated Annealing in \autoref{sec:preliminaries:simulatedannealing} mentions the ``building blocks'', which need to be defined to construct a complete heuristic for a concrete problem. This subsection presents the representation of the cabling and functions needed for initialization and mutation of this representation. Evaluating it leads to the cabling cost, which is to be minimized by the Simulated Annealing heuristic. 


\paragraph{The Representation}

Recall that the representation shall describe a solution candidate indirectly. A solution to the Wind Farm Cable Layout Problem assigns each edge in the input graph a cable type. 

Our representation is composed of two structures: a \emph{potential field} and \emph{edge cuts}. The potential field\footnote{not to be confused with the potential field in electrical physics} $p : V \rightarrow \indices n$ assigns each node $v \in V$ in the graph a number $p(v)$, its potential. Recall that $\indices n = \{1, \dots, n\}$, where $n$ is the number of nodes. Each node has a different value, so $p$ is a permutation of the node indices. Its concrete role becomes clear when the evaluation function is explained below. The edge cuts $H \subset E$ are the edges which shall not be used for cabling. Together, they form the representation as a tuple $R = (p,H)$.



\paragraph{The Evaluation Function}

Evaluating a representation $R$ requires decoding it into a solution, here the choices of cable types for each edge. Then, the cost of the solution is merely a task of summing up the cable costs, which is trivial in this case.

\begin{figure}
	\todo{Grafik mit den 4 Darstellungsformen: eigentliche Repräsentation, Pfade, Throughputs, Kabeltypen}
	\caption{The intermediate representations used to evaluate our indirect representation}
	\label{fig:intermediate-representations}
\end{figure}


To simplify the explanation of how a representation, given the potential field and edge cuts, is decoded into cable type choices, we introduce two intermediate representations: the \emph{turbine path representation} and the \emph{cable throughput representation}. Their correlation is visualized in \autoref{fig:intermediate-representations}. The cable throughput representation specifies for each edge the direction and amount of power routed through. From this, it is trivial to determine the corresponding cable types by choosing the most inexpensive cable still supporting the requested throughput.

The turbine path representation specifies for each turbine, how its produced power travels through the network, finally reaching a substation. Every turbine has its own path encoded in this intermediate representation.

The final, actual representation used in the algorithm, as explained above, contains the potential field $p$ and the edge cuts $H$. Using that, \autoref{algo:evaluation} computes a path $\pi$ for every turbine separately, which we explain in detail. Starting from the turbine $v$, the path is created incrementally node by node. The process ends when a substation was reached (\autoref{algo:evaluation:loop}). At each step, a restricted neighborhood $N'(x)$ of the current node $x$ is considered, from which we select the node maximizing the ratio of potential difference over distance (\autoref{algo:evaluation:max}). Think of this as a ``slope'' in a height field. The restricted neighborhood, from which the best node is chosen, are the neighbors of $x$ of which the edge is not in the edge cut set and which is not already part of the path (\autoref{algo:evaluation:neigh}). When the restricted neighborhood is empty (\autoref{algo:evaluation:empty}), the path can not be terminated at a substation, and the whole representation is considered invalid. This can be handled by setting its energy to $\infty$.

\begin{algorithm}
	\caption{Constructing a turbine path from the representation}
	\label{algo:evaluation}
	
	\SetFuncSty{textsc}
	\DontPrintSemicolon
	\KwIn{Graph $G = (V,E)$, Node distances $d_{vw}$, Capacities $\varccap vwi$, Costs $\varccap uvi$, Representation $R = (p,H)$, Turbine $v$}
	\KwOut{Path $\pi$ of nodes from $v$ to a substation}

	$\pi \gets$ new list of nodes\;
	$x \gets v$\;
	append $x$ to $\pi$\;
	\While {$x$ is not a substation} { \label{algo:evaluation:loop}
		$N'(x) \gets \{w \in V \;|\; (x,w) \in E \setminus H, \; w \notin \pi\}$ \label{algo:evaluation:neigh}\;
		\If {$N'(x) = \emptyset$} { \label{algo:evaluation:empty}
			\Return infeasible\;
		}
		$x \gets {\arg\max}_{w \in N'(x)} \frac{P(w)-P(x)}{d_{xw}}$ \label{algo:evaluation:max}\;
		append $x$ to $\pi$\;
	}
	\Return $\pi$\;
\end{algorithm}


\paragraph{The Initialization Function}

We create an initial potential field $p$ by randomly shuffling the integers $\{1, \dots, n\}$. The initial edge cut list $H$ is empty.


\paragraph{The Mutation Function}

When mutating a solution, we randomly choose one of the following different kinds of operations. Based on the representation $R=(p,H)$, they form a new representation $R'=(p',H')$.

\begin{enumerate}
	\item Modifying the potential field $p$ (and keeping the edge cuts $H':=H$): This operation is further divided into two kinds of potential field modifications. For both, we first choose two nodes $a,b\in V$ randomly and then
	\begin{enumerate}
		\item either \emph{swap} their potential value
		\[ p'(a) := p(b), \; p'(b) := p(a) \]
		\item or \emph{cycle} the potentials between $a$ and $b$, which is defined as follows: Without loss of generality, we assume $p(a) < p(b)$. We list the nodes having a potential between $p(a)$ and $p(b)$, in ascending order of $p(\cdot)$. We denote this list of nodes as $(v_1, \dots, v_k)$, while satisfying $p(v_i) < p(v_j)$ for $i<j$. Note that $v_1 = a$ and $v_k = b$.
		Then we rotate the potentials of these nodes in either direction. The direction of rotation is again chosen randomly: in one case, we set
		\[ p'(v_i) := p(v_{i+1}), \; p'(b) := p(a), \]
		and in the other case, we set
		\[ p'(v_i) := p(v_{i-1}), \; p'(a) := p(b). \]
		This moves up or lowers down the potential of one of the two chosen nodes to the level of the other, while manipulating the potentials of the rest of the nodes only slightly.
	\end{enumerate}
	\item Modifying the edge cuts $H$ (and keeping the potential field $p':=p$): Add or remove a random edge to the set of edge cuts. The probability to add a new edge cut (instead of removing one) depends on the current cardinality of $H$ (in order to probabilistically level out the number of cuts to some predefined number, for example $\bigO(\sqrt{n})$ with a tunable factor).
\end{enumerate}




\section{Improving the Algorithm}

So far, our algorithm only handles one solution at a time. It is mutated and evaluated iteratively, resulting in a single search exploration. The main problem with such iterative explorations is escaping a local (non-global) optimum, when a worse solution needs to be accepted temporarily. Although Simulated Annealing is designed to make this possible, our experiments showed that the search exploration gets caught in a different local optimum every time the algorithm is ran with a different random seed.


\paragraph{Maintaining Multiple Instances}

This led us to the idea to run multiple instances of the same algorithm in parallel, each with a different randomness. They explore the search space independently, and when one instance gets caught in a local optimum, others are not affected by that. We call the instances of the Simulated Annealing algorithm \emph{threads}\footnote{Not to be confused with the term in parallel computing.}. Note that every thread maintains its very own temperature, and they do not exchange any information.

We propose the following meta algorithm managing the computation threads. It can \emph{kill} threads or \emph{spawn} new ones. It can query the currently best solution and its energy from each thread. Initially it spawns a predefined (configurable) number of threads. Based on their ``best energy'' over time, our meta algorithm decides when a thread is likely to be caught in a local optimum, in which case it is killed.

In Simulated Annealing, when the probability of accepting a worse solution is too high, local optima can be escaped easily but at the same time, the global optimum (in the case it is currently being explored) can be escaped equally easily. This motivated us to add a mechanism saving a solution when it seems to be a good candidate for the global optimum, or one near it. For this, the meta algorithm can \emph{clone} a solution, whenever a thread is about to accept a worse one. Depending on the energy difference between the current and the proposed worse solution, the meta algorithm decides whether or not to clone the thread such that one copy explores the worse solution as usual, while the other copy stays at the current solution candidate, which is mutated differently in its next iteration.

\todo{Pseudo-Code?}


\paragraph{Crossing two Solutions}

In our experiments, we also made the observation, that different threads explore the search space at very different local optima. The solutions all have similar energy levels, but propose very different cable layouts, even the turbines are often assigned to different substations when comparing any two solutions.

This led us to the idea to \emph{cross} two solutions, that is, creating a whole new solution based on information from the both. This approach is similar to how biological evolution works, and is known as an \emph{Evolutionary Algorithm}. We maintain a set of solutions with low energy, from which over time we pick a pair with good compatibility but high diversity. The crossing of these two hopefully results in an even better solution. In our case, we allow the Simulated Annealing algorithm to perform a couple of mutations on the crossing, before we consider it \emph{mature}. (This is similar to how a weak newborn is protected by its parents until it is mature enough to live on its own.) Only mature threads are allowed to be killed by the meta algorithm. For this approach, we need to define three things: the pairwise compatibility and diversity of two solutions as well as their crossing.

The \emph{diversity} of two solutions is simply given by the sum of the absolute difference in the power ratings over all edges.

To evaluate the \emph{compatibility} of two solutions, we decided to look at how their substation assignments differ. For this, we introduce the \emph{substation assignment difference graph} $G_\textrm{sad} = (V_S, E_S, w)$, which is an edge-weighted undirected and complete graph over the substation nodes ($E_S := V_S \times V_S$). Its weight function $w : E_S \rightarrow \wholesgeq$ assigns each edge $e = \{a, b\} \in E_S$ the number of turbines which are assigned to substation $a$ in one solution, and to substation $b$ in the other. Then, we select the two substations $s, t \in V_S$ minimizing the value of the min-cut $(G_\textrm{sad}, s, t)$. \todo{$\rightarrow$ Preliminaries?}

The partitions resulting from the cut are then reused for crossing two solutions. \todo{Genaue Zusammensetzung der neuen Lösung noch nicht klar.}



\section{Improvement using a Two-Level Approach}

\subsection{Splitting the Problem in two Levels}

\subsection{Solving the Substation Assignment Problem}


