%% ==============
\chapter{Optimization Problems in a Wind Farm}
\label{ch:problems}
%% ==============


In this section, we describe the problems we are solving in the rest of this work. First, we introduce a cabling problem for a wind farm. We consider the special case in which the farm has only a single substation. The cabling problem assumes substations with fixed locations. Since the locations have a huge impact on the cabling costs, we also introduce a problem asking for their optimal position.

\section{The Cable Layout Problem}
\label{sec:cablelayoutproblem}

As discussed in our motivation, to minimize the construction costs of a wind farm, we compute an optimal cable network to connect the turbines. For now, we assume fixed locations of turbines and substations, and only consider the cabling up to the substations, ignoring how the power is delivered from the substations to the electrical grid. That means, the cables we consider connect a pair of turbines or a turbine to a substation. For each such possible connection, we can choose between different types of cables, differing in their cost and maximum power rating they are designed for, which we call the \emph{cable capacity}\footnote{Not to be confused with the capacity in electrical physics.}. Also, for each substation, a maximum total power rating of all connected turbines is specified, which we call the \emph{substation capacity}. The \emph{Wind Farm Cable Layout Problem} aims to choose the cable types such that the overall cabling costs are minimized, and the capacity constraints for the cables and substations are satisfied.

Before describing the whole problem in a more formal way, we need a notation for the input data, that is the power ratings of turbines and capacities, as well as the costs of the cables.

\paragraph{Power Ratings}

In a wind farm, power ratings and capacities occur at different places. Turbines generate a particular amount of power. Likewise, cables, substations and other electronic components have a rating limit they are designed for, while components with higher limits usually cost more.

The power a turbine generates depends on multiple factors. It is self-explaining that the wind strength plays the most significant role, but it affects all turbines in the same wind farm more or less equally. In practice, the ratings of all turbines in the same wind farm are very similar to each other, since they are usually of the same type and their exact location affects the wind strength only slightly. However, a turbine has a specified \emph{nominal power} which the generated power will never exceed.

Therefore, instead of expressing power ratings in units of watts, we simplify the notation a lot by expressing them in multiples of the nominal rating of one turbine. Furthermore, we only consider the situation in which every turbine produces the nominal power, that means its production is set to $1$. The power limits of cables and substations are rounded down to whole numbers.


\paragraph{Cabling Costs}

The cost to lay a cable from one node to another is basically composed of three parts: the connection points, the cable itself, and the cost of installing the cable between the nodes. The costs for the connection points in both nodes are independent of the length of the cable, but depend on the power rating routed through. The cost of the cable itself is the product of the cable length and the cost per length of the cable type used. Which cable type suites the connection mainly depends on the power rating. Finally, the cost to install the cable is significant especially when the cable is to be run underground, in which case it mainly depends on the terrain between the nodes.

When solving the cabling problem in this work, we are not interested in how the cabling cost is composed in detail. Not even the length of the cable is relevant on its own. Instead, for each edge and available cable type its total cost and the maximum power rating are interesting. These values can be precomputed for every edge separately. \autoref{fig:costfunctionexample} shows this data for an edge for which four cable types are available. For example, the first cable supports the power of two turbines. This edge does not support the power of more than 9 turbines.

For any edge, we call a cable \emph{superseded}, if there is any other cable available which has both a lower or the same cost as well as a greater or the same capacity. Superseded cables are not considered at all in our algorithms, since there is always a better (or equally well) option. This has the consequence that when ordering the cables by their cost, they are also automatically ordered by their capacities, and vice versa. This order implies the \emph{cable number}.

Note that our cable layout problem allows only one cable to be placed on each edge. However, if the user wants to allow a parallel wiring of multiple cables on the same edge, we can simulate this scenario by adding virtual cable to the list of available cable types for that edge, by precomputing the cost and capacity of the wiring. Even more complicated setups can be modeled in an analogous way.

\begin{figure}[tb!]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Cable number & Capacity & Cost \\
		\hline
		1 & 2 & 1.3 \\
		2 & 4 & 1.5 \\
		3 & 7 & 2.0 \\
		4 & 9 & 2.2 \\
		\hline
	\end{tabular}
	
	\caption{An exemplary edge for which four cable types are available.}
	\label{fig:cabletypesexample}
\end{figure}


\paragraph{The Input Data}

Formally, an instance of the Wind Farm Cable Layout Problem has the following input data. The wind farm has $t$ turbines and $s$ substations. We are given the set of possible connections, and for each of them a (separate) set of cable types, each with a capacity and cost. Furthermore, for each substation its capacity is specified, limiting the number of turbines which are allowed to be connected to it.

A cable network consists of the chosen cable type (maybe none) for every possible connection. It is called \emph{valid} if it respects the constraints given by the capacities of the chosen cables and the substations. An instance is called \emph{feasible} if there exists any valid cable network. In this case, its solution is a valid cable network with minimal total costs of the chosen cables.

Using the following mathematical model, this problem can be defined using a flow network, which is then formulated as a mixed integer linear program.


\paragraph{The Graph}

We model the wind farm as a simple directed graph $G = (V,E)$ in which turbines and substations are nodes. We denote them as separate sets: $V_T$ is the set of turbine nodes, and $V_S$ are the substation nodes, such that together they form the set of all nodes $V = V_T \cup V_S$. Each pair of nodes, which can be connected by a cable, is modeled as an edge in this graph. The direction of the edge is arbitrarily chosen, yet it needs to be consistent to have a notation of direction when talking about a positive or negative flow on an edge. The set of edges is denoted by the symbol $E$.

For each edge, we are given a set of cable types, each with a capacity and a cost. To notate these numbers, let $k \in \naturals$ be the maximum number of cable types available on any edge. For any edge, for which less than $k$ cable types are available in the original input, add dummy cable types with zero capacity (and arbitrary cost) such that every edge has $k$ cable types. The cable types are ordered in ascending capacity. Then, for edge $(v,w)$ and $i \in \indices k$, the capacity of the $i$-th cable is denoted as $\varccap vwi \in \realsgeq$ and the cost as $\varccost vwi \in \realsgeq$. To simplify the notation in the special case of an edge with no power routed through, we define these variables also with a cable type index of $0$: $\varccap vw0 := 0$ and $\varccost vw0 := 0$.

Finally, the capacity of substation $v \in V_S$ is denoted as $\varscap v \in \mathbb{N}$.


\paragraph{The Flow Network}

Before modeling the optimization problem as a flow network as explained in \autoref{sec:flownetworks}, we define $G^\star = (V^\star, E^\star)$ as a slightly modified version of $G=(V,E)$: we add a dummy node $\sigma$, later serving as a \emph{global sink}. This makes up $V^\star := V \cup \{\sigma\}$. Furthermore, we connect all substations to this global sink with the edges $E_\sigma := V_S \times \{\sigma\}$. Together with the original edges, $E^\star := E \cup E_\sigma$.

Now we are ready to model the optimization problem as a flow network $(G^\star, u, \balance, c_{(\cdot)})$. Note that since we notate power ratings as multiples of the nominal power of a single turbine, our flow values are whole numbers, and the edge cost functions are of the form $\wholes \rightarrow \realsgeq$.

The dummy edges are used to model the substation capacities: for substation $i \in V_S$, the edge $e = (i, \sigma) \in E_\sigma$ has capacity $u(e) := \varscap i$ and does not cost anything by setting $c_e(x) := 0$ for all $x \in \wholes$.

For all other edges $e = (v,w) \in E$, we also have to define the capacity $u(e)$ as well as the cost function $c_e : \wholes \rightarrow \realsgeq$. They model the cable type parameters. The edge capacity is set to the maximum capacity among the available cable types, that is $u(e) := \max_{i \in \indices k} \varccap vwi$. For a given flow value $f(e)$, we set $c_e(f(e)) := \varccost vwi$ where $i$ is the minimum value in $\{0\} \cup \indices k$ such that $|f(e)| \leq \varccap vwi$ (i.e. the cost of the cheapest suitable cable). Since the flow absolutely never exceeds its upper bound $u(e)$, for $x \in \wholes$ with $|x| > u(e)$, the value of $c_e(x)$ does not have to be defined. Consequently, a suitable $i$ is always found.

The resulting edge cost functions are all of the same basic shape: a step-wise constant function with one segment for each available cable type. \autoref{fig:cabletypesexample} shows the cost function for the cables from \autoref{fig:costfunctionexample}. As expected, it does not charge any cost for a power rating of zero, since in this case no cable is laid at all. The value of the edge cost function for any value greater the maximum capacity among the available cable types is not relevant, as the edge's capacity will constraint the flow value.

\begin{figure}[tb!]
	\centering
	\begin{tikzpicture}[
			scale=0.6,
			axis/.style={thick, ->, >=stealth, line join=miter},
			plot/.style={very thick}
		]
		
		% axis
		\draw[axis,<->] 
		(11.5,0) node[above left] {Power rating} 
		-| (0,4)    node[left]  {Cost};
		
		% ticks on x axis
		\foreach \x in {0,...,10} {
			\draw[thin] (\x,0)--(\x,-.15);
			\node at (\x.5,-.7) {$\x$};
		}
		
		% plot
		\draw[plot] (0,0)
		-| (1,1.3)
		-| (3,1.5)
		-| (5,2)
		-| (8,2.2)
		-- (10,2.2);
	\end{tikzpicture}
	
	\caption{The edge cost function corresponding to the cable types from \autoref{fig:cabletypesexample}.}
	\label{fig:costfunctionexample}
\end{figure}

Solving the flow network $(G^\star, u, \balance, c_{(\cdot)})$ results in a flow $f_\textrm{OPT}(e)$ for each edge, which corresponds to the power routed through a cable on that edge, if the network is feasible. Otherwise, the original cabling problem instance is also infeasible.

From a feasible flow, a cable layout can be computed trivially. A cable type for each edge is determined by choosing the cable with lowest cost with capacity at least the absolute value of the flow for that edge. If there is no flow on an edge, no cable is chosen.


\paragraph{Mixed Integer Linear Programming}

This network flow problem can be easily expressed as a mixed integer linear program (MILP) with integral variables and real coefficients. Note that the following model computes the choice of cable types directly, and the flow values only appear as helper variables in the program, as they are not important for a solution to the original problem. Also, we do not need the dummy edges introduced for the flow network. Instead, we use the original graph $G=(V,E)$.

To model the choice of cable types on each edge $(v,w) \in E$, we introduce the set of binary variables $\varcable vwi$ for each cable type. If $\varcable vwi = 1$, the $i$-th cable type was chosen for edge $(v,w)$. For every edge, at most one such variable can be $1$.

Furthermore, we introduce the integer helper variable $\varflow vw$ for each edge $(v,w)\in E$, which corresponds to the flow value $f(v,w)$ in the flow network. It is not required for the result, but to model the constraints in the flow network: the capacity constraint for each edge and the flow conversation at each node. To simplify notation, we alias $\varflow wv := -\varflow vw$ for any edge $(v,w) \in E$. Note that this does not add more variables to the linear program.

Usually, the edge cost in a flow network depends on the flow through that edge. However, since in our case the cabling cost only depends on the choice of cable, which is modeled by the separate set of variables $\varcable vwi$, it can be expressed without the flow helper variables $\varflow vw$ in our linear program. Before listing all equations and constraints of the program, we first show how the cost and capacity of edge $(v,w)$ can be expressed as sums of products using the variables from above.
\begin{align*}
	\textrm{Cost of edge $(v,w)$: } &\quad \sum_{i\in \indices k} \varccost vwi \cdot \varcable vwi \\
	\textrm{Capacity of edge $(v,w)$: } &\quad \sum_{i\in \indices k} \varccap vwi \cdot \varcable vwi \\
\intertext{Similarly, the excess of power at node $v$ can be expressed as a sum of flow values of the incident edges.}
	\textrm{Excess at node $v$: } &\quad \sum_{w\in N(v)} \varflow vw\\
\intertext{The objective function, the total cabling cost, is then a sum of the cost over all edges.}
	\textrm{Total cabling cost: } &\quad \sum_{(v,w)\in E} \left( \sum_{i\in \indices k} \varccost vwi \cdot \varcable vwi \right)
\end{align*}
This term is to be minimized under the following constraints:
\begin{align}
	\label{eq:linprog1}
		\varcable vwi
		& \in \{0,1\}
		&& \forall (v,w)\in E, {i\in \indices k}
	\\
	\label{eq:linprog2}
		\varflow vw
		& \in \wholesgeq                 
		&& \forall (v,w)\in E
	\\
	\label{eq:linprog3}
		\sum_{i\in \indices k} \varcable vwi  
		& \leq 1                    
		&& \forall (v,w)\in E
	\\
	\label{eq:linprog4}
		\varflow vw
		& \leq \sum_{i\in \indices k} \varccap vwi\cdot \varcable vwi
		&& \forall (v,w)\in E
	\\
	\label{eq:linprog5}
		\sum_{w \in N(v)} \varflow vw
		& \leq \varscap v                   
		&& \forall v\in V_S 
	\\
	\label{eq:linprog6}
		\sum_{w \in N(v)} \varflow vw  
		& = -1                      
		&& \forall v\in V_T
	%\\
	%\label{eq:linprog7}
	%	\sum_{v \in T_S} \sum_{w \in N(v)} \varflow vw
	%	& = t
\end{align}

The first two conditions \ref{eq:linprog1}, \ref{eq:linprog2} define the range for the variables in the linear program, and condition \ref{eq:linprog3} ensures that only one cable type is chosen for each edge. Condition \ref{eq:linprog4} constraints the power throughput of each edge to the capacity of the chosen cable type, as in \ref{eq:flow1}. Furthermore, condition \ref{eq:linprog5} constraints the total power coming in at each substation to its capacity. The balance is set to $-1$ for each turbine by condition \ref{eq:linprog6}. Together, these constraints model the flow conservation as in \ref{eq:flow2}.

This mixed integer linear program has $\bigO(|E|\cdot k)$ variables and $\bigO(|E|)$ constraints.


\paragraph{The Substation Cable Layout Problem}

We also consider the special case with only a single substation, that is $s=1$, which we call \emph{Substation Cable Layout Problem}. Here, the substation capacity is needless to specify, since there are just two possible cases: it can handle the power of all turbines, then the capacity is always respected; or it can not handle it, but then there does not exist a feasible solution.

As we will see later, this special case is much simpler to solve.



\section{The Substation Assignment Problem}

The Wind Farm Cable Layout Problem can be split into two layers. First, for every turbine $v \in V_T$, we find a substation $z \in V_S$, to which its power is routed. We say, $v$ is \emph{assigned to} $z$. We call this sub-problem the \emph{Substation Assignment Problem}. Then, for every substation $z \in V_S$, the Substation Cable Layout Problem is solved in which only the turbines assigned to $z$ are considered. The corresponding sub-graph is given by $G_z = (V_z, E_z)$ with $V_z := \{v \in V : v \textrm{ assigned to } z\} \cup \{z\}$ and $E_z := E \cap (V_z\times V_z)$.

As its input, the Substation Assignment Problem gets the same information as the Wind Farm Cable Layout Problem. Its output is a function $q : V_T \rightarrow V_S$ which assigns turbine $v\in V_T$ to substation $q(v) \in V_S$. The Substation Assignment Problem is an optimization problem. Its optimal solution $q_\textrm{OPT}$ is the assignment function having the minimum cable layout cost, which is defined as the sum of the costs of separate substation cable layouts as defined above. Observe that an optimal solution to the Substation Assignment Problem has the same cable layout cost as the solution of the corresponding Wind Farm Cable Layout Problem with the same input.




\section{The Substation Positioning Problem}

\todo{So far, we discussed the problem of finding cost-optimal cable layouts in wind farms where turbines and substations have predefined locations. However, when only the locations of the turbines are fixed, but the substations can be positioned freely or with some degree, the cabling costs can be reduced further.}

\todo{For this, we introduce the \emph{Substation Positioning Problem}: Given $t$ turbines (again denoted as $V_T$) and $s$ substations (again $V_S$, not yet located), find their optimal locations such that the total cabling costs are minimized. That is, among all possible combinations of substation locations, find the one resulting in the lowest cost as found by the Wind Farm Cable Layout Problem.}

\todo{Before specifying this problem formally, we need to discuss how to handle variable substation locations. Their allowed locations need to be given in a problem instance. Also, the costs for cables from turbines to substations are now no longer fixed; they depend on the location of the substation.}

\todo{First of all, we need a general notation of coordinates. We use the euclidean standard space. Then, for each turbine $v \in V_S$, its location is given as point $l_v = (x_v, y_v)$ in a problem instance. Furthermore, the allowed locations for substation $w \in V_T$ is given as the polygon (list of points, here $j$ points) $p_w = ((x_{w1}, y_{w1}), \dots, (x_{wj}, y_{wj}))$. The distance from turbine $v\in V_T$ to substation $w\in V_S$, when its location is $l_w = (x_w, y_w)$, is the euclidean distance $d(l_v, l_w) := ((x_w-x_v)^2+(y_w-y_v)^2)^\frac12$. The cost for cable type $i$ on edge $(v,w)$ is then given by two numbers: a base cost $\varccost vwi^\textrm{base}$ and a cost-per-length factor $\varccost vwi^\textrm{len}$, resulting in an edge cost depending on the substation position:}
\[
	\varccost vwi(l_w) := \varccost vwi^\textrm{base} + \varccost vwi^\textrm{len} \cdot d(l_v, l_w)
\]
\todo{Both factors can be precomputed similarly as before, when the cable lengths where fixed. The only remaining problem is the varying terrain. \todo{Wie erklären, dass wir das jetzt nicht mehr berücksichtigen?}}


\todo{$S$ Substations are to be positioned in order to connect $T$ wind turbines to the power grid. The objective is to determine their positions such that laying cables between turbines and the substations as well as from the substations to the power grid results in minimal overall cost.}

\todo{The power grid is given as a set of $G$ nodes at which the power of the wind farm can be injected. We are charged for each injection and the cables between the substations and the grid nodes.}

\todo{The positions of the substations are in the euclidean space. For each node $u$, we are given a distance function $d_u : \mathbb R^2 \mapsto \mathbb{R}_+$ to query the distance of a substation given its position. In the simplest case, this function is just the euclidean distance function to the position of  node $u$. It can however also model terrain dependent costs of placing substations in an on-shore wind farm, or even disallow placing substations in some areas.}

\todo{For the connections between a power grid node and a substation (``export cables'') we use a separate set of cable types, to allow for different costs.}