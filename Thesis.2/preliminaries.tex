\chapter{Related Works}

\todo{...}


%% ==============
\chapter{Preliminaries}
\label{ch:preliminaries}
%% ==============

We define the following notations for sets of numbers. As commonly, the set of \emph{real numbers} is written \reals. When writing \realsgeq, we mean the set of \emph{non-negative real numbers}.
The set of \emph{whole numbers} is written \wholes, and likewise we denote the set of \emph{non-negative whole numbers} as \wholesgeq.
The set of \emph{natural numbers} is written \naturals, which does not include the zero. To denote the first $k$ natural numbers, usually used for indexing a family of $k$ variables, we use the symbol $\indices k := \{1, \dots, k\}$.

\paragraph{Graphs} \todo{Überschrift sinnvoll? Wieder zurück in 3.1 verschieben?}
\label{sec:graphs}



In general, a (simple) \emph{graph} $G = (V,E)$ is an abstract structure representing a set of objects, which we call \emph{nodes}, and pairwise connections between them, which we call \emph{edges}. The set of nodes are denoted as $V$ and the set of edges as $E$. When the graph is \emph{directed}, the edges have a particular direction. For the two nodes $v, w \in V$, the edge connecting them is denoted as the pair $(v,w) \in E$. It is distinct from the opposite edge $(w,v) \neq (v,w)$, although both can be in $E$ at the same time. When the graph is \emph{undirected}, edges do not have a particular direction. In this case, an edge connecting $v$ and $w$ is written as a set $\{v, w\} \in E$. Note that $\{w, v\} = \{v, w\}$. \todo{Brauchen wir überhaupt ungerichtete Graphen?}

We introduce the \emph{neighborhood} $N(v)$ of $v \in V$ as the set of \emph{adjacent} nodes that are connected to $v$ via an edge. These edges are called \emph{incident} to $v$ and are denoted as $\delta(v)$. Note that in the case of a directed graph, these sets include both incoming and outgoing edges incident to $v$. To make a distinction of these two cases, we split $\delta(v)$ in the sets of incoming edges $\delta_-(v)$ and the outgoing edges $\delta_+(v)$. Similarly, the neighborhood $N(v)$ is split into $N_-(v)$ and $N_+(v)$, which denote nodes connected to $v$ via incoming or outgoing edges, respectively. \todo{Diese ganzen Mengen formal definieren?}



\section{Flow Networks}
\label{sec:flownetworks}

In a directed graph $G=(V,E)$, a \emph{flow} $f : E \rightarrow \reals$ assigns each edge a \emph{flow value}. When it is negative for an edge $e = (v,w)$, it represents a flow in the edge's opposite direction, that is from $w$ to $v$, instead of\todo{(?)} from $v$ to $w$. \todo{Folgenden Satz entfernen, falls wir insgesamt auf die Def. von ungerichteten Graphen verzichten.} When dealing with undirected graphs as the original input in an algorithm using a flow, it is common to define a corresponding directed graph, in which the edges directions are chosen arbitrarily but fixed throughout the algorithm, to have a notation of flow direction.

Given the flow $f$, the \emph{excess} $\excess(v)$ of node $v\in V$ is defined as the sum of incoming minus outgoing flow over all incident edges:
\[
	\excess(v) :=
		\underbrace{ \sum_{e\in \delta_-(v)} f(e) }_{\textrm{incoming into $v$}}
		-
		\underbrace{ \sum_{e\in \delta_+(v)} f(e) }_{\textrm{outgoing from $v$}}
\]
Observe that the sum of excesses of all nodes is always zero for any $f$, as every edge flow in the graph contributes to the whole sum once positively and once negatively:
\begin{align*}
	\sum_{v\in V} \excess(v)
		&= \sum_{v\in V} \left[ \sum_{e\in \delta_-(v)} f(e) - \sum_{e\in \delta_+(v)} f(e) \right] \\
		&= \sum_{e\in E} f(e) - \sum_{e\in E} f(e) \\
		&= 0
\end{align*}


A \emph{flow network} $(G,u,\balance)$ specifies a problem whose goal is to find a possible flow from \emph{sources} (sometimes also called producers) to \emph{sinks} (sometimes also called consumers) under certain constraints. Note that different kinds of definitions for flow networks exist in literature, and in the following we describe the one we use in our algorithms\footnote{In this work, we use a many-sources many-sinks network, but the most common definition of flow networks used in literature has a single source and a single sink node. However, the former can easily be transformed into the latter by adding a dummy source and a dummy sink, which are connected to all sources and sinks, respectively.}. Every node $v$ is assigned a \emph{balance} $\balance(v)$, which is negative for sources, positive for sinks and zero for nodes which are neither sources nor sinks. We require the sum of the balance values among all nodes to be zero, that is $\sum_{v\in V} \balance(v) = 0$.
The function $u : E \rightarrow \realsgeq$ defines the \emph{capacity} $u(e)$ of edge $e \in E$, which is an upper bound of the absolute flow value $f(e)$.

A flow $f$ is a \emph{feasible} solution to the flow network $(G,u,\balance)$, if all edges $e \in E$ satisfy the capacity constraint $u(e)$ in both directions:

\begin{equation}
	\label{eq:flow1}
	-u(e) \leq f(e) \leq u(e)
\end{equation}

and all nodes $v\in V$ satisfy the flow conservation constraint, that is its excess equals the given balance:

\begin{equation}
	\label{eq:flow2}
	\excess(v) = \balance(v)
\end{equation}


\paragraph{Optimization Problem / Minimum-Cost Flow}

\todo{Welche der beiden Überschriften passt hier besser?}

The above definition of a flow network $(G,u,\balance)$ formulates a decision problem. It is feasible if a valid flow $f : E \rightarrow \reals$ exists. Otherwise, the network is infeasible. We formulate a corresponding optimization problem $(G,u,\balance,c_{(\cdot)})$, asking about an optimal solution, if any exists.

For this, the additional parameter $c_{(\cdot)}$ in a problem instance specifies for any edge $e\in E$ a corresponding edge cost function $c_e : \reals \rightarrow \realsgeq$. Given a flow $f$, the non-negative cost of edge $e$ is given by $c_e(f(e))$. For symmetry reasons, we require that the cost for a negative flow equals the cost for the corresponding positive flow, that is $c_e(-x) = c_e(x)$ for all $x\in \reals$.

The objective function which is to be minimized is the total cost $c_\textrm{total}(f)$, that is the sum of all edge costs for a flow $f$:

\begin{equation}
	c_\textrm{total}(f) := \sum_{e\in E} c_e(f(e))
\end{equation}

The structure of the edge cost functions is essential for the complexity of the flow network problem. If they are linear functions, then the optimization problem is solvable in polynomial time\todo{Quelle: Goldberg/Tarjan?}. The same holds for instances in which the cost functions are contiguous and convex functions composed of several linear segments. Such an instance can be transformed into an instance with just linear cost functions by copying the edges such that every copy of the input edge models a linear segment of the original cost function.

Problems in which the cost functions are non-convex are proven to be NP-hard\todo{Quelle}. As explained later, the cost functions we deal with are of this shape.







\section{Simulated Annealing}
\label{sec:preliminaries:simulatedannealing}

Optimization problems can be solved exactly or, whenever an exact solution takes too much time to be computed, solved heuristically. In general, the solution of a heuristic algorithm does not perform as well as the exact solution, when compared by their cost. However, they usually outperform exact solvers in their computation time a lot, especially when the problem is NP-hard.

In general, a heuristic solver usually searches for a good solution in the solution space in a stochastic way. Simulated Annealing is one such heuristic, and is the base of our algorithms in this work. Its idea comes from the physical process of cooling down a glowing metal: its molecular structure can be altered while being hot enough. When cooling it down very quickly, its structure is frozen, not giving it any time to get into an energy-optimal form. However, when cooling the metal slowly, molecules still have time to move around such that the energy of the material is minimized: a crystal structure is formed. In nature, molecules move around randomly, and the material can have an intermediate state with higher energy before getting into a state with better energy. The probability for this to happen decreases with decreasing temperature.

This is the core idea of Simulated Annealing. The overall algorithm for an abstract problem is shown in \autoref{algo:sa}. Given an instance $I$, we first create an initial solution $r$ by the initialization function $INIT$. It is repeatedly changed slightly over time by the mutation function. Whenever a change results in lower energy, it is accepted as the new solution candidate. However, when it raises the energy, it is only accepted with some probability. The probability depends on a temperature parameter: the lower the temperature, the less the probability of accepting a worse solution. Also, the probability depends on how much worse the new solution is. The temperature is lowered over time.


\begin{algorithm}
	\caption{Simulated Annealing}
	\label{algo:sa}
	
	\SetFuncSty{textsc}
	\DontPrintSemicolon
	\KwIn{Instance $I$, Initial temperature $T_0$, Temperature step-size $\Delta T$}
	\KwOut{Near-optimal solution $r$ to $I$}
	
	\SetKwFunction{Init}{Init}
	\SetKwFunction{Eval}{Eval}
	\SetKwFunction{Mutate}{Mutate}
	
	$T \gets T_0$\;
	$r \gets \Init{I}$\tcc*{Initial solution}
	$E \gets \Eval{I,r}$\;
	\Repeat{$r$ is well enough}{
		$r' \gets \Mutate{I,r}$	\tcc*{New solution candidate}
		$E' \gets \Eval{I,r'}$\;
		\eIf{$E' \leq E$}{
			$a \gets 1$\tcc*{Always accept better solutions}
		}{
			$p \gets \exp(-T\cdot \frac{E'-E}{E})$\tcc*{Acceptance probability formula}
			$a \gets \textrm{$1$ with probability $p$, $0$ otherwise}$ \;
		}
		\If {$a = 1$}{
			$(r,E) \gets (r',E')$\tcc*{Accept solution $r'$}
		}
		$T \gets T - \Delta T$\tcc*{Lower the temperature}
	}
\end{algorithm}


Before using Simulated Annealing to solve a concrete optimization problem, we have to define a representation of a solution, an evaluation, an initialization and a mutation function.

The \emph{representation} is an indirect way to fully describe a solution candidate. Its structure may be very different from the solution itself, but deriving it from a representation needs to be deterministic and efficient. Describing solutions indirectly by a representation has the advantage to be able to define mutations more easily. Complex constraints of the search space do not need to be considered when designing a suitable representation structure.

An \emph{evaluation function} computes the energy of a solution candidate given its representation. This might be split into two separate steps: decoding the representation to a solution, and evaluating the resulting solution.

Two randomized functions in the Simulated Annealing algorithm define the search space exploration. An initial solution representation is created by the \emph{initialization function}. Starting from there, a \emph{mutation function} is applied repeatedly: it forms a new solution representation by changing the current only slightly. For any input, all possible outputs of this function define the \emph{neighborhood} of the given representation.

\todo{Hierfür Verweise finden, in welchen dieses Problem ähnlich geschildert wird.} In the initialization function, it might be tempting to use a random distribution preferring initial solutions with already low energy. However, it can be dangerous to compensate a poor mutation function with such a clever initialization, as it should be still possible to escape from the local optimum of the initial solution, in order to be able to also explore other local optima.

The ability to escape any local optimum is mainly affected by the design of the mutation function. It defines a \emph{search graph}, in which representations are nodes and neighbors are connected by edges. In order to make every representation reachable, starting from anywhere in this graph, it needs to be connected. Also, the number of mutations required to make all representations reachable should be kept low.



\paragraph{The temperature curve and acceptance probability formula}

The main idea of Simulated Annealing is to define a \emph{temperature parameter} $T$, which is lowered over time. This parameter defines the \emph{probability} of accepting a worse solution intermediately, hoping that a better solution is found later -- a process which is required to escape from local optima.

More formally, a typical Simulated Annealing algorithm does the following repeatedly, starting from an initial solution. A neighbor is formed by applying the mutation function and evaluated. Its energy is then compared to the current solution: if the energy was decreased, i.e. a better solution was found, it is definitely accepted as the new ``current'' solution for the next iteration. Otherwise, the probability of acceptance is
\[
P_\textrm{abs} = \exp(-T \cdot \Delta E)
\]
where $\Delta E = E'-E$ is the energy difference, which is positive when the new solution is worse. We call this the \emph{absolute} probability formula.

The problem with the definition from above is that the magnitude of $\Delta E$ depends on the magnitude of $E$, which in turn highly depends on the problem instance. It would be difficult to define a useful curve for $T$ leading to acceptance probabilities suitable for the concrete instance. To handle this, we \emph{normalize} the energy difference by dividing it by the energy of the current solution. We call this the \emph{normalized} probability formula:
\[
P_\textrm{norm} = \exp(-T \cdot \frac{\Delta E}{E})
\]
We still have to define the initial temperature $T_0$ and how this temperature is changed over time. Typical Simulated Annealing implementations use a fixed $T_0$ and a step $\Delta T$, subtracted in every iteration, resulting in a linear temperature curve independent of the instance as well as the performance of the algorithm over time.

The problem with such a curve is that its parameters have to be tuned depending on each instance. We later introduce a different approach for how to define the temperature curve over time, not requiring an instance-dependent tuning.



