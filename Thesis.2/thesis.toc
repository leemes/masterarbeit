\select@language {english}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Components in Wind Farms}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Related Works}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Preliminaries}{5}{chapter.3}
\contentsline {paragraph}{\nonumberline Graphs}{5}{section*.2}
\contentsline {section}{\numberline {3.1}Flow Networks}{5}{section.3.1}
\contentsline {paragraph}{\nonumberline Optimization Problem / Minimum-Cost Flow}{6}{section*.3}
\contentsline {section}{\numberline {3.2}Simulated Annealing}{7}{section.3.2}
\contentsline {paragraph}{\nonumberline The temperature curve and acceptance probability formula}{8}{section*.4}
\contentsline {chapter}{\numberline {4}Optimization Problems in a Wind Farm}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}The Cable Layout Problem}{11}{section.4.1}
\contentsline {paragraph}{\nonumberline Power Ratings}{11}{section*.5}
\contentsline {paragraph}{\nonumberline Cabling Costs}{12}{section*.6}
\contentsline {paragraph}{\nonumberline The Input Data}{12}{section*.7}
\contentsline {paragraph}{\nonumberline The Graph}{13}{section*.8}
\contentsline {paragraph}{\nonumberline The Flow Network}{13}{section*.9}
\contentsline {paragraph}{\nonumberline Mixed Integer Linear Programming}{14}{section*.10}
\contentsline {paragraph}{\nonumberline The Substation Cable Layout Problem}{15}{section*.11}
\contentsline {section}{\numberline {4.2}The Substation Assignment Problem}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}The Substation Positioning Problem}{16}{section.4.3}
\contentsline {chapter}{\numberline {5}Heuristics for the Wind Farm Cable Layout Problem}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}An Algorithm based on Simulated Annealing}{17}{section.5.1}
\contentsline {paragraph}{\nonumberline The Representation}{17}{section*.12}
\contentsline {paragraph}{\nonumberline The Evaluation Function}{18}{section*.13}
\contentsline {paragraph}{\nonumberline The Initialization Function}{18}{section*.14}
\contentsline {paragraph}{\nonumberline The Mutation Function}{19}{section*.15}
\contentsline {section}{\numberline {5.2}Improving the Algorithm}{19}{section.5.2}
\contentsline {paragraph}{\nonumberline Maintaining Multiple Instances}{19}{section*.16}
\contentsline {paragraph}{\nonumberline Crossing two Solutions}{20}{section*.17}
\contentsline {section}{\numberline {5.3}Improvement using a Two-Level Approach}{20}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Splitting the Problem in two Levels}{20}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Solving the Substation Assignment Problem}{20}{subsection.5.3.2}
\contentsline {chapter}{\numberline {6}Approximations for the Substation Positioning Problem}{21}{chapter.6}
\contentsline {chapter}{\numberline {7}Conclusion}{23}{chapter.7}
\contentsline {chapter}{Bibliography}{25}{section*.18}
\contentsline {chapter}{\nonumberline Appendix}{25}{chapter*.19}
