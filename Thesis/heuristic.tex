%% ==============
\chapter{Heuristics for Wind Farm Problems}
\label{ch:heuristic}
%% ==============

In \autoref{sec:cablelayoutproblem}, we introduced the Wind Farm Cable Layout Problem. We proposed an MILP which we implemented using a generic solver for our experiments. However, in this chapter we propose an alternative algorithm solving the layout problem heuristically. The first approach is based on Simulated Annealing, which we describe in the first section. The algorithm is then improved in the following sections.




\section{An Algorithm based on Simulated Annealing}
\label{sec:algorithmbasic}

Our first algorithm which heuristically solves the \emph{Wind Farm Cable Layout Problem} efficiently is based on Simulated Annealing, a randomized search space exploration heuristic introduced in \autoref{sec:preliminaries:simulatedannealing}. The algorithm uses an indirect representation of a solution candidate, which is altered over time using a mutation scheme.

This section is structured as follows. First, the algorithm is described in details. We then evaluate it for randomly generated instances. The strengths and limits of the algorithm are discussed, before we try to fix this in the next section by an improved version.

The introduction of Simulated Annealing in \autoref{sec:preliminaries:simulatedannealing} mentions the ``building blocks'', which need to be defined to construct a complete heuristic for a concrete problem. This subsection presents the representation of the cabling and functions needed for initialization and mutation of this representation. Evaluating it leads to the cabling cost, which is to be minimized by the Simulated Annealing heuristic. 



\paragraph{The Representation}

Recall that the representation shall describe a solution candidate indirectly. A solution to the Wind Farm Cable Layout Problem assigns each edge in the input graph a cable type. 

Our representation is composed of two structures: a \emph{potential field} and \emph{edge cuts}. The potential field\footnote{not to be confused with the potential field in electrical physics} $p : V \rightarrow \indices n$ assigns each node $v \in V$ in the graph a number $p(v)$, its potential. Recall that $\indices n = \{1, \dots, n\}$, where $n$ is the number of nodes. Each node has a different value, so $p$ is a permutation of the node indices. Its concrete role becomes clear when the evaluation function is explained below. The edge cuts $H \subset E$ are the edges which shall not be used for cabling. Together, they form the representation as a tuple $R = (p,H)$.

We create a representation of the initial solution as follows. For each node $v\in V$, we find its nearest substation. We order the nodes by the distance to their nearest substation in descending order. The initial potential $p(v)$ of each node is set to its rank in this ordered list. This results in substations having the largest potential values, and the turbines most far away from any substation have the lowest numbers. The initial edge cut list $H$ is empty.



\paragraph{The Evaluation Function}

Evaluating a representation $R$ requires decoding it into a solution, here the choices of cable types for each edge. Then, the cost of the solution is merely a task of summing up the cable costs, which is trivial in this case.

\begin{figure}
	\def\scale{0.45}
	\def\labeldistance{3.3cm}
	
	\centering
	\begin{tikzpicture}[
		rep/.style={minimum width=6cm},
		arr/.style={lightgray,->,>=stealth,line width=2mm},
		desc/.style={lightgray,text=black,<-,>=stealth,very thick}
	]
		\node[rep] (A) at (0,6) {\includegraphics[page=1,scale=\scale]{figures/reps.pdf}};
		\node [left of=A,node distance=\labeldistance] {\bf(A)};
		
		\node[rep] (B) at (7.5,6) {\includegraphics[page=2,scale=\scale]{figures/reps.pdf}};
		\node [right of=B,node distance=\labeldistance] {\bf(B)};
		
		\node[rep] (C) at (0,0) {\includegraphics[page=3,scale=\scale]{figures/reps.pdf}};
		\node [left of=C,node distance=\labeldistance] {\bf(C)};
		
		\node[rep] (D) at (7.5,0) {\includegraphics[page=4,scale=\scale]{figures/reps.pdf}};
		\node [right of=D,node distance=\labeldistance] {\bf(D)};
		
		\draw[arr] (A) -- (B);
		\draw[arr] (B) -- (C);
		\draw[arr] (C) -- (D);
		
		% description labels
		% (A)
		\draw[desc] (-1.3,4.3) |- (-1.5,3.8) node[left]{\small cut edge};
		\draw[desc] (.5,4.1) |- (-1.5,3.4) node[left]{\small normal edge};
		\draw[desc] (-1.27,7.55) |- (-1.8,7.9) node[left]{\small potential};
		% (B)
		\draw[desc] (8.0,7.2) -- (9.0,7.2) node[right]{\small turbine path};
		% (C)
		\draw[desc] (-0.5,1.6) |- (-0.8,2.15) node[left]{\small calble throughput};
		% (D)
		\draw[desc] (8.2,1.3) -- (9.2,1.3) node[right]{\small thin cable};
		\draw[desc] (7.7,0.8) -- (9.2,0.8) node[right]{\small thick cable};
	\end{tikzpicture}
	
	\caption{An example encoded in our representation (A) as well as the intermediate representations used during the evaluation: the turbine path representation (B), the cable throughput representation (C), and finally the cable types (D).}
	\label{fig:intermediate-representations}
\end{figure}

To simplify the explanation of how a representation, given the potential field and edge cuts, is decoded into cable type choices, we introduce two intermediate representations: the \emph{turbine path representation} and the \emph{cable throughput representation}. Their correlation is visualized in \autoref{fig:intermediate-representations}. In the following, we explain these representations and how to decode them in reverse order: from the last to the first.

The cable throughput representation (C) specifies for each edge the direction and amount of power routed through. From this, it is trivial to determine the corresponding cable types (D) by choosing the most inexpensive cable still supporting the requested throughput. Summing up all cable costs finally leads to the total cost of the representation. However, if for some edge there is no cable available which supports the requested throughput, we charge the cost of the thickest cable plus a penalty cost. We chose a penalty proportional to the square of the amount of power the cable is exceeded, as well as inversely proportional to the temperature. This means, penalties increase over time. Similarly, we enforce the substation capacity constraints by charging a penalty proportional to the amount of power it is exceeded by the solution. The proportionality factors for both types of penalties are tunable parameters of the algorithm.

The turbine path representation (B) specifies for each turbine, where its produced power travels through the network, finally reaching a substation. Every turbine has its own path encoded in this intermediate representation. From these paths, the cable throughput representation (C) is computed as follows. Given the paths of all turbines, we find the cable throughput for an edge by counting how many turbine paths use that edge. Recall that we have a notation of edge direction, which we use here to eliminate positive and negative flow on an edge: for every path which uses an edge in its direction, we increase a counter for that edge; whenever the same edge is used in its opposite direction, we decrease the counter. Finally, the absolute value of the counter is the throughput a cable on that edge needs to support.

The actual representation used in the algorithm (A), as explained above, contains the potential field $p$ and the edge cuts $H$. Using that, \autoref{algo:evaluation} computes a path $\pi$ for every turbine separately, which we explain in detail. Starting from the turbine $v$, the path is created incrementally node by node. The process ends when a substation was reached (\autoref{algo:evaluation:loop}). At each step, a restricted neighborhood $N'(x)$ of the current node $x$ is considered, from which we select the node maximizing some expression (\autoref{algo:evaluation:max}). This expression is based on the idea of a ``slope'' in a height field, but with some adjustments, which we explain in detail below. The restricted neighborhood, from which the best node is chosen, are the neighbors of $x$ of which the edge is not in the edge cut set and which is not already part of the path (\autoref{algo:evaluation:neigh}). When the restricted neighborhood is empty (\autoref{algo:evaluation:empty}), the path can not be terminated at a substation, and the whole representation is considered invalid. This is handled by setting its energy to $\infty$\footnote{We found that this case rarely occurs (less than one in a thousand cases). Whenever the energy of a representation evaluates to $\infty$, it is always discarded by Simulated Annealing. Effectively, this means that a new representation is chosen by mutating the current one again.}.

The formula used in \autoref{algo:evaluation:max} needs some more explanation: From the restricted neighborhood, nodes with higher potentials should be preferred (such that the potential field serves as a ``guide'' for the flow direction), while at the same time near neighbors (i.e. short cables) should be preferred. The most intuitive formula would be $(p(w)-p(x)) \cdot {d_{xw}}^{-1}$, i.e. basically the slope in the potential field in the edge direction, which we originally used in our algorithm. The problem here is that when no node with higher potential exists in the restricted neighborhood, this expression is always negative. This in turn means that longer distances are preferred, which we want to avoid. We divide the potential difference by $n$, i.e. normalize it to the range $(-1,1)$, and then add $1$ to that result. This gives a positive value in the range $(0,2)$ for every potential difference, while satisfying our original intent: neighbors with higher potential as well as smaller distance are preferred.

\begin{algorithm}
	\caption{Constructing a turbine path from the representation}
	\label{algo:evaluation}
	
	\SetFuncSty{textsc}
	\DontPrintSemicolon
	\KwIn{Graph $G = (V,E)$, Node distances $d_{vw}$, Capacities $\varccap vwi$, Costs $\varccap uvi$, Representation $R = (p,H)$, Turbine $v$}
	\KwOut{Path $\pi$ of nodes from $v$ to a substation}

	$\pi \gets$ new list of nodes\;
	$x \gets v$\;
	append $x$ to $\pi$\;
	\While {$x$ is not a substation} { \label{algo:evaluation:loop}
		$N'(x) \gets \{w \in V \;|\; (x,w) \in E \setminus H, \; w \notin \pi\}$ \label{algo:evaluation:neigh}\;
		\If {$N'(x) = \emptyset$} { \label{algo:evaluation:empty}
			\Return infeasible\;
		}
		$x \gets {\arg\max}_{w \in N'(x)} \left( (1+\frac{p(w)-p(x)}{n})\cdot {d_{xw}}^{-1} \right)$ \label{algo:evaluation:max}\;
		append $x$ to $\pi$\;
	}
	\Return $\pi$\;
\end{algorithm}



\paragraph{The Mutation Function}

When mutating a solution, we randomly choose one of the following different kinds of operations. Based on the representation $R=(p,H)$, they form a new representation $R'=(p',H')$.

The first method swaps two random values in the potential field $p$ (and keeps the edge cuts $H':=H$). We choose two distinct nodes $a,b\in V$ randomly and then set $p'(a) := p(b), \; p'(b) := p(a)$.

The second method also modifies the potential field $p$, but tries to only modify the value of one node notably. Since the potential field is defined as a permutation of node indices, we can not just change one entry. Instead, we adjust the potentials of other nodes very slightly such that the result is again a valid permutation. For this, we also choose two distinct nodes $a,b\in V$ randomly, where $a$ is the node of which the potential is to be changed significantly: it is set to the potential of $b$: $p'(a) := p(b)$. For now, assume that $p(a) < p(b)$, i.e. the potential of $a$ has been raised. We now decrease the potentials of all nodes with a potential in between $p(a)$ and $p(b)$ by $1$. This results in a permutation again, since we just ``rotated'' the potential values of some node subset. Analogously, if $p(a) > p(b)$, the potential of the nodes with potentials between $p(b)$ and $p(a)$ are increased by $1$.

The third method modifies the edge cuts $H$ while keeping the potential field $p':=p$ as is: we add or remove a random edge to the set of edge cuts. The probability to add a new edge cut (instead of removing one) depends on the current cardinality of $H$ (in order to probabilistically level out the number of cuts to some predefined number, for example $\bigO(\sqrt{n})$ with a tunable factor).





\section{Improving the Algorithm}
\label{sec:improvements}

The algorithm as seen so far does not perform as well as we hoped at first. We did early experiments in which we noticed several problems of this basic algorithm. Therefore, we implemented some improvements which we discuss in this section.



\paragraph{Dynamic Temperature Curve}

The temperature curve in the original Simulated Annealing heuristic is fixed over time (measured in the number of iterations). However, we observed that for larger instances the algorithm needs more iterations to reach a good solution compared to smaller instances, since more mutations need to be applied, as every mutation only performs one local change. For this, the temperature curve should be adjusted depending on the instance. We came up with two possible solutions: making $T_0$ and $\tau$ depend on the instance size $n$, or adjusting the temperature delta dynamically during runtime such that the curve is flattened as long as a good solution has not yet been reached.

As the first approach is just a matter of adjusting the algorithm's parameters before running it, we implemented the second idea into our algorithm. For this, we introduce a new parameter which is computed during running time: the \emph{activity} $\mu$ measures how volatile the current search exploration behaves. This is done by exponentially smoothing the probability of accepting worse solutions (whether they are accepted or not), by computing the following formula after \autoref{algo:sa:probability} in \autoref{algo:sa}:
\[
	\mu' := \alpha_\textrm{smooth} \, p + (1-\alpha_\textrm{smooth}) \, \mu 
\]
The value of $\mu$ is initialized with $1$. The parameter $\alpha_\textrm{smooth}$ is a tuning parameter which defines how easily the activity is influenced by temporary fluctuations of the probability value. We set it to $10^{-3}$ in our implementation. Adjusting the temperature after each iteration then becomes the formula
\[
	T' := (1 - \mu\tau) \cdot T
\]

This approach follows the intention that in the early stage of the cooling simulation, we basically explore the search space globally. Not much time should be spend in this phase. It is important that enough time is available for the later stage, in which the temperature should be rather cool such that the already quite good solutions can be further improved. This could be done by simply raising the value of $\tau$. However, the temperature in that late stage should not be \emph{too} low, as that would contradict to the idea of Simulated Annealing: that a worse solution should be accepted temporarily.

We want a temperature curve which falls quickly in the beginning, but slower towards the end. This is achieved by the formula for our dynamic temperature curve as we expect a decreasing activity during the runtime of a computation: making the temperature change proportional to the activity deforms the otherwise exponentially decreasing curve to a sub-exponential shape. \autoref{fig:t3i130-temp-plot} shows both the standard and dynamic temperature curves for one of our experiments. Note that both curves start and end with (approximately) the same temperature levels. Compared to the standard curve, the dynamic one thus basically \emph{compresses} the early phase, while \emph{expanding} the late stage of the computation. In \autoref{sec:generalobservations} we analyze experimentally how that influences the quality of the solution.

\begin{figure}[tb!]
	\hspace{0.1cm}\includeplot{t3i130-temp}
	\caption{The standard and dynamic temperature curve of one of our experimental instances, as well as the activity corresponding to the computation with the dynamic temperature curve. Both temperature curves start at $T_0 := 0.01$. The parameter $\tau$ is chosen separately in the two cases such that both curves reach approximately the same final temperature of about $0.00015$ ($= 1.5~\%$ of $T_0$).}
	\label{fig:t3i130-temp-plot}
\end{figure}



\paragraph{Maintaining Multiple Instances}

So far, our algorithm only handles one solution at a time. It is mutated and evaluated iteratively, resulting in a single search space exploration. The main difficulty with such an iterative exploration is escaping a local (non-global) optimum, when a worse solution needs to be accepted temporarily. Although Simulated Annealing is designed to make this possible, our experiments showed that the search exploration gets caught in a different local optimum every time the algorithm is ran with a different random seed. This is shown in detail in \autoref{ch:experiments}.

This led us to the idea to run multiple instances of the same algorithm in parallel, each with a different random seed. They explore the search space independently, and when one instance gets caught in a local optimum, others are not affected by that. We call the instances of the Simulated Annealing algorithm \emph{threads}\footnote{Not to be confused with the term in parallel computing.}. Note that every thread maintains its very own temperature, and they do not exchange any information.

Furthermore, we had the following additional idea for managing the computation threads. Initially, a configurable number of threads are started. From each thread, the currently best solution, its energy and the activity can be queried. A meta algorithm decides when a thread is likely to be caught in a local optimum, in which case its computation is stopped. We propose to use the activity as a measure of the likelihood that continuing a computation does not make much more sense. For this, an activity threshold value $\mu_\textrm{thresh}$ is introduced as a configurable parameter. As soon as the activity falls below this value, the thread is considered \emph{dead}, in which case it is stopped. This saves computation power now available either for the other threads, or for a whole new thread.

The solution of a stopped thread is not discarded, as the meta algorithm regularly queries all threads for their best solution seen so far. We globally track the currently best global solution, which is then the output of the overall algorithm. So only because a thread is considered dead, it does not mean that its solution is bad, it only means we do not give it more time to improve the computation.

A similar idea could be that instead of distributing the available computation power evenly among all threads, but rather proportional to their activity. This slows down the computation of inactive threads, while more effort is put into improving the solution of active threads.

Even more complex and smarter strategies could be developed for managing the threads. We leave this for future work.



\paragraph{Recovering from Bad Local Optima}

We observed in our early experiments that the Simulated Annealing algorithm sometimes takes very long time to improve its best solution seen so far. Our assumption is that this means that it is then caught in a local optimum from which it is hard to escape again (we discuss this in detail in \autoref{ch:experiments}). Therefore, we propose to track the number of iterations since the last improvement over the best solution seen so far. When this number exceeds a configurable threshold value, we reset the computation to that best solution, starting the search space exploration again from there.

An alternative to this idea is to \emph{branch} the thread in such cases: one thread continues the computation as according to the original algorithm, while a second one restarts from the best solution seen so far, increasing the number of computation threads by one.


%In Simulated Annealing, when the probability of accepting a worse solution is too high, local optima can be escaped easily but at the same time, the global optimum (in the case it is currently being explored) can be escaped equally easily. This motivated us to add a mechanism saving a solution when it seems to be a good candidate for the global optimum, or one near it. For this, the meta algorithm can \emph{clone} a solution, whenever a thread is about to accept a worse one. Depending on the energy difference between the current and the proposed worse solution, the meta algorithm decides whether or not to clone the thread such that one copy explores the worse solution as usual, while the other copy stays at the current solution candidate, which is mutated differently in its next iteration.

We do not want the number of threads to raise unconditionally, since the available running time has to be divided among them, reducing the running time per thread. For this, we configure our algorithm with a maximum number of threads. When exceeding this value, one thread should be killed. Note that we want to allow the Simulated Annealing algorithm to perform a predefined number of minimum mutations on newly created threads, before we consider it \emph{mature}. Only mature threads are allowed to be killed by the meta algorithm. From all mature threads we kill the worst, i.e. the one with highest energy of the best solution it has seen so far.



\section{Towards Evolutionary Algorithm}
\label{sec:evolutionary}

In our experiments, we made the observation, that different threads explore the search space at very different local optima. The solutions all have similar energy levels, but propose very different cable layouts, even the turbines are often assigned to different substations when comparing any two solutions.

This led us to the following idea which extends the above meta algorithm with a new event: we \emph{cross} two solutions, that is, we create a whole new solution based on information from the both. This approach is similar to how biological evolution works, and is known as an \emph{Evolutionary Algorithm}, in which our representation can be thought of as a genetic code.

We propose to integrate this technique in our existing algorithm as a separate phase. For this, the total running time is split into a longer first phase, in which the existing algorithm is run. Afterwards, the best pairs suitable for crossings are chosen, and their representation is crossed. The second, shorter phase is then used for solving these crossings.

We consider a pair of solutions suited for crossings if the two solutions have a good \emph{compatibility} but high \emph{diversity}. The compatibility measures how well we can construct a crossing from the both solutions, which hopefully results in an even better solution. Each crossing results in two new solutions.

A couple of questions arise, which have to be answered in order to complete the description of the algorithm: How is the pairwise compatibility and diversity of two solutions defined? How many pairs of solutions are considered for constructing crossings (i.e. how many crossings do we construct)? How is a crossing constructed? And finally, how much time of the total running time is used to solve these crossings? In the following, we provide suggestions to these questions.



\paragraph{Diversity and Compatibility of Two Solutions}

We define the \emph{diversity} of two solutions by the sum of the squared difference of the power ratings over all edges.

For the \emph{compatibility} of two solutions $A$ and $B$, given by representations $R_A=(p_A,H_A)$ and $R_B=(p_B,H_B)$, we decided to look at how their substation assignments differ. For this, we introduce the \emph{substation assignment difference graph} $G_\textrm{sad} = (V_S, E_S, w)$, which is an edge-weighted undirected and complete graph over the substation nodes ($E_S := V_S \times V_S$). Its weight function $w : E_S \rightarrow \wholesgeq$ assigns each edge $e = \{a, b\} \in E_S$ the number of turbines which are assigned to substation $a$ in one solution, and to substation $b$ in the other. Then, we select the two substations $s, t \in V_S$ minimizing the value of the min-cut $(G_\textrm{sad}, s, t, w)$.

The min-cut $(G_\textrm{sad}, s, t, w)$ is a partitioning problem in which the optimal solution is a pair of node subsets $P_0 \subset V$ and $P_1 \subset V$ (with $P_0 \cap P_1 = V$) such that the sum of the weights of all cut edges is minimized. The \emph{cut edges} are the edges of which one node is in $P_0$ while the other is in $P_1$. Several algorithms exist for the min-cut problem, such as the Stoer-Wagner algorithm \cite{mincut}.

Finally, the compatibility of the solutions is defined as the weight sum of the cut edges.





\paragraph{Constructing the Crossing}

The partitions resulting from the cut are reused for constructing the crossing of two solutions; we call the two partitions $P_0, P_1$. To put it simply, we use the representation $R_A$ of the first solution for regions which connect to substations to partition $P_0$ and for the rest of the farm, representation $R_B$ is considered. However, we should not cut the representation data sharply at the border of the two regions, as this introduces discontinuities in the potential field. We rather interpolate between the two solutions in regions near the border.

For this, we define a \emph{weight function} $z : V \rightarrow [0,1]$, which assigns each node $v\in V$ (turbine or substation) a weight $z(v)$. Later, this weight determines the composition of the representations from the both solutions we are crossing: areas in which the node weight is~$0$ will use the representation $R_A$, while a value of $1$ refers to $R_B$. However, since we use a real-valued weight function, an interpolation of the two solutions is possible, which we use for the boundary region in which we ``cut'' the two solutions.

We set $z(v) := 0$ if $v$ is assigned to substation $w$ in both solutions, and that substation is part of partition $P_0$ in the min-cut, that is $w\in P_0$. If however $w\in P_1$, we set $z(v) := 1$. In the case it is assigned to different substations in both solutions, we set $z(v) := 0.5$.

Finally, using $z$, we construct the potential field $p'$ and the edge cuts $H'$ of the new solution as follows. For all nodes $v\in V$, we linearly interpolate between $p_A$ and $p_B$:
\[
	p_{A,B}(v) := (1-z(v)) \cdot p_A(v) + z(v) \cdot p_B(v)
\]
This can however not directly be used as the new potential field $p'$, as it is real-valued and not a permutation of the node indices, as required by our definition of the representation. We construct a permutation by defining the value of $p'(v)$ as the rank of $v$ in the list of nodes sorted by their value $p_{A,B}$.

The edge cut list $H'$ is also constructed as a combination of $H_A$ and $H_B$. For an edge $e=(v,w)$ we determine its related solution by looking at $z(v)$ and $z(w)$: if they are both~$0$, the edge will be part of $H'$ if and only if it is part of $H_A$. Similarly, if both are $1$, it is part of $H'$ if and only if it is part of $H_B$. In all other cases, $e$ is not in $H'$.

Now, we constructed a new solution $R'=(p',H')$ out of the solutions $R_A, R_B$ based on the weight function $z$, specifying what area of which solution is being reused in the resulting solution. Note that we did not compare the performance of the two original solutions in the two regions. We rather have arbitrarily assigned one of the partitions of $G_\textrm{sad}$ to one original solution. Therefore, we also construct the corresponding opposite crossing $\hat{R}' := (\hat p',\hat H')$ by using $\hat{z}$ instead of $z$ as the weight function, where $\hat{z}(v) := 1 - z(v)$.

Note that we treat both representations $R'$ and $\hat{R}'$ equally when they are then being optimized. This is because we do not see a good way to measure which original representation is advantageous for which of the two regions. This is why each crossing results in two new solutions, as mentioned above.



\paragraph{Tuning Parameters}

For the remaining open questions, we decided to introduce the following three tuning parameters: the ratio of the running time which is used for crossings, the number of solution pairs which are crossed, and the initial temperature of a thread optimizing a crossing. For the latter parameter, we decided to express this as a multiple of the average temperature of the two original solutions which were used for constructing this crossing.

In a corresponding experiment in \autoref{ch:experiments}, we try different values for these parameters.





\section{Improvement using a Two-Level Approach}
\label{sec:twolevel}

As explained in \autoref{sec:substationassignmentproblem}, the Wind Farm Cable Layout Problem can be split into the Substation Assignment Problem and the Substation Cable Layout Problem, of which one instance per substation is to be solved.

In general, the idea of the following approach is to use the previously explained algorithm for the Wind Farm Cable Layout Problem, and additionally fine-tune the solution at the end of a Simulated Annealing computation. For this, we determine the substation assignment $q$ of the solution, and split the problem into sub-problems for the fine-tuning. The fine-tuning is then done using our Simulated Annealing algorithm for each single substation. After fine-tuning, the cable layouts of the sub-problems can simply be merged to a solution for the full farm.

As already addressed in \autoref{sec:substationassignmentproblem}, splitting the set of turbines into one subset per assigned substation does not necessarily result in connected subgraphs induced by the node subsets. This is because the path of a turbine might use another turbine as an intermediate node, but the power of that turbine is not necessarily also routed to the same substation. While such a scenario is not really typical for real world wind farms, it can occur in our theoretical model, and therefore also in a result of our algorithm. However, in order to fine-tune the subgraph of a single turbine with an instance of the Simulated Annealing algorithm, it has to be connected. If a substation assignment induces only connected subgraphs, we call it \emph{valid} for our fine-tuning. Otherwise, we call it \emph{invalid}, but we try to fix it, i.e. find a similar assignment which is valid.



\paragraph{Invalid Substation Assignments}

\begin{figure}[tb!]
	\includegraphics[width=\textwidth]{figures/representation.pdf}
	\caption{A representation and its corresponding solution. Substations are drawn as colored circles (the same colors are also used in the following figures). Node labels denote the potential values. Edge cuts are omitted to not complicate the illustration too much (they are not important for the issue we are discussing). Edge labels denote the cable throughput. Their shade indicates the cable type.}
	\label{fig:example-representation}
\end{figure}

How the substation assignment of a solution looks like is not as straight-forward as it might first sound. This is because we ``cancel out'' flows in opposite directions when decoding the turbine path representation into the cable throughput representation (see our definition of the evaluation function in \autoref{sec:algorithmbasic}). We observed in our experiments, that very often the paths of two turbines use the same edge in the opposite direction. Intuitively, one might think that this does not happen, as both turbines use the same potential field to find the best next node during the path construction. However, as we do not allow the path to form cycles, in particular the predecessor node is not considered when determining the best next node.

To illustrate the resulting problem, in \autoref{fig:example-representation} you can see the potential field of a representation as node labels. The resulting cables are drawn as differently shaded edges with their throughput (the absolute flow value) as labels. The colors in \autoref{fig:example-assignment} illustrate the substation each node was assigned (i.e. at which substation its path terminates). Note for example how the two disconnected nodes near the center, which are assigned to the yellow substation, are separated by some turbines assigned to the blue substation. At the same time, two nodes near the yellow substation are assigned to the blue substation. However, the resulting solution as visualized by the edges in \autoref{fig:example-representation} contradicts to this situation (note that the two separated yellow nodes are connected to the blue substation).

\begin{figure}[tb!]
	\vspace{0.7em}
	\includegraphics[width=\textwidth]{figures/assignment-broken.pdf}
	\caption{The substation assignment resulting from the representation from \autoref{fig:example-representation}. Each color corresponds to a substation. The edges are induced by the subset of the nodes. Note that the substation subgraphs are not connected, making the assignment invalid.}
	\label{fig:example-assignment}
\end{figure}



\paragraph{Fixing Invalid Substation Assignments}

As one possible attempt to fix this problem, we tried the following: for every node $v$ in any subgraph $G_v$, which is disconnected from its corresponding substation, we find another node as a candidate for swapping their assigned substations. For this, we suggest to select the node $w \in N(v)$ (a neighbor in the original graph) which itself has the maximum number of neighbors already being in the subgraph $G_v$ (i.e. maximizing $|N(w) \cap G_v|$). To apply the swap, we remove $v$ from the subset of nodes of $G_v$ and add it to $G_w$. At the same time we remove $w$ from $G_w$ and add it to $G_v$. The corresponding new sets of edges are the one induced by the subsets of nodes. In other words, we just pretend that $v$ and $w$ have been assigned to the opposite substations from the beginning.

This procedure is repeated for every disconnected node in any subgraph resulting from the substation assignment. The final result does not necessarily only contain connected subgraphs, as every swap not only modifies the connection of the node we want to fix, but at the same time disrupt existing connections in another subgraph. \autoref{fig:example-assignment-fixed} shows the result after four iterations of this procedure. We observe that the result is very different from the original assignment. Although this assignment is now valid, it is not suited for our two-level approach: experiments showed that when optimizing the individual substation cabling problems, their total cost does not improve the original results, and instead are much worse than that.

Yet we decided to leave this issue at that, and continue describing how the algorithm proceeds once a valid substation assignment was found. In the conclusion in \autoref{ch:conclusion}, we propose a different method for splitting the wind farm cabling problem into individual substation problems.

\begin{figure}[tb!]
	\vspace{0.7em}
	\includegraphics[width=\textwidth]{figures/assignment-fixed.pdf}
	\caption{The valid substation assignment after fixing the invalid substation assignment from \autoref{fig:example-assignment}.}
	\label{fig:example-assignment-fixed}
\end{figure}



\paragraph{Integration in Overall Algorithm}

Similar to the evolutionary algorithm, we integrate our two-level approach in our existing algorithm as a separate phase. The majority of the total available computation time is used for running our existing algorithm, which finds relevant substation assignments. During this phase, for all solutions which caused an update of the best solution seen so far, the assignment and the cost of the best solution are recorded.

After the first phase, this list of substation assignment candidates is ordered by the cost of the best solution which has been seen by the Simulated Annealing algorithm. The best of them are considered for the second phase, in which they are fine-tuned. How many substation assignments are considered is again a configuration parameter. The substation assignments are split into their individual sub-problems, each consisting of a substation and the corresponding turbines.

Note that among the different substation assignments, the same sub-problem may occur multiple times. In these cases, the assignments are identical in that region, but differ elsewhere. Taking that into account, before solving the sub-problems, we identify equal ones by their subset of nodes. Technically, this can be done using a bit vector, which can easily be hashed to allow fast lookups.



\paragraph{Future Work}

In general, we find the two-level approach a good idea to solve the Wind Farm Cable Layout Problem, since each substation network can be optimized very well with our Simulated Annealing-based heuristic. However, the difficult part seems to find a good substation assignment, that is a partitioning of the original problem into smaller sub-problems.

For this, our idea was to find multiple such substation assignments in the first level, which seem promising to analyze furhter in the second level. We need to consider multiple assignments, since finding the best substation assignment in the first level would not reduce the original problem (as the cabling for each individual substation network needs to be considered just as well). However, this first level heuristic could be replaced with a whole different partitioning method, as it is independent of which algorithm is used for the second level.

Future work on this topic could focus on finding a suitable partitioning method for these substation assignments. When still using our Simulated Annealing approach for this, we believe that a variant of \emph{Fuzzy C-means} clustering algorithm can be used to construct a substation assignment from the representation. For this, the node distance metric used in Fuzzy C-means could use the cables induced by the representation. A different method could be engineered based on multiple deep-first searches in the cable network, started at each substation and hitting each other at the partitioning borders.

Alternatively, we suggest to still use Simulated Annealing for the first level, but with a different representation: the substation assignment itself, making the above construction needless. Evaluating such a representation could then be done by solving the single substation problems it induces. An initial solution could be created with a generic partitioning or clustering algorithm, and the mutation could simply swap the assigned substations of two turbines. Basically, this method is a hierarchy of two different and independent Simulated Annealing heuristics.


