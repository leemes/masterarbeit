%% ==================
\chapter{Conclusion}
\label{ch:conclusion}
%% ==================

In the experiments from \autoref{ch:experiments}, we have seen that our heuristic is a good alternative to the MILP model for small and medium-sized wind farms. For our experimental instances with up to 450 turbines, our solution outperforms the results of Gurobi, even after a shorter amount of time. However, we see a couple of problems with our approach.

We had problems solving large wind farms with more than 450 turbines. An explanation could be that the representation structure we use in Simulated Annealing is not suited for such a large graph size, and an alternative representation should be used instead. Also, when the capacities of the substations are very tight, i.e. almost equal the total amount of power of the whole wind farm, our algorithm has difficulties with assigning the turbines to the substations. We aimed to solve both problems with the two-level approach, which first partitions the wind farm into substation networks, and then optimizing the cabling for each substation separately. However, we had difficulties with the partitioning step.

Furthermore, tuning some parameters, such as $\tau$, the velocity of the temperature drop, seems to be very critical for the algorithm's performance. However, we found it difficult to find a good strategy to choose an optimal parameter value in advance.


\paragraph{Improving our Heuristic}

We therefore suggest that our algorithm can be improved in future work with the following ideas.

First of all, a different representation could be used in the Simulated Annealing framework. For this, we primarily think of the intermediate representations we explained in \autoref{fig:intermediate-representations} on \autopageref{fig:intermediate-representations}. Furthermore, we complement the list of alternative representations by the following two ideas. The first restricts the set of edges to only trees. This is similar to our edge cuts but with a fixed cardinality such that just enough edges to form one tree per substation are remaining. The mutation would then just swap the states of an enabled and a disabled edge. The second alternative idea assigns each edge its maximal cable type (but does not say anything about whether that cable is actually used on that edge). Edges near a substation tend to use thicker cables, while edges far away only use smaller ones. This serves as a capacity constraint, and we evaluate that by optimizing the resulting CMST (for example with the Esau-Williams heuristic). The mutation would lift or reduce the cable type on an edge.

For the two-level approach, a different substation assignment construction could be used. As we saw in \autoref{sec:twolevel}, the method we use to split a solution into individual substation sub-problems does not work very well. Yet, the general idea of splitting the problem into the tasks of assigning each turbine to a substation and optimizing the single substation cablings seems to be very promising. We conclude this from our experiments in which we found out that the main complexity of the cabling problem arises from the first of these two tasks. An alternative method to construct the sub-problems could be to primarily focus on how the power is routed in the final solution, instead of to which substation each turbine path is routed.

A hybrid approach which unifies Simulated Annealing and Linear Programming could also be interesting. Here, different kinds of hybrid solutions seem to be plausible: we could first use an MILP to find a good initial solution which is then fine-tuned using our heuristic. More concretely, this could be implemented using a two-level approach similar to the one we proposed in \autoref{sec:twolevel}. A different option could be to optimize a problem instance using our Simulated Annealing heuristic, but to not use the resulting cabling, but rather assign the edges a weight depending on how often the heuristic used that edge. Given this weight, we can construct hardening constraints for the MILP (which is again a heuristic approach similar to how we restrict the edges in our experiments).

Summarizing, we think our Simulated Annealing-based heuristic serves as a good basis for developing more complex algorithms. In particular, we think that one of the different kinds of two-level approaches can be a good alternative for the linear programming approach to optimize the cabling for even larger wind farms.


\paragraph{Other Optimization Problems in Wind Farms}

We only covered a particular optimization problem when designing wind farms: finding a cable layout with minimal installation cost. We assume fixed locations for all turbines and substations. We already suggest an MILP model to find optimal locations for the substations, such that the cabling becomes cheapest. We did not have enough time to integrate this into our Simulated Annealing heuristic, however we think that this could be tried.

Besides that, we find the following other optimization problems interesting, which could be covered in future work.

In addition to not fixing the substation locations, we could optimize their quantity and capacity from a given set of substation types, similar to how we choose between different cable types for each edge.

When constraining the locations of the turbines and substations in the input, such as defining their minimum pairwise distance, the problem could become more easily to be solved. There might be an approximation scheme making use of this additional information.

Furthermore, in our problem model, only at turbines and substations cables are allowed to be merged. However, when relaxing this to allow arbitrary merge points, the cabling cost could be reduced, although the added point might imply additional costs. This is similar to the Steiner Tree problem, which is also \NP-hard \cite{steinertree}. Specifically for the design of electrical circuits, there is the Transmission Network Expansion Planning (TNEP) problem, which can be solved using Genetic Algorithms \cite{779513}.

We could furthermore forbid that cables cross each other, as this requires to bury one of the two cables deeper than the other to avoid conductive influences. Instead of forbidding, we can also charge an additional cost for an intersection. We believe that this could be incorporated into the Simulated Annealing heuristic.

