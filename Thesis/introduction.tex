%% ==============================
\chapter{Introduction}
\label{ch:introduction}
%% ==============================



A wind farm is a kind of collector system which converts kinetic energy in the form of wind to electrical energy, which is injected into the power grid (see \autoref{fig:windfarm}). For this, wind turbines play the major role in this energy conversion, however they cannot be directly attached to the power grid due to several reasons.

First, to produce a considerable amount of power, a typical wind farm consists of many turbines. They are either placed on a field, in which case we call it an \emph{onshore} wind farm, or on the sea, which is called an \emph{offshore} wind farm. Connecting all of them separately to the power grid would require many cables over long distances. Instead, the power is routed through a network of cables connecting the turbines to a grid point, which builds the access to the (high voltage) power grid.

Turbines deliver their power as alternating current (AC) in a relatively low voltage of around 33~kV, while the electricity grid usually operates at 110~kV, 220~kV or 380~kV. Transporting electrical energy over cables is always subject to power loss, which is greater for lower voltage levels and longer distances. Therefore, when connecting many turbines together and when transporting the generated power over longer distances like in an offshore farm, high voltage direct current (HVDC) is recommended to reduce the power loss. Also, for really long distances in the range of hundreds of kilometers, AC is subject to inductive and capacitive power loss. This is the case for offshore farms which are very distant from the shore. In these cases, it might be beneficial to also convert the power into high-voltage direct current using an HVDC converter. Whenever transformers are required, they are built into a so-called \emph{substation} near the turbines. From there, \emph{export cables} deliver the power to a grid point. There, yet another transformer might be installed to convert the power back into alternating current and to the voltage level of the electrical grid in which it is finally injected.

\begin{figure}[t]
	\centering
	\includegraphics[]{figures/windfarm.pdf}
	\caption{Components in a typical wind farm: turbines produce power, which is delivered to a substation by a network of transport cables connecting multiple turbines with each other. Export cables connect the substations to the power grid.}
	\label{fig:windfarm}
\end{figure}

For example, the BARD Offsore 1 Wind Farm, as shown in \autoref{fig:bard1}, is a German wind farm in the North Sea, which was built in 2010. It consists of 80 turbines palced on a region of 59 square kilometers, each with a maximum power rating of 5~MW. They add up to a total maximum rating of 400~MW for the whole project.
Each turbine has its own transformer, exporting the power in the form of 33~kV AC. They are connected to a substation in 10 circuits of 8 turbines each. The substation has two transformers, converting the 33~kV to 150~kV for export to BorWin 1 converter, another substation near the wind farm \cite{4coffshore-bard1,bard1,4coffshore-borwin1}.


\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{figures/4coffshoremap-bard1.png}
	\\
	\includegraphics[width=\linewidth]{figures/4coffshoremap-bard1-magnified.png}
	\\[1em]
	
	\caption{Screenshots from the \emph{Global Offshore Wind Farm Map} by 4C Offshore \cite{4coffshore-map}, showing the BARD Offshore 1 Wind Farm. The nearby red shaded wind farms are projects under construction, and the pink ones are being planned (as of May 2016). The first map shows the north of German and the North Sea. The second image is a zoomed-in view of BARD Offshore 1. We see its turbines, its own substation, the near BorWin 1 converter station as well as cables interconnecting the sites.}
	\label{fig:bard1}
\end{figure}


When designing such a wind farm, a lot of decisions have to be made, influencing the building and operating costs. This includes the type, the number and the location of the turbines and the substations, as well as where and which types of cables are used to connect them to the grid points.

Designing a small wind farm with minimal building costs and maximum operating efficiency can be done manually, as the number of possible combinations of choices is quite manageable. However, today's wind farms are already very large and new projects tend to get even larger, so that planning them by hand is no longer reasonable. The demand for tools automating the design decisions is growing. For example, there is \emph{Openwind}, ``a wind project design and optimization software that provides professional wind developers with the tools they need to design, analyze and optimize a wind farm'' \cite{openwind-website}.

The design problems have in common that simply brute-forcing all combinations of choices takes too much time even with today's computing power, as their number rises exponentially with the number of turbines. Such an approach would likely incur costs for computation and project delay which would exceed the savings the solution could ever provide.

In this work, we focus on the cabling problem. We consider a given number of turbines, each with a given location, as well as a given set of cable types we can use in the network for the transportation cables. First, also the substations have fixed locations and support a given number of turbines, while not considering the export cables at all. Then, we relax this problem by not fixing the locations of the substations. We are then interested in their optimal position which minimizes the total cost for the transport and export cables of the wind farm.

The work is structured as follows. In the following chapter, we outline the related work, and in \autoref{ch:preliminaries} we introduce some basic preliminaries. The problem is modeled formally in \autoref{ch:problems} as both a flow network problem and as a formulation of a mixed integer linear program (MILP). Our main contribution however is an alternative algorithm which we introduce in \autoref{ch:heuristic}, a heuristic based on Simulated Annealing. In \autoref{ch:experiments}, we evaluate our idea and compare its quality performance to the MILP as solved by the generic solver \emph{Gurobi Optimizer} \cite{gurobi}. A conclusion in \autoref{ch:conclusion} completes this work.