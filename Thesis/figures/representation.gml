Creator	"yFiles"
Version	"2.11"
graph
[
	hierarchic	1
	label	""
	directed	1
	node
	[
		id	0
		label	"124"
		graphics
		[
			x	913.1999999999999
			y	669.7
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#FF0000"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"124"
			color	"#FFFFFF"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	1
		label	"125"
		graphics
		[
			x	952.4
			y	358.9
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#00FF00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"125"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	2
		label	"122"
		graphics
		[
			x	288.1
			y	463.2
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#FFFF00"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"122"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	3
		label	"121"
		graphics
		[
			x	666.1
			y	159.4
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#CCCCFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"121"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	4
		label	"119"
		graphics
		[
			x	1252.6999999999998
			y	524.0999999999999
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#000000"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"119"
			color	"#FFFFFF"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	5
		label	"118"
		graphics
		[
			x	619.2
			y	546.5
			w	20.0
			h	20.0
			type	"ellipse"
			fill	"#0000FF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"118"
			color	"#FFFFFF"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	6
		label	"65"
		graphics
		[
			x	785.8
			y	667.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"65"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	7
		label	"58"
		graphics
		[
			x	643.7
			y	321.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"58"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	8
		label	"13"
		graphics
		[
			x	1151.1999999999998
			y	455.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"13"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	9
		label	"32"
		graphics
		[
			x	493.20000000000005
			y	627.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"32"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	10
		label	"55"
		graphics
		[
			x	1075.6
			y	162.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"55"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	11
		label	"22"
		graphics
		[
			x	757.8
			y	420.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"22"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	12
		label	"15"
		graphics
		[
			x	1197.4
			y	308.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"15"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	13
		label	"20"
		graphics
		[
			x	675.2
			y	365.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"20"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	14
		label	"106"
		graphics
		[
			x	572.3
			y	652.9000000000001
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"106"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	15
		label	"46"
		graphics
		[
			x	886.6
			y	610.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"46"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	16
		label	"10"
		graphics
		[
			x	1256.9
			y	314.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"10"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	17
		label	"117"
		graphics
		[
			x	825.6999999999999
			y	443.59999999999997
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"117"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	18
		label	"44"
		graphics
		[
			x	939.1
			y	666.8999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"44"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	19
		label	"27"
		graphics
		[
			x	997.9
			y	496.09999999999997
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"27"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	20
		label	"101"
		graphics
		[
			x	885.9
			y	375.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"101"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	21
		label	"92"
		graphics
		[
			x	447.0
			y	421.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"92"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	22
		label	"43"
		graphics
		[
			x	568.8
			y	393.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"43"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	23
		label	"31"
		graphics
		[
			x	736.8
			y	510.09999999999997
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"31"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	24
		label	"17"
		graphics
		[
			x	347.6
			y	431.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"17"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	25
		label	"84"
		graphics
		[
			x	514.9
			y	555.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"84"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	26
		label	"3"
		graphics
		[
			x	435.1
			y	248.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	27
		label	"68"
		graphics
		[
			x	831.3
			y	120.20000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"68"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	28
		label	"71"
		graphics
		[
			x	900.6
			y	125.10000000000001
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"71"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	29
		label	"50"
		graphics
		[
			x	954.5
			y	421.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"50"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	30
		label	"33"
		graphics
		[
			x	762.0
			y	348.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"33"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	31
		label	"8"
		graphics
		[
			x	990.1999999999999
			y	137.70000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	32
		label	"85"
		graphics
		[
			x	676.6
			y	628.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"85"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	33
		label	"96"
		graphics
		[
			x	362.3
			y	496.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"96"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	34
		label	"116"
		graphics
		[
			x	1191.1
			y	513.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"116"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	35
		label	"23"
		graphics
		[
			x	1074.1999999999998
			y	565.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"23"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	36
		label	"75"
		graphics
		[
			x	646.5
			y	437.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"75"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	37
		label	"25"
		graphics
		[
			x	729.8
			y	178.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"25"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	38
		label	"113"
		graphics
		[
			x	592.6
			y	574.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"113"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	39
		label	"64"
		graphics
		[
			x	568.8
			y	244.10000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"64"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	40
		label	"29"
		graphics
		[
			x	468.0
			y	316.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"29"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	41
		label	"73"
		graphics
		[
			x	1254.1
			y	410.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"73"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	42
		label	"111"
		graphics
		[
			x	624.1
			y	134.20000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"111"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	43
		label	"104"
		graphics
		[
			x	1166.6
			y	393.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"104"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	44
		label	"120"
		graphics
		[
			x	903.4
			y	302.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"120"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	45
		label	"51"
		graphics
		[
			x	1004.9
			y	365.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"51"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	46
		label	"18"
		graphics
		[
			x	800.5
			y	533.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"18"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	47
		label	"95"
		graphics
		[
			x	998.6
			y	651.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"95"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	48
		label	"41"
		graphics
		[
			x	515.6
			y	158.70000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"41"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	49
		label	"72"
		graphics
		[
			x	1061.6
			y	242.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"72"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	50
		label	"97"
		graphics
		[
			x	698.3
			y	123.00000000000001
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"97"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	51
		label	"115"
		graphics
		[
			x	1278.6
			y	521.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"115"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	52
		label	"53"
		graphics
		[
			x	614.3
			y	512.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"53"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	53
		label	"26"
		graphics
		[
			x	857.1999999999999
			y	246.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"26"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	54
		label	"69"
		graphics
		[
			x	780.1999999999999
			y	288.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"69"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	55
		label	"54"
		graphics
		[
			x	500.90000000000003
			y	432.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"54"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	56
		label	"78"
		graphics
		[
			x	652.1
			y	265.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"78"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	57
		label	"1"
		graphics
		[
			x	420.4
			y	190.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	58
		label	"12"
		graphics
		[
			x	1049.6999999999998
			y	510.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"12"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	59
		label	"88"
		graphics
		[
			x	319.6
			y	539.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"88"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	60
		label	"37"
		graphics
		[
			x	1146.3
			y	335.1
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"37"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	61
		label	"56"
		graphics
		[
			x	258.70000000000005
			y	324.6
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"56"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	62
		label	"5"
		graphics
		[
			x	1266.0
			y	257.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	63
		label	"34"
		graphics
		[
			x	850.9
			y	491.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"34"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	64
		label	"89"
		graphics
		[
			x	883.1
			y	671.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"89"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	65
		label	"83"
		graphics
		[
			x	1070.6999999999998
			y	626.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"83"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	66
		label	"99"
		graphics
		[
			x	1000.0
			y	276.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"99"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	67
		label	"49"
		graphics
		[
			x	1336.0
			y	393.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"49"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	68
		label	"21"
		graphics
		[
			x	525.4
			y	288.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"21"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	69
		label	"16"
		graphics
		[
			x	468.70000000000005
			y	505.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"16"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	70
		label	"81"
		graphics
		[
			x	768.3
			y	125.80000000000001
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"81"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	71
		label	"57"
		graphics
		[
			x	340.6
			y	361.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"57"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	72
		label	"4"
		graphics
		[
			x	1128.1
			y	188.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	73
		label	"60"
		graphics
		[
			x	1130.1999999999998
			y	588.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"60"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	74
		label	"9"
		graphics
		[
			x	805.4
			y	393.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"9"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	75
		label	"114"
		graphics
		[
			x	659.1
			y	184.60000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"114"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	76
		label	"47"
		graphics
		[
			x	465.20000000000005
			y	583.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"47"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	77
		label	"61"
		graphics
		[
			x	947.5
			y	519.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"61"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	78
		label	"112"
		graphics
		[
			x	584.9
			y	446.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"112"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	79
		label	"0"
		graphics
		[
			x	1196.6999999999998
			y	255.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"0"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	80
		label	"91"
		graphics
		[
			x	1303.8000000000002
			y	454.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"91"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	81
		label	"86"
		graphics
		[
			x	402.20000000000005
			y	452.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"86"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	82
		label	"6"
		graphics
		[
			x	409.20000000000005
			y	298.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	83
		label	"109"
		graphics
		[
			x	625.5
			y	663.3999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"109"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	84
		label	"108"
		graphics
		[
			x	1222.6
			y	563.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"108"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	85
		label	"24"
		graphics
		[
			x	939.8
			y	176.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"24"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	86
		label	"36"
		graphics
		[
			x	593.3
			y	302.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"36"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	87
		label	"77"
		graphics
		[
			x	1314.2999999999997
			y	326.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"77"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	88
		label	"102"
		graphics
		[
			x	948.1999999999999
			y	611.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"102"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	89
		label	"66"
		graphics
		[
			x	786.5
			y	200.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"66"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	90
		label	"93"
		graphics
		[
			x	738.9
			y	643.0999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"93"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	91
		label	"67"
		graphics
		[
			x	362.3
			y	232.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"67"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	92
		label	"98"
		graphics
		[
			x	588.4
			y	185.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"98"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	93
		label	"28"
		graphics
		[
			x	748.7
			y	571.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"28"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	94
		label	"7"
		graphics
		[
			x	654.9
			y	572.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	95
		label	"35"
		graphics
		[
			x	1021.6999999999999
			y	187.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"35"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	96
		label	"100"
		graphics
		[
			x	300.0
			y	463.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"100"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	97
		label	"14"
		graphics
		[
			x	484.1
			y	219.60000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"14"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	98
		label	"105"
		graphics
		[
			x	939.1
			y	358.2
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"105"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	99
		label	"52"
		graphics
		[
			x	414.1
			y	375.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"52"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	100
		label	"59"
		graphics
		[
			x	320.3
			y	290.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"59"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	101
		label	"42"
		graphics
		[
			x	1131.6
			y	515.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"42"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	102
		label	"70"
		graphics
		[
			x	540.8
			y	609.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"70"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	103
		label	"123"
		graphics
		[
			x	259.40000000000003
			y	402.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"123"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	104
		label	"11"
		graphics
		[
			x	519.8
			y	364.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"11"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	105
		label	"2"
		graphics
		[
			x	1009.1
			y	435.9
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	106
		label	"103"
		graphics
		[
			x	778.1
			y	469.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"103"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	107
		label	"110"
		graphics
		[
			x	707.4
			y	320.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"110"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	108
		label	"45"
		graphics
		[
			x	619.2
			y	370.79999999999995
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"45"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	109
		label	"62"
		graphics
		[
			x	725.5999999999999
			y	244.10000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"62"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	110
		label	"94"
		graphics
		[
			x	897.1
			y	433.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"94"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	111
		label	"39"
		graphics
		[
			x	704.5999999999999
			y	409.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"39"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	112
		label	"90"
		graphics
		[
			x	681.5
			y	496.09999999999997
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"90"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	113
		label	"79"
		graphics
		[
			x	845.3
			y	334.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"79"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	114
		label	"38"
		graphics
		[
			x	1123.9
			y	265.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"38"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	115
		label	"40"
		graphics
		[
			x	388.9
			y	555.5999999999999
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"40"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	116
		label	"63"
		graphics
		[
			x	1015.4
			y	580.8
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"63"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	117
		label	"30"
		graphics
		[
			x	1221.1999999999998
			y	357.5
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"30"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	118
		label	"76"
		graphics
		[
			x	936.3
			y	249.70000000000002
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"76"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	119
		label	"19"
		graphics
		[
			x	856.5
			y	171.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"19"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	120
		label	"87"
		graphics
		[
			x	824.3
			y	634.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"87"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	121
		label	"107"
		graphics
		[
			x	1202.3
			y	445.7
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"107"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	122
		label	"82"
		graphics
		[
			x	1105.6999999999998
			y	431.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"82"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	123
		label	"74"
		graphics
		[
			x	1065.8
			y	348.4
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"74"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	124
		label	"48"
		graphics
		[
			x	543.6
			y	507.3
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"48"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	node
	[
		id	125
		label	"80"
		graphics
		[
			x	876.1
			y	557.0
			w	20.0
			h	20.0
			type	"rectangle"
			fill	"#FFFFFF"
			outline	"#000000"
		]
		LabelGraphics
		[
			text	"80"
			fontSize	11
			fontName	"Dialog"
			anchor	"c"
		]
	]
	edge
	[
		source	125
		target	15
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	124
		target	25
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	123
		target	45
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	122
		target	43
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	121
		target	34
		label	"9"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"9"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	120
		target	64
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	119
		target	27
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	118
		target	44
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	117
		target	43
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	116
		target	88
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	115
		target	33
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	114
		target	49
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	113
		target	20
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	112
		target	5
		label	"10"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"10"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	111
		target	36
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	110
		target	20
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	109
		target	107
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	108
		target	78
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	107
		target	56
		label	"7"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	106
		target	17
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	106
		target	23
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	105
		target	29
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	104
		target	22
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	103
		target	2
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	102
		target	38
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	101
		target	34
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	100
		target	61
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	99
		target	21
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	98
		target	1
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	97
		target	26
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	96
		target	2
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	95
		target	49
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	94
		target	5
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	93
		target	90
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	92
		target	42
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	91
		target	71
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	90
		target	6
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	89
		target	70
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	88
		target	0
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	87
		target	67
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	86
		target	7
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	85
		target	28
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	84
		target	4
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	83
		target	14
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	82
		target	40
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	81
		target	33
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	80
		target	51
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	79
		target	12
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	78
		target	36
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	77
		target	88
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	76
		target	25
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	75
		target	3
		label	"9"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"9"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	74
		target	17
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	72
		target	10
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	70
		target	50
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	69
		target	25
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	68
		target	39
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	66
		target	1
		label	"7"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	65
		target	47
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	64
		target	0
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	63
		target	17
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	60
		target	43
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	59
		target	2
		label	"9"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"9"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	58
		target	19
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	55
		target	21
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	53
		target	44
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	52
		target	5
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	51
		target	4
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	50
		target	3
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	45
		target	1
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	44
		target	1
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"thead"
		]
	]
	edge
	[
		source	42
		target	3
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	38
		target	5
		label	"10"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"10"
			fontSize	12
			fontName	"Dialog"
			model	"null"
			position	"null"
		]
	]
	edge
	[
		source	34
		target	4
		label	"11"
		graphics
		[
			fill	"#000000"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"11"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	29
		target	1
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	28
		target	27
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	18
		target	0
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	15
		target	0
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	8
		target	121
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	43
		target	121
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	41
		target	121
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	6
		target	120
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	12
		target	117
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	23
		target	112
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	36
		target	112
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	17
		target	110
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	13
		target	107
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	30
		target	107
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	7
		target	107
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	54
		target	107
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	11
		target	106
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	46
		target	106
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	61
		target	103
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	71
		target	103
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	14
		target	102
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"stail"
		]
	]
	edge
	[
		source	40
		target	99
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	20
		target	98
		label	"7"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"7"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	24
		target	96
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	31
		target	95
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	39
		target	92
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	48
		target	92
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	57
		target	91
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	26
		target	91
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	47
		target	88
		label	"3"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"3"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	16
		target	87
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	62
		target	87
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	73
		target	84
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	32
		target	83
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	21
		target	81
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	67
		target	80
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	22
		target	78
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"shead"
		]
	]
	edge
	[
		source	19
		target	77
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	9
		target	76
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	56
		target	75
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	27
		target	70
		label	"4"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"4"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	49
		target	66
		label	"6"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"6"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	35
		target	65
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	33
		target	59
		label	"8"
		graphics
		[
			fill	"#555555"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"8"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	37
		target	50
		label	"1"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"1"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	10
		target	49
		label	"2"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"2"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
	edge
	[
		source	25
		target	38
		label	"5"
		graphics
		[
			fill	"#AAAAAA"
			targetArrow	"standard"
		]
		LabelGraphics
		[
			text	"5"
			fontSize	12
			fontName	"Dialog"
			model	"six_pos"
			position	"tail"
		]
	]
]
