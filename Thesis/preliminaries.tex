%% ==============
\chapter{Preliminaries}
\label{ch:preliminaries}
%% ==============

We define the following notations for sets of numbers. As commonly, the set of \emph{real numbers} is denoted by \reals. With \realsgeq, we mean the set of \emph{non-negative real numbers} including zero.
Intervals of real numbers including the boundaries are denoted with square brackets: $[a,b] :=  \{ x \in \reals : a \leq x \leq b \}$. When using parenthesis, the boundaries are excluded: $(a,b) :=  \{ x \in \reals : a < x < b \}$. Mixed forms are also possible: $[a, b) := \{ x \in \reals : a \leq x < b \}$.

The set of \emph{integers} is denoted by \wholes, and likewise we denote the set of \emph{non-negative integers} as \wholesgeq.
The set of \emph{natural numbers} is denoted by \naturals, which does not include zero. To denote the first $k$ natural numbers, usually used for indexing a family of $k$ variables, we use the symbol $\indices k := \{1, \dots, k\}$.


\section{Flow Networks}
\label{sec:flownetworks}

In general, a (simple) \emph{graph} $G = (V,E)$ is an abstract structure representing a set of objects, which we call \emph{nodes}, and pairwise connections between them, which we call \emph{edges}. The set of nodes is denoted as $V$ and the set of edges as $E$. When the graph is \emph{directed}, the edges have a particular direction. For the two nodes $v, w \in V$, the edge connecting them is denoted as the pair $(v,w) \in E$. It is distinct from the opposite edge $(w,v) \neq (v,w)$, although both can be in $E$ at the same time. When the graph is \emph{undirected}, edges do not have a particular direction. In this case, an edge connecting $v$ and $w$ is written as the set $\{v, w\} \in E$.

We introduce the \emph{neighborhood} $N(v)$ of $v \in V$ as the set of \emph{adjacent} nodes that are connected to $v$ via an edge. These edges are called \emph{incident} to $v$ and are denoted as $\delta(v)$. Note that in the case of a directed graph, these sets include both incoming and outgoing edges incident to $v$. To make a distinction of these two cases, we split $\delta(v)$ in the sets of incoming edges $\delta_-(v)$ and outgoing edges $\delta_+(v)$. Similarly, the neighborhood $N(v)$ is split into $N_-(v)$ and $N_+(v)$, which denote nodes connected to $v$ via incoming or outgoing edges, respectively.

In a directed graph $G=(V,E)$, a \emph{flow} $f : E \rightarrow \reals$ assigns each edge a \emph{flow value}. When it is negative for an edge $e = (v,w)$, it represents a flow in the edge's opposite direction, that is from $w$ to $v$, instead of from $v$ to $w$. When dealing with undirected graphs as the original input in an algorithm using a flow, it is common to define a corresponding directed graph, in which the edge directions are chosen arbitrarily but fixed throughout the algorithm, to have a notation of flow direction.

Given the flow $f$, the \emph{excess} $\excess(v)$ of node $v\in V$ is defined as the sum of incoming minus outgoing flow over all incident edges:
\begin{equation}
	\excess(v) :=
		\underbrace{ \sum_{e\in \delta_-(v)} f(e) }_{\textrm{incoming into $v$}}
		-
		\underbrace{ \sum_{e\in \delta_+(v)} f(e) }_{\textrm{outgoing from $v$}}
\end{equation}
Observe that the sum of excesses of all nodes is always zero for any $f$, that is $\sum_{v\in V} \excess(v) = 0$. This is because the flow value of every edge in the graph contributes to the whole sum once positively and once negatively.

In the following, we define a flow network problem, in which a flow is to be found under certain constraints.
Note that different kinds of definitions for flow networks exist in the literature, and in the following we describe the one we use in our algorithms\footnote{In this work, we use a many-sources many-sinks network, but the most common definition of flow networks used in the literature has a single source and a single sink node. However, the former can easily be transformed into the latter by adding a dummy source and a dummy sink, which are connected to all sources and sinks, respectively.}.
The graph $G = (V,E)$, the function $u : E \rightarrow \realsgeq$ which assigns each edge $e\in E$ a \emph{capacity} $u(e)$, and the function $\balance : V \rightarrow \reals$ which assigns each node $v\in V$ a \emph{balance} $\balance(v)$, form the \emph{flow network} $(G,u,\balance)$. Depending on $\balance(v)$, a node is a \emph{source} (also called producer) when $\balance(v) < 0$, or a \emph{sink} (also called consumer) when $\balance(v) > 0$. For nodes which are neither sources nor sinks, $\balance(v) = 0$. The corresponding \emph{flow problem} asks for a flow from sources to sinks under two kinds of constraints, resulting from $u$ and $\balance$.

A flow $f$ is a \emph{feasible} solution to the flow network $(G,u,\balance)$, if for all edges $e \in E$ the absolute value of the flow $|f(e)|$ does not exceed the edge's capacity $u(e)$. We call this the \emph{edge capacity constraint}:

\begin{equation}
	\label{eq:flow1}
	-u(e) \leq f(e) \leq u(e)
\end{equation}

Additionally, $f$ has to satisfy the \emph{flow conservation constraint} for all nodes $v\in V$, which states that its excess shall be equal to the given balance:

\begin{equation}
	\label{eq:flow2}
	\excess(v) = \balance(v)
\end{equation}


\paragraph{Minimum-Cost Flow Problem}

The above flow problem $(G,u,\balance)$ is a decision problem: there either exists a feasible flow $f : E \rightarrow \reals$, or the network is infeasible. We formulate a corresponding optimization problem, the \emph{minimum-cost flow problem} $(G,u,\balance,c_{(\cdot)})$, asking about an optimal solution, if any exists.

For this, the additional parameter $c_{(\cdot)}$ in a problem instance specifies for any edge $e\in E$ a corresponding edge cost function $c_e : \reals \rightarrow \realsgeq$. Given a flow $f$, the non-negative cost of edge $e$ is given by $c_e(f(e))$. For symmetry reasons, we require that the cost for a negative flow equals the cost for the corresponding positive flow, that is $c_e(-x) = c_e(x)$ for all $x\in \reals$.

The objective function which is to be minimized is the total cost $c_\textrm{total}(f)$, that is the sum of all edge costs for a flow $f$:

\begin{equation}
	c_\textrm{total}(f) := \sum_{e\in E} c_e(f(e))
\end{equation}

The structure of the edge cost functions is essential for the complexity of the flow network problem. If they are linear functions, then the optimization problem is solvable in polynomial time \cite{minoux, gt-namfp-88}. The same holds for instances in which the cost functions are contiguous and convex functions composed of several linear segments. Such an instance can be transformed into an instance with just linear cost functions by copying the edges such that every copy of the input edge models a linear segment of the original cost function.

As we will see later, the cost functions we deal with are non-convex. In this case, the flow network problem is \NP-hard. This is because we can solve an instance of the Capacitated Minimum Spanning Tree (CMST) problem, which is known to be \NP-hard \cite{Jothi:2005:AAC:1103963.1103967}, with an algorithm solving the flow network problem as explained above. We migrate the input graph as well as the edge capacities of the CMST instance into our model. We set the balance of the root node of the CMST instance to $n-1$ (where $n$ is the number of nodes in the CMST instance), while setting it to $-1$ for all other nodes. Furthermore, we set the cost function of all edges to $0$ for a flow value of $0$, and for any non-zero flow we set it to the edge cost in the CMST instance. The total cost of our flow network corresponds to the cost of the CMST, and the edges which have a non-zero flow value are the spanning tree we search for.



\section{Simulated Annealing}
\label{sec:preliminaries:simulatedannealing}

Optimization problems can be solved exactly or, whenever an exact solution takes too much time to be computed, approximated or solved heuristically. In general, the solution of a heuristic algorithm does not perform as well as the exact solution, when compared by their cost. However, they usually outperform exact solvers in their computation time a lot, especially when the problem is \NP-hard.

In general, a heuristic solver usually searches for a good solution in the solution space in a stochastic way. Simulated Annealing is one such heuristic, and is the base of our algorithms in this work. Its idea comes from the physical process of cooling down a glowing metal: its molecular structure can be altered while being hot enough. When cooling it down very quickly, its structure is frozen, not giving it any time to get into an energy-optimal form. However, when cooling the metal slowly, molecules still have time to move around such that the energy of the material is minimized: a crystal structure is formed. In nature, molecules move around randomly, and the material can have an intermediate state with higher energy before getting into a state with better energy. The probability for this to happen decreases with decreasing temperature.

This is the core idea of Simulated Annealing. The overall algorithm for an abstract problem is shown in \autoref{algo:sa}. Given an instance $I$, we first create an initial solution $R$ by the initialization function (\autoref{algo:sa:init}). It is repeatedly changed slightly over time by the mutation function (\autoref{algo:sa:mutate}). The energy of a solution is determined by an evaluation function (\autoref{algo:sa:eval1}, \autoref{algo:sa:eval2}). Whenever a change results in lower energy, it is accepted as the new solution candidate (\autoref{algo:sa:acceptbetter}). However, when it raises the energy, it is only accepted with some probability. The probability depends on a temperature parameter: the lower the temperature, the less the probability of accepting a worse solution (\autoref{algo:sa:probability}). Also, the probability depends on how much worse the new solution is. The temperature is lowered over time (\autoref{algo:sa:tempchange}).


\begin{algorithm}
	\caption{Simulated Annealing}
	\label{algo:sa}
	
	\SetFuncSty{textsc}
	\DontPrintSemicolon
	\KwIn{Instance $I$, Initial temperature $T_0$, Cooling speed $\tau$}
	\KwOut{Near-optimal solution $R$ of $I$}
	
	\SetKwFunction{Init}{Init}
	\SetKwFunction{Eval}{Eval}
	\SetKwFunction{Mutate}{Mutate}
	
	$T \gets T_0$\;
	$R \gets \Init{$I$}$\tcc*{initial solution} \label{algo:sa:init}
	$E \gets \Eval{$I,R$}$\; \label{algo:sa:eval1}
	\Repeat{$R$ is well enough}{
		$R' \gets \Mutate{$I,R$}$	\tcc*{new solution candidate} \label{algo:sa:mutate}
		$E' \gets \Eval{$I,R'$}$\; \label{algo:sa:eval2}
		\eIf{$E' \leq E$}{
			$a \gets 1$\tcc*{always accept better solutions} \label{algo:sa:acceptbetter}
		}{
			$p \gets \exp(-T^{-1}\cdot \frac{E'-E}{E})$\tcc*{acceptance probability formula} \label{algo:sa:probability}
			$a \gets \textrm{$1$ with probability $p$, $0$ otherwise}$ \;
		}
		\If {$a = 1$}{
			$(R,E) \gets (R',E')$\tcc*{accept solution $R'$}
		}
		$T \gets (1 - \tau) \cdot T$\tcc*{lower the temperature} \label{algo:sa:tempchange}
	}
\end{algorithm}


Before using Simulated Annealing to solve a concrete optimization problem, we have to define a representation of a solution, the corresponding evaluation function, as well as an initialization and a mutation function.

The \emph{representation} is an indirect way to fully describe a solution candidate. Its structure may be very different from the solution itself, but deriving it from a representation needs to be deterministic and efficient. Depending on the concrete problem, describing solutions indirectly by a representation can have the advantage that a mutation can be defined more easily.

An \emph{evaluation function} computes the energy of a solution candidate given its representation. This may be split into two separate steps: decoding a solution from the representation and calculating the energy of the resulting solution.

Two randomized functions in the Simulated Annealing algorithm define the search space exploration. An initial solution representation is created by the \emph{initialization function}. Starting from there, a \emph{mutation function} is applied repeatedly: it forms a new solution representation by changing the current one only slightly. For any input, the set of all possible outputs of this function defines the \emph{neighborhood} of the given representation.

The initialization function does not necessarily need to be randomized. However, if it is not, or if it uses a clever heuristic to start with an initial ``guess'' of a good solution, it is still important to design the mutation function such that it is still possible to escape from the local optimum of the initial solution quite easily. Otherwise, the simulation might stay at the local optimum of the initial solution, instead of exploring solutions near a different local optimum as well. Ideally, the initial solution is irrelevant for the quality of the result, and at the very most only an optimization of the running time.

The ability to escape any local optimum is mainly affected by the design of the mutation function. It defines a \emph{search graph}, in which representations are nodes and neighbors are connected by edges. In order to make every representation reachable, starting from anywhere in this graph, the search graph needs to be connected. Also, the number of mutations required to make all representations reachable should be kept low. This can be achieved by designing the mutation function such that it sometimes changes the representation only slightly, and sometimes significantly, chosen randomly.



\paragraph{The Temperature Curve}

The main idea of Simulated Annealing is to define a \emph{temperature parameter} $T$, which is lowered over time, to simulate a natural cooling process. This parameter influences the probability of accepting a worse solution intermediately, hoping that a better solution is found later --- a process which is required to escape from local optima.

Typical Simulated Annealing implementations resemble a natural temperature curve starting with a fixed temperature $T_0$. At any point in time, the temperature change is proportional to the difference of the temperatures of the object and the environment. This results in a hyperbolic temperature curve. However, how fast the object is cooling down is determined by the proportionality factor, basically the thermal conductivity and capacity of the object. Analogously, we introduce the parameter $\tau$ which determines the temperature delta $\tau \cdot T$ for every iteration and thus how fast the simulation is cooled down (see \autoref{algo:sa:tempchange} in \autoref{algo:sa}).

The problem with such a curve is that its parameters $T_0$ and $\tau$ have to be tuned depending on each instance to give great results: very large instances require a slower cooling procedure, since the solution space is much larger and more complicated compared to that of smaller instances. However, when the total running time of the algorithm is to be fixed, the parameter should be higher for larger instances. This is because each iteration takes more time in this case, reducing the total number of iterations the algorithm is running. That means that to reach the same final temperature, the parameter $\tau$ needs to be larger. In the experiments in \autoref{ch:experiments}, we analyze this in detail.



\paragraph{The Acceptance Probability Formula}

In a Simulated Annealing algorithm, by applying the mutation function we obtain a neighbor solution which is then evaluated. The energy of this new candidate is compared to the current solution: if the energy was decreased, i.e. a better solution was found, it is definitely accepted as the new solution for the next iteration. Otherwise, the probability of accepting the candidate is
\[
P_\textrm{abs} = \exp(-T^{-1} \cdot \Delta E)
\]
where $\Delta E = E'-E$ is the energy difference, which is positive when the new solution is worse. We call this the \emph{absolute} probability formula.

The problem with the definition from above is that the magnitude of $\Delta E$ depends on the magnitude of $E$, which in turn highly depends on the problem instance. It would be difficult to define a useful value for $T_0$ leading to acceptance probabilities suitable for the concrete instance. To handle this, we \emph{normalize} the energy difference by dividing it by the energy of the current solution. We call this the \emph{normalized} probability formula:
\[
P_\textrm{norm} = \exp(-T^{-1} \cdot \frac{\Delta E}{E})
\]
This is the formula we use in our algorithm (\autoref{algo:sa:probability}).


