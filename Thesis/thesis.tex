\documentclass{thesisclass}
% Based on thesisclass.cls of Timo Rohrberg, 2009
% ----------------------------------------------------------------
% Thesis - Main document
% ----------------------------------------------------------------


%% -------------------------------
%% |  Information for PDF file   |
%% -------------------------------
\hypersetup{
 pdfauthor={Sebastian Lehmann},
 pdftitle={Simulated Annealing-Based Heuristics for Wind Farm Cabling Problems},
 pdfsubject={Wind Farm Cabling Problems},
 pdfkeywords={wind farm, renewables, energy, cabling, network, transportation, optimization, simulated annealing, heuristic, mixed integer linear programming}
}


%% ---------------------------------
%% | Information about the thesis  |
%% ---------------------------------

\newcommand{\myname}{Sebastian Lehmann}
\newcommand{\mytitle}{Simulated Annealing-Based Heuristics for Wind Farm Cabling Problems}
\newcommand{\myinstitute}{Institute of Theoretical Informatics}

\newcommand{\reviewerone}{Prof. Dr. Dorothea Wagner}
\newcommand{\reviewertwo}{Prof. Dr. rer. nat. Peter Sanders}
\newcommand{\advisor}{Dr. rer. nat. Ignaz Rutter}
\newcommand{\advisortwo}{Franziska Wegner}

\newcommand{\timestart}{1st December 2015}
\newcommand{\timeend}{11th May 2016}


%% ---------------------------------
%% | Packages                      |
%% ---------------------------------

\usepackage{xspace}
\usepackage{amsmath}
\usepackage[color,pdftex,leftbars]{changebar}
\usepackage{transparent}
\usepackage{paralist}
\usepackage{enumitem}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{complexity}


%% ---------------------------------
%% | Commands                      |
%% ---------------------------------

\providecommand{\DontPrintSemicolon}{\dontprintsemicolon}

\newtheorem{definition}{Definition} \numberwithin{definition}{chapter}
\newtheorem{theorem}[definition]{Theorem}
\newtheorem{lemma}[definition]{Lemma}
\newtheorem{corollary}[definition]{Corollary}
\newtheorem{conjecture}[definition]{Conjecture}
\newtheorem{claim}[definition]{Claim}

% notation
\newcommand{\reals}{\ensuremath{\mathbb{R}}\xspace}
%\newcommand{\realsg}{\ensuremath{\mathbb{R}_{>0}}\xspace}
\newcommand{\realsgeq}{\ensuremath{\mathbb{R}_{\geq0}}\xspace}
\newcommand{\wholes}{\ensuremath{\mathbb{Z}}\xspace}
\newcommand{\wholesgeq}{\ensuremath{\mathbb{Z}_{\geq0}}\xspace}
\newcommand{\naturals}{\ensuremath{\mathbb{N}}\xspace}
\newcommand{\indices}[1]{\ensuremath{\mathbb{N}_{\leq #1}}\xspace}
\newcommand{\realszeroone}{\ensuremath{[0,1]}}
\newcommand{\realsexzeroone}{\ensuremath{(0,1]}}


\newcommand{\bigO}{\ensuremath{\mathcal{O}}\xspace}

\DeclareMathOperator{\ex}{ex}
\newcommand{\excess}{\ensuremath{\ex}\xspace}
\newcommand{\balance}{\ensuremath{b}\xspace}
\newcommand{\varcable}[3]{\ensuremath{x_{#1#2#3}}\xspace}
\newcommand{\varflow}[2]{\ensuremath{f_{#1#2}}\xspace}
\newcommand{\varccost}[3]{\ensuremath{c_{#1#2#3}}\xspace}
\newcommand{\varccap}[3]{\ensuremath{u_{#1#2#3}}\xspace}
\newcommand{\varscap}[1]{\ensuremath{m_{#1}}\xspace}



\addto\extrasenglish{%   
	\def\sectionautorefname{Section}%
	\def\subsectionautorefname{Section}%
	\def\chapterautorefname{Chapter}%
	\def\equationautorefname{equation}%
	\def\algorithmautorefname{Algorithm}%
	%\def\theoremautorefname{Claim}%
} 

\def\includeplot#1{
	% Solve the relative path problem by recursively included eps files from the gnuplot's tex files
	\let\igx\includegraphics
	%\def\includegraphics##1{\igx{plots/##1-eps-converted-to.pdf}}
	\def\includegraphics##1{\igx{plots/##1}}
	\input{plots/#1}
}

\setlength{\floatsep}{20.0pt plus 2.0pt minus 2.0pt}



%% --------------------------------
%% | Settings for word separation |
%% --------------------------------
% Help for separation:
% In german package the following hints are additionally available:
% "- = Additional separation
% "| = Suppress ligation and possible separation (e.g. Schaf"|fell)
% "~ = Hyphenation without separation (e.g. bergauf und "~ab)
% "= = Hyphenation with separation before and after
% "" = Separation without a hyphenation (e.g. und/""oder)

% Describe separation hints here:
\hyphenation{
}


%% ------------------------
%% |    Including files   |
%% ------------------------
% Only files listed here will be included!
% Userful command for partially translating the document (for bug-fixing e.g.)
%\includeonly{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here, main documents begins %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% Remove the following line for German text
\selectlanguage{english}

\frontmatter
\pagenumbering{roman}
\include{titlepage}
\blankpage

%% -------------------------------
%% |   Statement of Authorship   |
%% -------------------------------

\thispagestyle{plain}

\vspace*{\fill}

\centerline{\bf Statement of Authorship}

\vspace{0.25cm}

I hereby declare that this document has been composed by myself and describes my own work, unless otherwise acknowledged in the text.

\vspace{2.5cm}

\hspace{0.25cm} Karlsruhe, May 20, 2016

\vspace{2cm}

\blankpage

%% -------------------
%% |   Abstract      |
%% -------------------

\thispagestyle{plain}

\begin{addmargin}{0.5cm}

\centerline{\bf Abstract}

In a wind farm, the cables connecting turbines and substations with the power grid significantly contribute to its installation cost. Minimizing the cabling cost traces back to a capacitated network flow problem similar to those arising in areas such as logistics and transport. In this work, we study the following cable layout problem: In the wind farm, the locations of turbines and substations are fixed, and they should be connected with cables. We are given a set of cable types, which differ in their cost per meter and in the amount of power they support. We are interested in the cable network minimizing the cabling cost, such that the farm is connected properly.

We formally model this problem as a capacitated minimum-cost flow problem with non-convex edge cost functions. This problem is \NP-hard which means that, as matters stand, no polynomial time algorithm is known. We also model the cable layout problem as a mixed integer linear program (MILP) which can be solved using a generic optimizer. As an alternative, we propose a heuristic based on Simulated Annealing, which we qualitatively compare with the MILP solved with Gurobi Optimizer, a generic optimization program.

In our experiments, it turns out that our heuristic is a good alternative to the MILP model for small and medium-sized instances. For wind farms with up to 450 turbines, our solution outperforms the results of Gurobi in the majority of cases. We propose different modifications to our heuristic, of which only some were useful to achieve these results. We conclude the work with different additional ideas for improvements which could be tried to make the heuristic suitable for even larger wind farms.


\vskip 1cm

\centerline{\bf Deutsche Zusammenfassung}

Die Konstruktionskosten eines Windparks hängen unter anderem davon ab, wie die Windräder und die Umspannwerke miteinander verkabelt sind. Es ist daher von großem Interesse, die Kosten für diese Verkabelung auf ein Minimum zu beschränken, ohne die gegebenen Vorausetzungen zu verletzen. Dabei stehen uns mehrere Kabeltypen zur Verfügung, welche für unterschiedliche Ströme ausgelegt sind und verschiedene Konstruktionskosten verursachen.

Das Verkabelungsproblem lässt sich auf ein kapazitives Flussproblem reduzieren, welches auch für Transportprobleme wie zum Beispiel in der Logistik Anwendung findet. Auch dort hat man verschiedene Transportmittel zur Auswahl, welche sich in ihrer Kapazität und den Kosten unterscheiden. In beiden Szenarien verhalten sich die Kantenkosten dabei nicht-konvex, was das Problem \NP-schwer macht. Nach dem aktuellen Stand der Forschung ist demnach kein Algorithmus bekannt, welcher dieses in polynomieller Laufzeit löst. Gleichzeitig lässt sich das Verkabelungsproblem auch als gemischtes ganzzahliges lineares Programm (engl.: mixed integer linear program, MILP) formulieren. Dieses ist zwar immer noch \NP-schwer, jedoch lässt es sich mit einer generischen Optimierungssoftware wie Gurobi approximativ lösen.

Als Alternative schlagen wir eine Heuristik vor, welche auf Simulated Annealing, einer physikalisch motivierten Optimierungsheuristik, basiert. Wie wir in Experimenten zeigen, werden damit Ergebnisse erreicht, welche gegen diejenigen des Gurobi Optimizer für kleine und mittelgroße Windparks konkurrieren kann. Für bis zu 450 Turbinen erreicht unsere Heuristik bessere Ergebnisse in kürzerer Zeit. Wir stellen Verbesserungsvorschläge vor, mit welchen wir diese Ergebnisse erzielt haben. Wir schließen die Arbeit mit weiteren Vorschlägen ab, welche auch für noch größere Windparks Anwendung finden könnten.


\end{addmargin}

\blankpage

%% -------------------
%% |   Directories   |
%% -------------------

\tableofcontents
\blankpage

%% -----------------
%% |   Main part   |
%% -----------------

\mainmatter
\pagenumbering{arabic}

\include{introduction}
\include{relatedwork}
\include{preliminaries}
\include{problems}
\include{heuristic}
\include{experiments}
\include{conclusion}


%% --------------------
%% |   Bibliography   |
%% --------------------

\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{\bibname}

\iflanguage{english}
{\bibliographystyle{alpha}}
{\bibliographystyle{babalpha-fl}} % german style

\bibliography{references}


%% ----------------
%% |   Appendix   |
%% ----------------

%\cleardoublepage
\input{appendix}


\end{document}
