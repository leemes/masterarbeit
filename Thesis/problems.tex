%% ==============
\chapter{Optimization Problems in Wind Farms}
\label{ch:problems}
%% ==============


In this chapter, we describe the problems we attempt to solve in the rest of this work. First, we formally describe a cabling problem for a wind farm. We consider the special case in which the farm has only a single substation. The cabling problem assumes substations with already fixed locations. Since the locations have a huge impact on the cabling costs, we also formulate the problem of finding their optimal positions.

\section{The Cable Layout Problem}
\label{sec:cablelayoutproblem}

As discussed in the introductory chapter, to minimize the construction costs of a wind farm, we compute an optimal cable network connecting the turbines. For now, we assume fixed locations of turbines and substations, and only consider the cabling up to the substations, ignoring how the power is delivered from the substations to the electrical grid. That means, the cables we consider connect either a pair of turbines to each other or a turbine to a substation.

For each such possible connection, we can choose between different types of cables, differing in their cost and the maximum power rating they are designed for. Specifying a maximum power rating for cables is necessary because of the energy loss inside the cable due to a non-zero resistance: the electrical energy is not really lost but rather converted into thermal energy. This heat has to dissipate properly in order to avoid over-heating and in turn damage to the cable's insulation. The maximum power rating is thus a result of the thermal limit of the cable's design (and its environment, to be precise), which we call the \emph{cable capacity}\footnote{Not to be confused with the capacity in electrical physics.}. Also, for each substation, a maximum total power rating of all connected turbines is specified, which we call the \emph{substation capacity}. The \emph{Wind Farm Cable Layout Problem} aims to choose the cable types such that the overall cabling costs are minimized, and the capacity constraints for the cables and substations are satisfied.

Before describing the whole problem in a more formal way, we need a notation for the input data, that is the power ratings of turbines and capacities, as well as the costs of the cables.

\paragraph{Power Ratings}

In a wind farm, power ratings and capacities come into play on several occasions. Turbines generate a particular amount of power. Likewise, cables, substations and other electronic components have a rating limit they are designed for, where components with higher limits usually cost more.

The power a turbine generates depends on multiple factors. It is self-explanatory that the wind strength plays the most significant role, but it affects all turbines in the same wind farm more or less equally. In practice, the ratings of all turbines in the same wind farm are very similar to each other, since they are usually of the same type and their exact location affects the wind strength only slightly. However, turbines have a specified maximum power rating which they never exceed, even if they could produce more. This quantity is called the turbine's \emph{nominal power}, and is generally used to dimension the cabling and other electrical components for power transportation.

Therefore, instead of expressing power ratings in units of watts, we simplify the notation a lot by expressing them in multiples of the nominal rating of one turbine. Furthermore, we only consider the situation in which every turbine produces the nominal power, that means its production is set to $1$. The power limits of cables and substations are rounded down to whole numbers.


\paragraph{Cabling Costs}

The cost to lay a cable from one node to another is basically composed of three parts: the connection points, the cable itself, and the cost of installing the cable between the nodes. The costs for the connection points in both nodes are independent of the length of the cable, but depend on the power rating routed through. The cost of the cable itself is the product of the cable length and the cost per length of the cable type used. Which cable type suites the connection mainly depends on the power rating. Finally, the cost to install the cable is significant especially when the cable is to be run underground, in which case it mainly depends on the terrain between the nodes.

When solving the cabling problem in this work, we are not interested in how the cabling cost is composed in detail. Not even the length of the cable is relevant on its own. Instead, for each edge and available cable type its total cost and the maximum power rating are interesting. These values can be precomputed for every edge separately. \autoref{fig:cabletypesexample} shows this data for an edge for which four cable types are available. For example, the first cable supports the power of two turbines. This edge does not support the power of more than 9 turbines.

For any edge, we call a cable \emph{superseded}, if there is any other cable available which has both a lower or the same cost as well as a greater or the same capacity. Superseded cables are not considered at all in our algorithms, since there is always a better (or equally good) option. This has the consequence that when ordering the cables by their cost, they are also automatically ordered by their capacities, and vice versa. This order implies the \emph{cable number}.

Note that our cable layout problem allows only one cable to be placed on each edge. However, if the user wants to allow a parallel wiring of multiple cables on the same edge, we can simulate this scenario by adding a virtual cable to the list of available cable types for that edge, by precomputing the cost and capacity of the wiring. Even more complicated setups can be modeled in an analogous way.

\begin{figure}[tb!]
	\centering
	\begin{tabular}{c|c|c}
		\toprule
		Cable number & Capacity & Cost \\
		\midrule
		1 & 2 & 1.3 \\
		2 & 4 & 1.5 \\
		3 & 7 & 2.0 \\
		4 & 9 & 2.2 \\
		\bottomrule
	\end{tabular}
	
	\caption{An exemplary edge for which four cable types are available.}
	\label{fig:cabletypesexample}
\end{figure}


\paragraph{The Input Data}

Formally, an instance of the Wind Farm Cable Layout Problem has the following input data. The wind farm has $t$ turbines and $s$ substations. We are given the set of possible connections, and for each of them a (separate) set of cable types, each with a capacity and cost. Furthermore, for each substation its capacity is specified, limiting the number of turbines which can be connected to it.

A cable network consists of the chosen cable type (maybe none) for every possible connection. It is called \emph{valid} if it respects the constraints given by the capacities of the chosen cables and the substations. An instance is called \emph{feasible} if a valid cable network for it exists. In this case, its solution is a valid cable network with minimal total costs of the chosen cables.

Using the following mathematical model, this problem can be defined using a flow network, which is then formulated as a mixed integer linear program.


\paragraph{The Wind Farm as a Graph}

We model the wind farm as a simple directed graph $G = (V,E)$ in which turbines and substations are nodes. We denote them as separate sets: $V_T$ is the set of turbine nodes, and $V_S$ are the substation nodes, such that together they form the set of all nodes $V = V_T \cup V_S$. Each pair of nodes, which can be connected by a cable, is modeled as an edge in this graph. The direction of the edge is arbitrarily chosen, yet it needs to be consistent to have a notation of direction when talking about a positive or negative flow on an edge. The set of edges is denoted by the symbol $E$.

For each edge, we are given a set of cable types, each with a capacity and a cost. To notate these numbers, let $k \in \naturals$ be the maximum number of cable types available on any edge. For any edge, for which less than $k$ cable types are available in the original input, add dummy cable types with zero capacity (and arbitrary cost) such that every edge has $k$ cable types. The cable types are ordered by ascending capacity. Then, for edge $(v,w)$ and $i \in \indices k$, the capacity of the $i$-th cable is denoted as $\varccap vwi \in \realsgeq$ and the cost as $\varccost vwi \in \realsgeq$. To simplify the notation in the special case of an edge with no power routed through, we define these variables also with a cable type index of $0$: $\varccap vw0 := 0$ and $\varccost vw0 := 0$.

Finally, the capacity of substation $v \in V_S$ is denoted as $\varscap v \in \mathbb{N}$.


\paragraph{The Flow Network}

Before modeling the optimization problem as a minimum-cost flow problem as explained in \autoref{sec:flownetworks}, we need to define a modified version of the above graph. Our cabling problem defines substation capacities which we have to model somehow in the flow problem. Recall that an instance of the minimum-cost flow problem contains the balance function $\balance : V \rightarrow \reals$, which is negative for sinks, where the absolute value models the amount of consumed flow at that node.

However, it can not be used for a flexible amount of consumption, which is what we need to model for our substations: their received amount of power is not fixed, yet limited by their individual capacities. Since the sum of substation capacities might be greater than the total amount of power produced in the wind farm, we have to deal with this flexibility in the assignment of power to the substation nodes. But since our definition of a flow network uses fixed values for the consumed amount at a sink, we have to model the substation capacities using a workaround. For this, we introduce a \emph{dummy sink} $\sigma$. It serves as the global sink in the flow network, i.e. it is the only sink with a balance of $-t$. We connect it with dummy edges to the actual substation nodes. The edge does not charge any cost but has the substation capacity as its capacity.

Formally, we define $G' = (V', E')$ as our modified version of $G=(V,E)$: The set of nodes is $V' := V \cup \{\sigma\}$. For the edges, we first define the set of dummy edges connecting the substations to our global sink as $E_\sigma := V_S \times \{\sigma\}$. Together with the original edges, they form the edge set $E' := E \cup E_\sigma$.

Now we are ready to model the optimization problem as the minimum-cost flow problem $(G', u, \balance, c_{(\cdot)})$. Note that since we notate power ratings as multiples of the nominal power of a single turbine, our flow values are whole numbers, and the edge cost functions are of the form $\wholes \rightarrow \realsgeq$.

As explained above, the dummy edges are used to model the substation capacities: for substation $i \in V_S$, the edge $e = (i, \sigma) \in E_\sigma$ has capacity $u(e) := \varscap i$ and does not cost anything by setting $c_e(x) := 0$ for all $x \in \wholes$.

For all other edges $e = (v,w) \in E$, we also have to define the capacity $u(e)$ as well as the cost function $c_e : \wholes \rightarrow \realsgeq$, which are used to model the cable type parameters. The edge capacity is set to the maximum capacity among the available cable types, that is $u(e) := \max_{i \in \indices k} \varccap vwi$. For a given flow value $f(e)$, we set $c_e(f(e)) := \varccost vwi$ where $i$ is the minimum value in $\{0\} \cup \indices k$ such that $|f(e)| \leq \varccap vwi$ (i.e. the cost of the cheapest suitable cable). Since the flow absolutely never exceeds its upper bound $u(e)$, for $x \in \wholes$ with $|x| > u(e)$, the value of $c_e(x)$ does not have to be defined. Consequently, a suitable $i$ is always found.

The resulting edge cost functions are all of the same basic shape: a step-wise constant function with one segment for each available cable type. \autoref{fig:cabletypesexample} shows the cost function for the cables from \autoref{fig:costfunctionexample}. As expected, it does not charge any cost for a power rating of zero, since in this case no cable is laid at all. The value of the edge cost function for any value greater than the maximum capacity among the available cable types is not relevant, as the edge's capacity will constraint the flow value.

\begin{figure}[tb!]
	\centering
	\begin{tikzpicture}[
			scale=0.6,
			axis/.style={thick, ->, >=stealth, line join=miter},
			plot/.style={very thick}
		]
		
		% axis
		\draw[axis,<->] 
		(11.5,0) node[above left] {Power rating} 
		-| (0,4)    node[left]  {Cost};
		
		% ticks on x axis
		\foreach \x in {0,...,10} {
			\draw[thin] (\x,0)--(\x,-.15);
			\node at (\x.5,-.7) {$\x$};
		}
		
		% plot
		\draw[plot] (0,0)
		-| (1,1.3)
		-| (3,1.5)
		-| (5,2)
		-| (8,2.2)
		-- (10,2.2);
	\end{tikzpicture}
	
	\caption{The edge cost function corresponding to the cable types from \autoref{fig:cabletypesexample}.}
	\label{fig:costfunctionexample}
\end{figure}

Solving the minimum-cost flow problem $(G', u, \balance, c_{(\cdot)})$ results in the cost-optimal flow $f^\star$, in case it is feasible. For any edge $e\in E$ (the original edges), the flow value $f^\star(e)$ denotes the power routed through a cable on that edge. If the flow problem is infeasible, our original cabling problem instance is also infeasible.

From a feasible flow $f^\star$, a cable layout can be computed trivially. A cable type for each edge is determined by choosing the cheapest cable with capacity at least the absolute edge's flow value. If there is no flow on an edge, no cable is chosen.


\paragraph{Mixed Integer Linear Programming}

This network flow problem can be easily expressed as a mixed integer linear program (MILP) with integer variables and real coefficients. Note that the following model computes the choice of cable types directly, and the flow values only appear as helper variables in the program, as they are not important for a solution to the original problem. Also, we do not need the dummy edges introduced for the flow network. Instead, we use the original graph $G=(V,E)$.

To model the choice of cable types on each edge $(v,w) \in E$, we introduce the set of binary variables $\varcable vwi$ for each cable type. If $\varcable vwi = 1$, the $i$-th cable type was chosen for edge $(v,w)$. For every edge, at most one such variable can be $1$.

Furthermore, we introduce the integer helper variable $\varflow vw$ for each edge $(v,w)\in E$, which corresponds to the flow value $f(v,w)$ in the flow network. It is not required for the result, but to model the constraints in the flow network: the capacity constraint for each edge and the flow conservation at each node. To simplify notation, we alias $\varflow wv := -\varflow vw$ for any edge $(v,w) \in E$. Note that this does not add more variables to the linear program.

Usually, the edge cost in a flow network depends on the flow through that edge. However, since in our case the cabling cost only depends on the choice of cable, which is modeled by the separate set of variables $\varcable vwi$, it can be expressed without the flow helper variables $\varflow vw$ in our linear program. Before listing all equations and constraints of the program, we first show how the cost and capacity of an edge $(v,w)$ can be expressed as sums of products using the variables above.
\begin{align*}
	\textrm{Cost of edge $(v,w)$: } &\quad \sum_{i\in \indices k} \varccost vwi \cdot \varcable vwi \\
	\textrm{Capacity of edge $(v,w)$: } &\quad \sum_{i\in \indices k} \varccap vwi \cdot \varcable vwi \\
\intertext{Similarly, the excess of power at node $v$ can be expressed as a sum of flow values of the incident edges.}
	\textrm{Excess at node $v$: } &\quad \sum_{w\in N(v)} \varflow vw\\
\intertext{The objective function, the total cabling cost, is then a sum of the cost over all edges.}
	\textrm{Total cabling cost: } &\quad \sum_{(v,w)\in E} \left( \sum_{i\in \indices k} \varccost vwi \cdot \varcable vwi \right)
\end{align*}
This term is to be minimized under the following constraints:
\begin{align}
	\label{eq:linprog1}
		\varcable vwi
		& \in \{0,1\}
		&& \forall (v,w)\in E, {i\in \indices k}
	\\[0.5em]
	\label{eq:linprog2}
		\varflow vw
		& \in \wholesgeq                 
		&& \forall (v,w)\in E
	\\[0.5em]
	\label{eq:linprog3}
		\sum_{i\in \indices k} \varcable vwi  
		& \leq 1                    
		&& \forall (v,w)\in E
	\\
	\label{eq:linprog4}
		\varflow vw
		& \leq \sum_{i\in \indices k} \varccap vwi\cdot \varcable vwi
		&& \forall (v,w)\in E
	\\
	\label{eq:linprog5}
		\sum_{w \in N(v)} \varflow vw
		& \leq \varscap v                   
		&& \forall v\in V_S 
	\\
	\label{eq:linprog6}
		\sum_{w \in N(v)} \varflow vw  
		& = -1                      
		&& \forall v\in V_T
\end{align}

The first two conditions (\ref{eq:linprog1}), (\ref{eq:linprog2}) define the range for the variables in the linear program, and condition \ref{eq:linprog3}) ensures that only one cable type is chosen for each edge. Condition (\ref{eq:linprog4}) constraints the power throughput of each edge to the capacity of the chosen cable type. This models the edge capacity constraint (\ref{eq:flow1}) from our definition of the flow problem in \autoref{sec:flownetworks}.

Furthermore, condition (\ref{eq:linprog5}) constraints the total power coming in at each substation to its capacity. The balance is set to $-1$ for each turbine by condition (\ref{eq:linprog6}). Together, these constraints model the flow conservation constraint (\ref{eq:flow2}) from our definition of the flow problem.

This mixed integer linear program has $\bigO(|E|\cdot k)$ variables and $\bigO(|E|)$ constraints. For complete graphs as the input, i.e. when $|E| = |V|^2$, its size is quadratic in the number of turbines.


\paragraph{The Substation Cable Layout Problem}

We also consider the special case with only a single substation, that is $s=1$, which we call \emph{Substation Cable Layout Problem}. Here, it isn't necessary to specify the substation capacity, since there are just two possible cases: it can handle the power of all turbines, then the capacity is always respected; or it can not handle it, but then a feasible solution does not exist.

As we will see later in our experiments in \autoref{ch:experiments}, this special case is much simpler to solve.



\section{The Substation Assignment Problem}
\label{sec:substationassignmentproblem}

The Wind Farm Cable Layout Problem can be split into two layers. First, for every turbine $v \in V_T$, we find a substation $w \in V_S$, to which its power is routed. We say, $v$ is \emph{assigned to} $w$. We call this sub-problem the \emph{Substation Assignment Problem}. Then, for every substation $w \in V_S$, the Substation Cable Layout Problem is solved in which only the substation $w$ and the turbines assigned to $w$ are considered.

As its input, the Substation Assignment Problem gets the exact same information as the Wind Farm Cable Layout Problem. The difference is that, instead of finding the optimal cables for each possible interconnection, its output is a function $q : V_T \rightarrow V_S$ which assigns turbine $v\in V_T$ to substation $q(v) \in V_S$ leading to minimal cabling cost when computing the cable layouts for each substation separately. The Substation Assignment Problem is an optimization problem, so we define an objective function which an optimal solution $q^\star$ minimizes.

For this, we have to describe how the input for each individual Substation Cable Layout Problem is constructed, based on an assignment function $q$. For a given substation $w \in V_S$, we set $V_w := \{ v\in V_T : q(v) = w \}$, i.e. the turbines of which the power is to be routed to substation $w$. However, the paths for routing the power of these turbines may not necessarily only use these nodes. Instead, the power of turbine $v\in V_w$ might use turbines from $V_T \setminus V_w$ as intermediate nodes, i.e. nodes of which the power is routed to a substation other than $w$.

We observed that this is the case for most representations. Why this happens and how this can be handled is explained in \autoref{ch:heuristic}, when we describe an algorithm implementing the two-level approach.


\section{The Substation Positioning Problem}
\label{sec:substationpositioningproblem}

So far, we discussed the problem of finding cost-optimal cable layouts in wind farms where turbines and substations have predefined locations. However, when only the locations of the turbines are fixed, but the substations can be positioned freely or with some degree of freedom, the cabling costs can be reduced further.

For this, we introduce the \emph{Substation Positioning Problem}: Given $t$ turbines and $s$ substations, find the optimal locations of the substations such that the total cabling costs are minimized. Now, we also need to consider the costs for the export cables, as they depend on the choice of the substation locations -- previously they where not relevant as the locations had been fixed. That means, among all possible combinations of substation locations, find the one resulting in the lowest cost as found by the Wind Farm Cable Layout Problem plus the cost for the export cables.

For simplicity reasons, we no longer define different capacities for the substations. We are rather given a single substation capacity $m \in \naturals$, which is the capacity of all substations in the wind farm.

Before we model the problem formally, we have to discuss which locations are considered for the substations. In this work we use discrete locations, that is we are given a set $L$ of possible substation locations, among which the optimal ones are to be chosen. Also, how are the costs for export cables, and the cost for the cables connecting turbines to the substations (which have no longer a fixed length) given in the input data?

For this, we define our node set as $V := V_T \cup V_L$ in which $V_T$ are again the turbine nodes, and $V_L$ are the nodes representing all possible substation locations. The set of edges are then $E := (V_T \times V_T) \cup (V_T \times V_L)$. Together with the nodes, we have the graph $G := (V,E)$. This enables us to model the cabling cost as precomuted tabular data similar as before: for every cable from a turbine to a (possible location of a) substation, we have a set of cable types each with cost and capacity, like in our Cable Layout Problem. We also need to model the cost for an export cable from $v\in V_T$ to the grid. For this, we assume that for any possible substation location, we already know the nearest grid point, or to be more precise, the one for which an export cable is as cheap as possible. This cost is then given as $c_v \in \realsgeq$.


\paragraph{Mixed Integer Linear Programming}

There is no straight forward transformation of this problem into a flow network problem: we would need to restrict the number of location nodes $V_L$ used to $s$, the number of substations.

On the other hand, the problem can modeled as an MILP similar to our model of the Cable Layout Problem. For this, we add the set of binary variables $y_v$ for $v \in V_L$, which are $1$ if a substation is placed at the location associated with node $v$. Their sum must not exceed $s$. Also, the flow on an edge from a turbine $v\in V_T$ to a substation location node $w \in V_L$ has to be zero if there is no substation, that is $y_w = 0$. This is expressed by the disjunction:
\[
\sum_{v \in V_T} \varflow vw = 0 \;\lor\; y_w = 1
\]
To add this condition to our MILP, we need to reformulate it as a linear expression. For this, observe that although the excess of a location node is not fixed, like for regular nodes which have a balance, it never exceeds $m$ due to the capacity constraint of a substation:
\[
\sum_{v \in V_T} \varflow vw \leq m
\]
Also observe that $(y_w = 1) \Leftrightarrow (1-y_w=0)$, which lets us construct the linear expression as
\[
\sum_{v \in V_T} \varflow vw \leq m \cdot (1-y_w)
\]
In the case the substation is used, the right-hand side evaluates to $m$, such that the expression constrains the incoming flow of the substation to $m$. In the other case, the right-hand side evaluates to $0$ such that no incoming flow is allowed.

We construct the full linear program by reusing the constraints (\ref{eq:linprog1})-(\ref{eq:linprog4}) and (\ref{eq:linprog6}) from above, where $V_S$ is substituted with $V_L$. Note that we do not need (\ref{eq:linprog5}), since the above expression already models the substation constraints. We add the following additional constraints:
\begin{align}
		y_{v}
		& \in \{0,1\}
		&& \forall v\in V_L
	\\[0.5em]
		\sum_{v\in V_L} y_{v} 
		& \leq s
	\\
		\sum_{v \in V_T} \varflow vw 
		&\leq m \cdot (1-y_w)
		&& \forall w\in V_L
\end{align}

So far, we did not consider the cost for the export cables. We simply add them to the objective function: when placing a substation at the location node $v\in V_L$, that is $y_v = 1$, the cost $c_v$ is charged. The objective function to be minimized then becomes

\[
	\sum_{(v,w)\in E} \left(\; \sum_{i\in \indices k} \varccost vwi \cdot \varcable vwi \;\right) \quad + \quad \sum_{v\in V_L} c_v \cdot y_v\,.
\]