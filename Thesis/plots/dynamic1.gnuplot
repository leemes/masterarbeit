#!/usr/bin/gnuplot

set terminal cairolatex
set output "dynamic1.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun



set key top left Left reverse invert
#unset key

set grid xtics
set grid ytics

set yrange [-.6 to .6]
set ytics .2
set mytics 2

set xlabel "{Number of turbines $t$ (grouped in steps of 50)}"
set ylabel "{Cost of our solution vs. reference (\\%)}"

set arrow 1 from 50,0 to 400,0 nohead 

plot \
    "dynamic.csv" using ($1+1):($2*100):($3*50) with yerrorlines lc 2 title "dynamic temperature curve", \
    "dynamic.csv" using ($1-1):($4*100):($5*50) with yerrorlines lc 1 title "standard temperature curve"

