#!/usr/bin/gnuplot

set terminal cairolatex
set output "singlesubstation-2.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top right
set yrange [-0.5 to 3.5]
set xrange [1 to 125]
set xtics 30
set mxtics 2
set ytics 1

set grid xtics
set grid ytics

set xlabel "{Time (seconds)}"
set ylabel "{Best solution seen so far vs. reference (\\%)}"

set arrow 1 from 0,0 to 120,0 nohead 

plot \
    "singlesubstation.csv" using ($1-.15):($10*100):($11*50) with yerrorlines title "s=1", \
    "singlesubstation.csv" using ($1-.05):($12*100):($13*50) with yerrorlines title "s=2", \
    "singlesubstation.csv" using ($1+.05):($14*100):($15*50) with yerrorlines title "s=3", \
    "singlesubstation.csv" using ($1+.15):($16*100):($17*50) with yerrorlines title "s=4"

