#!/usr/bin/gnuplot

set terminal cairolatex
set output "t3i130-lp.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top right
set yrange [-45 to 25]
set xrange [0 to 60]
set xtics 10
set mxtics 5
set ytics 20
set mytics 2

set grid xtics
set grid ytics

set xlabel "{Time (minutes)}"
set ylabel "{Objective value relative to result (\\%)}"

set key invert

set arrow 1 from 0,0 to 60,0 nohead 

REF=2455.82

plot \
    "t3i130-log-lp.csv" using ($1/60):($3/REF-100) with steps title "\\small upper bound", \
    "t3i130-log-lp.csv" using ($1/60):($2/REF-100) with steps title "\\small lower bound"

