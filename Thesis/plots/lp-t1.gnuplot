#!/usr/bin/gnuplot

set terminal cairolatex
set output "lp-t1.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key bottom right
#unset key

set xrange [9.5 to 20.5]
set xtics 1

set yrange [0 to 64]
set ytics 10

set y2range [0 to 32]
set y2tics 5

set grid xtics
set grid ytics

set xlabel "{Number of turbines $t$}"
set ylabel "{Running time of Gurobi (minutes)}"
set y2label "{Gap between upper and lower bound (\\%)}"

plot \
    "lp-t1.csv" using ($1):($2/60):($3/120) with yerrorlines axes x1y1 title "running time", \
    "lp-t1.csv" using ($1):($4*100):($5*50) with yerrorlines axes x1y2 title "gap"

