#!/usr/bin/gnuplot

set terminal cairolatex
set output "recover.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top left Left reverse
#unset key

set grid xtics
set grid ytics

set xrange [0.5 to 7.5]
set xtics 1,1,7
set xtics add ("640k" 1)
set xtics add ("320k" 2)
set xtics add ("160k" 3)
set xtics add ("80k" 4)
set xtics add ("40k" 5)
set xtics add ("20k" 6)
set xtics add ("10k" 7)

set yrange [-.15 to .55]

set xlabel "{Iteration threshold value triggering the recovery}"
set ylabel "{Change of cost when enabling recovery (\\%)}"

set arrow 1 from 0.5,0 to 7.5,0 nohead 

plot \
    "recover.csv" using ($1-.03):($3*100):($4*50) with yerrorlines title "medium-sized instances (T3)", \
    "recover.csv" using ($1+.03):($5*100):($6*50) with yerrorlines title "large instances (T4)"

