#!/usr/bin/gnuplot

set terminal cairolatex
set output "crossings1.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top left Left reverse 
#unset key

set grid xtics
set grid ytics

set xrange [0 to 8]
set xtics 1,2,7
set xtics add ("2" 1)
set xtics add ("8" 3)
set xtics add ("32" 5)
set xtics add ("128" 7)

set yrange [-.05 to .05]

set xlabel "{Temperature factor applied for crossings}"
set ylabel "{Change of cost when enabling crossings (\\%)}"

set arrow 1 from 0,0 to 8,0 nohead 

plot \
    "crossings.csv" using ($1-.03):($2*100):($3*50) with yerrorlines title "using 10~\\% time for crossings", \
    "crossings.csv" using ($1+.03):($4*100):($5*50) with yerrorlines title "using 20~\\% time for crossings"

