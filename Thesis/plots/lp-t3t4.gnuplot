#!/usr/bin/gnuplot

set terminal cairolatex
set output "lp-t3t4.tex"

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set datafile separator ","

#set key top left Left reverse
unset key

set yrange [28 to 33]

set grid xtics
set grid ytics

set xlabel "{Number of turbines $t$ (grouped in steps of 50)}"
set ylabel "{Gap between upper and lower bound (\\%)}"

plot \
    "lp-t3t4.csv" using ($1):($2*100):($3*50) with yerrorlines

