#!/usr/bin/gnuplot

set terminal cairolatex
set output "seed.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top left
set yrange [0 to 1]
set xrange [0.5 to 7.5]

set grid xtics
set grid ytics

set xlabel "{Number of substations $s$}"
set ylabel "{Ratio of distinct substation assignments}"

plot \
    "seed.csv" using ($1-0.01):2:($3/2) with yerrorlines title "after 2 minutes", \
    "seed.csv" using ($1+0.01):4:($5/2) with yerrorlines title "after 30 minutes"

