#!/usr/bin/gnuplot

set terminal cairolatex
set output "instancesize-lp.tex"

set datafile separator ","

unset key
#set yrange [0 to 1]
set xrange [80 to 200]

set grid xtics
set grid ytics

set xlabel "{Instance size $n$}"
set ylabel "{Gap between upper and lower bound (\\%)}"

plot \
    "tight.csv" using ($1+$2):($4*100) with points

