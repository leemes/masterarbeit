#!/usr/bin/gnuplot

set terminal cairolatex
set output "tight-lp.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


unset key
set xrange [0.83 to 1]
set xtics 0.05
set mxtics 5

set grid xtics
set grid ytics

set xlabel "{Tightness of substation capacities $\\gamma$}"
set ylabel "{Gap between upper and lower bound (\\%)}"

f(x) = a*x+b
fit f(x) "tight.csv" using (1/$3):($4*100) via a,b

plot \
    f(x) lc rgb "#888888" lw 10, \
    "tight.csv" using (1/$3):($4*100) with points lt 1

