#!/usr/bin/gnuplot

set terminal cairolatex
set output "tau-t4.tex"

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set datafile separator ","

set key top left Left reverse

set yrange [-1.5 to 6.5]

set grid xtics
set grid ytics

set xlabel "{Number of turbines $t$ (grouped in steps of 50)}"
set ylabel "{Cost of our solution vs. reference (\\%)}"

set arrow 1 from 200,0 to 500,0 nohead 

plot \
    "tau-t4.csv" using ($1+15):($2 *100):($3 *50) with yerrorlines title "$\\tau=1\\cdot10^\{-5\}$", \
    "tau-t4.csv" using ($1+20):($4 *100):($5 *50) with yerrorlines title "$\\tau=2\\cdot10^\{-5\}$", \
    "tau-t4.csv" using ($1+25):($6 *100):($7 *50) with yerrorlines title "$\\tau=4\\cdot10^\{-5\}$", \
    "tau-t4.csv" using ($1+30):($8 *100):($9 *50) with yerrorlines title "$\\tau=8\\cdot10^\{-5\}$", \
    "tau-t4.csv" using ($1+35):($10*100):($11*50) with yerrorlines title "$\\tau=16\\cdot10^\{-5\}$"

