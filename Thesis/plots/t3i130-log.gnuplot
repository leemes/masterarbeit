#!/usr/bin/gnuplot

set terminal cairolatex
set output "t3i130-log.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top right
set yrange [-2 to 5]
set xrange [0 to 30]
set xtics 5
set mxtics 5
set ytics 1

set grid xtics
set grid ytics

set xlabel "{Time (minutes)}"
set ylabel "{Best solution seen so far vs. reference (\\%)}"

set key invert

set arrow 1 from 0,0 to 30,0 nohead 

REF=2455.82

plot \
    "t3i130-log-lp.csv" using ($1/60):($3/REF-100) with steps title "\\small Gurobi's upper bound solution" lc 2, \
    "t3i130-log-dynamic.csv" using ($1/60):($3/REF-100) with steps title "\\small our heuristic with dynamic temp. curve, $\\tau = 4 \\cdot 10^\{-5\}$" lc 3, \
    "t3i130-log-fixed.csv" using ($1/60):($3/REF-100) with steps title "\\small our heuristic with standard temp. curve, $\\tau = 4 \\cdot 10^\{-7\}$" lc 1

