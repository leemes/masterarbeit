#!/usr/bin/gnuplot

set terminal cairolatex
set output "instancesize-our.tex"

set datafile separator ","

unset key
#set yrange [0 to 1]
#set xrange [0.5 to 7.5]

set grid xtics
set grid ytics

set xlabel "{Instance size $n$}"
set ylabel "{Cost of our solution vs. reference (\\%)}"

plot \
    "tight.csv" using ($1+$2):($5*100) with points

