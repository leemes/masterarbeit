INSERT INTO testset VALUES
    -- Single substation, small
    (null,
        .7,     --bmin
        1,      --bmax
        10,     --tmin
        80,     --tmax
        1,      --smin
        1,      --smax
        0,      --stmin
        1,      --stmax
        1,      --cmin
        1,      --cmax
        0,      --cvar
        6,      --knearest
        1.1,    --maxdetour

        500     --numinstances
    )
;
