#!/bin/bash

# $1: run id
# $2: instance first
# $3: instance last

echo Solving instances in run \#$1 [instances $2 to $3]

run="$1"
row=$(echo "SELECT * FROM solve_run WHERE id = $1;" | sqlite3 db/db.sqlite)
testset=$(echo "$row" | awk '{split($0,a,"|"); print a[2]}')
args=$(echo "$row" | awk '{split($0,a,"|"); print "config.json --maxtime="a[3]"  --tinit="a[5]"  --tstep="a[6] }')
numruns=$(echo "$row" | awk '{split($0,a,"|"); print a[4]}')

echo "  - Testset: $testset"
echo "  - Arguments: $args"
echo "  - Number of runs per instance: $numruns"

function exec() {
    # $1: arguments for 'experiments' command
    # $2: command outfile
    
    name=experiments
    path="../code/build-experiments-qt5-Release/"

    echo
    echo -e '\e[33;1m'$name '\e[31;1m'$1 '\e[0m'
    echo "  > $2"
    echo    
    
    ./$path$name $1 | tee $2 | tee /dev/stderr |
        awk '{print "INSERT INTO solve_result VALUES (null, '$run', '$instance', "$1", "$2", "$3", "$4", "$5", "$6", "$7", \""$8"\");"}' |
        sqlite3 db/db.sqlite
}


for ((instance=$2; instance < $3; ++instance)) {
    filename=$(echo "SELECT filename FROM instance WHERE testset_id=$testset AND num=$instance;" | sqlite3 db/db.sqlite)

    for ((seed=1; seed < $numruns; ++seed)) {
        exec "solve $filename $args --seed=$seed" out/run-$1-instance-$instance-seed-$seed.log
    }
}

