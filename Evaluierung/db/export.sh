#!/bin/bash

tables=$(echo "SELECT name FROM sqlite_master WHERE type='table';" | sqlite3 db.sqlite)

for table in $tables; do
    sqlite3 -header -csv db.sqlite "select * from $table;" > $table.csv
done
