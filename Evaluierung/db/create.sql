CREATE TABLE testset (
    id INTEGER PRIMARY KEY AUTOINCREMENT,

    -- Generator params:
    bmin FLOAT, bmax FLOAT,
    tmin INTEGER, tmax INTEGER,
    smin INTEGER, smax INTEGER,
    stmin FLOAT, stmax FLOAT,
    cmin FLOAT, cmax FLOAT, cvar FLOAT,
    knearest INTEGER, maxdetour FLOAT,

    -- Testset size:
    numinstances INTEGER
);


CREATE TABLE instance (
    id INTEGER PRIMARY KEY AUTOINCREMENT,

    testset_id INTEGER,
    num INTEGER,

    -- Actual params:
    b FLOAT,
    t INTEGER,
    s INTEGER,
    c FLOAT,

    -- Where the .gml file is located
    filename TEXT
);


CREATE TABLE solve_run (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    testset_id INTEGER,

    -- solve params:
    maxtime INTEGER, -- in seconds
    numruns INTEGER, -- number of runs with different seeds
    tinit FLOAT, -- initial temperature
    tstep FLOAT -- exponential temperature step size (in each iteration: temperature *= 1 - tstep)
);

CREATE TABLE lp_run (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    testset_id INTEGER,
    
    -- lp params:
    maxtime INTEGER -- in seconds
);


CREATE TABLE solve_result (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    run_id INTEGER,
    instance_id INTEGER,
    
    -- log entries
    time FLOAT,
    thread INTEGER,
    iter INTEGER,
    cost FLOAT,
    temp FLOAT,
    act FLOAT,
    kill BOOLEAN,
    assignment TEXT
);

CREATE TABLE lp_result (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    run_id INTEGER,
    instance_id INTEGER,
    
    -- log entries
    time FLOAT,
    lower FLOAT,
    upper FLOAT,
    gap FLOAT
);
