#!/bin/bash

# $1: testset id

testset=$1

echo Generating instances for testset \#$1

args=$(
    echo "SELECT * FROM testset WHERE id = $1;" |
    sqlite3 db/db.sqlite |
    awk '{split($0,a,"|"); print "generate  graphs/testset-'$1'-instance-%1.gml  --bmin="a[2]"  --bmax="a[3]"  --tmin="a[4]"  --tmax="a[5]"  --smin="a[6]"  --smax="a[7]"  --stmin="a[8]"  --stmax="a[9]"  --cmin="a[10]"  --cmax="a[11]"  --cvar="a[12]"  --knearest="a[13]"  --maxdetour="a[14]"  --n="a[15] }'
)


function exec() {
    # $1: arguments for 'experiments' command
    # $2: command outfile
    
    name=experiments
    path="../code/build-experiments-qt5-Release/"

    echo
    echo -e '\e[33;1m'$name '\e[31;1m'$1 '\e[0m'
    echo "  > $2"
    echo    
    
    ./$path$name $1 | tee $2 | tee /dev/stderr |
        awk '{print "INSERT INTO instance VALUES (null, '$testset', "$1", "$2", "$3", "$4", "$5", \""$6"\");"}' |
        sqlite3 db/db.sqlite
}

exec "$args" out/generate-$1.log
