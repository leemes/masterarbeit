Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    weight 1
    graphics [
      x 559.0000000
      y 297.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    weight 1
    graphics [
      x 479.0000000
      y 471.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    weight 1
    graphics [
      x 869.0000000
      y 423.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    weight 1
    graphics [
      x 712.0000000
      y 600.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 4
    label "4"
    weight 1
    graphics [
      x 588.0000000
      y 720.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 5
    label "5"
    weight 1
    graphics [
      x 655.0000000
      y 446.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 6
    label "6"
    weight 1
    graphics [
      x 862.0000000
      y 643.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 7
    label "7"
    weight 1
    graphics [
      x 1007.000000
      y 304.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 8
    label "8"
    weight 1
    graphics [
      x 1117.000000
      y 579.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 9
    label "9"
    weight 1
    graphics [
      x 276.0000000
      y 661.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 10
    label "10"
    weight 1
    graphics [
      x 839.0000000
      y 812.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 11
    label "11"
    weight 1
    graphics [
      x 1012.000000
      y 539.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 12
    label "12"
    weight 1
    graphics [
      x 1044.000000
      y 768.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 13
    label "13"
    weight 1
    graphics [
      x 532.0000000
      y 603.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 14
    label "14"
    weight 1
    graphics [
      x 403.0000000
      y 839.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 15
    label "15"
    weight 1
    graphics [
      x 272.0000000
      y 436.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 16
    label "16"
    weight 1
    graphics [
      x 371.0000000
      y 241.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 17
    label "17"
    weight 1
    graphics [
      x 858.0000000
      y 264.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 18
    label "18"
    weight 1
    graphics [
      x 673.0000000
      y 877.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 19
    label "19"
    weight -19
    graphics [
      x 705.0000000
      y 126.0000000
      w 44.00000000
      h 44.00000000
      fill "#00CCFF"
      line "#703C4B"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "oval"
    ]
  ]
  edge [
    source 15
    target 16
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 16
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 7
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 7
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 8
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 8
    target 12
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 12
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 7
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 8
    target 7
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 12
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 12
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 8
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 10
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 10
    target 18
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 18
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 10
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 10
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 18
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 18
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 10
    target 12
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 18
    target 14
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 14
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 14
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 14
    target 9
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 9
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 16
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 15
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 9
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 9
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 15
    target 9
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 15
    target 14
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 15
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 16
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 16
    target 19
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 19
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 19
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 19
    target 7
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 19
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 16
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 16
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 7
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 7
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 8
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 12
    target 8
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 12
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 7
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 7
    target 8
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 11
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 12
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 12
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 8
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 10
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 18
    target 10
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 18
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 10
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 10
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 18
    target 6
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 18
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 12
    target 10
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 14
    target 18
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 14
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 4
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 14
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 9
    target 14
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 9
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 6
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 17
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 16
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 9
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 13
    target 9
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 15
    target 13
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 9
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 14
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 15
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 5
    target 16
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 11
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 4
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 19
    target 16
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 19
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 19
    target 17
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 7
    target 19
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 19
    target 5
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
]
