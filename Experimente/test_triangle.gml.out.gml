Creator "ogdf::GraphIO::writeGML"
graph [
  directed 1
  node [
    id 0
    label "0"
    weight 1
    graphics [
      x 937.0000000
      y 304.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#29330E"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 1
    label "1"
    weight 1
    graphics [
      x 503.0000000
      y 304.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#29330E"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 2
    label "2"
    weight 1
    graphics [
      x 722.0000000
      y 586.0000000
      w 30.00000000
      h 30.00000000
      fill "#FFCC00"
      line "#29330E"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "rectangle"
    ]
  ]
  node [
    id 3
    label "3"
    weight -3
    graphics [
      x 722.0000000
      y 401.0000000
      w 44.00000000
      h 44.00000000
      fill "#00CCFF"
      line "#29330E"
      pattern "1"
      stipple 1
      lineWidth 1.000000000
      type "oval"
    ]
  ]
  edge [
    source 2
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 3
    target 2
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 0
    target 3
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 2
    target 1
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
  edge [
    source 1
    target 0
    label "[]"
    graphics [
      type "line"
      arrow "last"
      stipple 1
      lineWidth 0.000000000
      weight 1.000000000
      fill "#000000"
    ]
  ]
]
