#!/bin/bash
# shuber, 2010-01-05

if [[ $# < "1" ]]; then
	echo "$0 <file> [params to pdftoipe]"
	echo ""
	echo "Convert eps, pdf and svg files to Ipe xml-files."
	exit -1
fi


infile=$1
shift
infileend=${infile##*.}

if [[ ! -f "$infile" ]]; then
	echo "No file '$infile'"
	exit -1
fi


md5infile=`cat $infile | md5sum  | cut -d ' ' -f 1`
outfile="${infile%%.*}.xml"

case $infileend in

	eps|svg )
		tmppdffile="/tmp/${md5infile}.pdf"

		case $infileend in

			eps )
			epstopdf "$infile" --outfile $tmppdffile || exit -2
			;;

			svg )
			inkscape "$infile" --export-pdf=$tmppdffile || exit -2
			;;

			* )
			echo "Script author is lame. Tell him!"
			;;
		esac

		pdftoipe $tmppdffile "$outfile" $@ || exit -2
		rm $tmppdffile
		;;

	pdf )
		pdftoipe "$infile" "$outfile" $@ || exit -2
		;;

	* )
		echo "Unknown filetype '$infileend'"
		exit -1
		;;
esac



