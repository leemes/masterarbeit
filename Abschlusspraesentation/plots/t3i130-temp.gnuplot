#!/usr/bin/gnuplot

set terminal cairolatex standalone pdf font 'phv,bx,it' header '\usepackage{sfmath}' size 5.5,2
set output "t3i130-temp.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top right
set yrange [0 to 0.01]
set y2range [0 to 0.1]
set xrange [0 to 30]
set xtics 5
set mxtics 5
set ytics 0.002
set y2tics 0.02

set grid xtics
set grid ytics

set xlabel "{Time (minutes)}"
set ylabel "{Temperature}"
set y2label "{Activity}"

plot \
    "t3i130-log-fixed.csv" using ($1/60):($4) with lines title "\\small standard temperature curve" lc 1, \
    "t3i130-log-dynamic.csv" using ($1/60):($4) with lines title "\\small dynamic temperature curve" lc 3, \
    "t3i130-log-dynamic.csv" using ($1/60):($6) with lines axes x1y2 title "\\small activity (of dynamic temperature)" lc 4

