#!/usr/bin/gnuplot

set terminal cairolatex standalone pdf font 'phv,bx,it' header '\usepackage{sfmath}' size 5,2.5
set output "multithreads.tex"

set datafile separator ","

# == KIT colors ==
set linetype  1 lc rgb "#2c76bb" lw 1   # KIT blau
set linetype  2 lc rgb "#bf252c" lw 1   # KIT rot
set linetype  3 lc rgb "#00a68d" lw 1   # KIT gruen
set linetype  4 lc rgb "#ba198b" lw 1   # KIT lila
set linetype  5 lc rgb "#b08433" lw 1   # KIT braun


set key top left Left reverse
#unset key

set yrange [-.6 to .6]

set grid xtics
set grid ytics

set xlabel "{Number of turbines $t$ (grouped in steps of 50)}"

set arrow 1 from 50,0 to 500,0 nohead 

plot \
    "multithreads.csv" using ($1-2):($2 *100):($3*50) with yerrorlines title "1 run" lc 2, \
    "multithreads.csv" using ($1)  :($4 *100):($5*50) with yerrorlines title "2 runs" lc 1, \
    "multithreads.csv" using ($1+2):($6 *100):($7*50) with yerrorlines title "4 runs", \


