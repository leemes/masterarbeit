#include "lpsolverwrapper.h"
#include "gurobi_c++.h"
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/basic/Array2D.h>
#include "util.h"
#include "model.h"
#include "fn.h"

#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;
using namespace ogdf;


template<class RangeOfVars>
GRBLinExpr sum(RangeOfVars && vars) {
    using std::begin; using std::end;
    return std::accumulate(begin(vars), end(vars), GRBLinExpr());
}





LPSolverWrapper::LPSolverWrapper(ogdf::Graph &g, const GraphAttributes &ga, const ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge, int maxtime, bool saveSolution) :
    g(g), ga(ga), cableTypesPerEdge(cableTypesPerEdge), maxtime(maxtime), saveSolution(saveSolution),
    m(0),
    gaOut(util::makeOut(g, ga)), turbineIndex(g)
{
    setAutoDelete(false);
    initArrays();
}

void LPSolverWrapper::run()
{
    int N = g.numberOfNodes();
    int K = 0;
    for (int i = 0; i < g.numberOfEdges(); ++i)
        K = std::max(K, int(cableTypesPerEdge[i].size()));

    node v;
    edge e;


    try
    {
        GRBEnv env;
        if (m) {
            // If m is set, we call optimize repeatedly and render the solution into m
            env.set(GRB_DoubleParam_TimeLimit, 1.0);
        }
        env.set(GRB_IntParam_Threads, 1);
        env.set(GRB_IntParam_LogToConsole, 0);

        GRBModel lpModel = GRBModel(env);
        gurobiModel = &lpModel;

        // Variables
        vars_f_ij = vector<vector<GRBVar>>(N, vector<GRBVar>(N));
        vars_x_ijt = vector<vector<vector<GRBVar>>>(N, vector<vector<GRBVar>>(N, vector<GRBVar>(K)));
        forall_edges(e, g) {
            int i = e->source()->index();
            int j = e->target()->index();

            if (i < j) {
                for(int t = 0; t < K; ++t) {
                    auto name = "x_{i=" + to_string(i) + ", j=" + to_string(j) + ", t=" + to_string(t) + "}";
                    vars_x_ijt[i][j][t] = lpModel.addVar(0.0, 1.0, 0.0, GRB_BINARY, name);
                }

                auto name = "f_{i=" + to_string(i) + ", j=" + to_string(j) + "}";
                vars_f_ij[i][j] = lpModel.addVar(-GRB_INFINITY, GRB_INFINITY, 0.0, GRB_INTEGER, name);
            }
        }
        lpModel.update();

        // Helper expressions
        vector<vector<GRBLinExpr>> expr_cable_count_ij(N, vector<GRBLinExpr>(N));
        vector<vector<GRBLinExpr>> expr_cable_cost_ij(N, vector<GRBLinExpr>(N));
        vector<vector<GRBLinExpr>> expr_cable_capacity_ij(N, vector<GRBLinExpr>(N));
        expr_excess_i = vector<GRBLinExpr>(N);
        forall_edges(e, g) {
            int i = e->source()->index();
            int j = e->target()->index();
            const auto &cableTypes = cableTypesPerEdge[e];

            if (i < j) {
                for(int t = 0; t < K; ++t) {
                    expr_cable_count_ij[i][j]    += vars_x_ijt[i][j][t];
                    expr_cable_cost_ij[i][j]     += vars_x_ijt[i][j][t] * (cableTypes[t].cost);
                    expr_cable_capacity_ij[i][j] += vars_x_ijt[i][j][t] * (cableTypes[t].capacity);
                }
                expr_excess_i[i] -= vars_f_ij[i][j];
                expr_excess_i[j] += vars_f_ij[i][j];
            }
        }


        // Objective
        GRBLinExpr obj;
        forall_edges(e, g) {
            int i = e->source()->index();
            int j = e->target()->index();

            if (i < j) {
                obj += expr_cable_cost_ij[i][j];
            }
        }
        lpModel.setObjective(obj, GRB_MINIMIZE);


        // (4.3): Only one cable per edge
        // (4.4): Cable capacity
        forall_edges(e, g) {
            int i = e->source()->index();
            int j = e->target()->index();

            if (i < j) {
                lpModel.addConstr(expr_cable_count_ij[i][j] <= 1,
                                  "only one cable on edge " + to_string(i) + " -> " + to_string(j));

                lpModel.addConstr(vars_f_ij[i][j] <= expr_cable_capacity_ij[i][j],
                                  "cable capacity on edge " + to_string(i) + " -> " + to_string(j) + " [upper bound]");
                lpModel.addConstr(-vars_f_ij[i][j] <= expr_cable_capacity_ij[i][j],
                                  "cable capacity on edge " + to_string(i) + " -> " + to_string(j) + " [lower bound]");
            }
        }

        // (4.5): Substation capacity
        // (4.6): Production of turbines = 1
        forall_nodes(v, g) {
            int i = v->index();

            if (isTurbine(v)) {
                // (4.6): Production of turbines = 1
                lpModel.addConstr(expr_excess_i[i] == -1,
                                  "production of turbine " + to_string(i));
            } else {
                // (4.5): Substation capacity
                lpModel.addConstr(expr_excess_i[i] <= ga.weight(v),
                                  "substation capacity " + to_string(i));
            }
        }

        // Optimize model
        if (m) {
            double baseTime = 0.0;
            forever {
                lpModel.setCallback(new Callback(this, baseTime));
                lpModel.optimize();
                baseTime += lpModel.get(GRB_DoubleAttr_Runtime);
                renderIntoModel(m);
            }
        } else {
            lpModel.setCallback(new Callback(this, 0.0));
            lpModel.optimize();
        }

        if (saveSolution) {
            updateOut();
            util::saveOut(gaOut, "out.gml");
        }
    }
    catch(GRBException e)
    {
        cout << "Error code = " << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;
    }
    catch(...)
    {
        cout << "Exception during optimization" << endl;
    }

    done();
}

void LPSolverWrapper::setModel(Model *model)
{
    m = model;
}



void LPSolverWrapper::initArrays() {
    turbineIndex.fill(-1);
    node v;
    int i = 0;
    forall_nodes(v, g) {
        if (isTurbine(v))
            ++i;
    }
    turbines.resize(i);
    i = 0;
    forall_nodes(v, g) {
        if (isTurbine(v)) {
            turbineIndex[v] = i;
            turbines[i] = v;
            ++i;
        }
    }

    nodes.resize(g.numberOfNodes());
    forall_nodes(v, g) {
        nodes[v->index()] = v;
    }
}

bool LPSolverWrapper::isTurbine(node v)
{
    return util::shapeToType(ga.shape(v)) == util::Turbine;
}

bool LPSolverWrapper::updateLower(double time, double lower)
{
    gurobiLower = lower;
    if (time >= gurobiLastTime + 0.1) {
        gurobiLastTime = time;
        printInfo();
    }
    return maxtime && time > maxtime;
}

bool LPSolverWrapper::updateUpper(double time, double upper)
{
    gurobiUpper = upper;
    if (time >= gurobiLastTime + 0.1) {
        gurobiLastTime = time;
        printInfo();
    }
    return maxtime && time > maxtime;
}

void LPSolverWrapper::printInfo()
{
    double gap = (1.0 - gurobiLower / gurobiUpper);

    cout << setw(16) << gurobiLastTime
         << setw(16) << gurobiLower
         << setw(16) << gurobiUpper
         << setw(16) << gap
         << endl;
}

double LPSolverWrapper::updateOut()
{
    double cost = 0;
    edge e;
    forall_edges(e, g) {
        int i = e->source()->index();
        int j = e->target()->index();
        const auto &cableTypes = cableTypesPerEdge[e];

        double flow = vars_f_ij[min(i,j)][max(j,i)].get(GRB_DoubleAttr_X);
        if (!(i < j))
            flow = -flow;

        if (flow > 0) {
            int c = CableType::findCable(cableTypes, flow);
            gaOut.label(e) = std::to_string(c);
            gaOut.intWeight(e) = cableTypes[c].capacity;
            cost += cableTypes[c].cost;
            gaOut.strokeWidth(e) = flow;
        } else {
            gaOut.label(e) = "";
            gaOut.intWeight(e) = 0.0;
        }
    }

    node v;
    forall_nodes(v, g) {
        if (!isTurbine(v))
            gaOut.label(v) = std::to_string(expr_excess_i[v->index()].getValue());
    }
    return cost;
}

void LPSolverWrapper::renderIntoModel(Model *m)
{
    double cost = updateOut();
    m->setGraph(g, gaOut);
    m->setTemperature(0);
    m->setEnergy(cost);
}



