#ifndef CABLETYPE_H
#define CABLETYPE_H

#include <vector>

class QJsonValue;
class QJsonArray;
template<class> class QVector;

struct CableType {
    double cost;
    double capacity;

    static CableType fromJson(QJsonValue json);

    static std::vector<CableType> vectorFromJson(QJsonArray json);

    static int findCable(const std::vector<CableType> &cableTypes, double flow);
};

#endif // CABLETYPE_H
