#ifndef UTIL_H
#define UTIL_H

#include <ogdf/basic/graphics.h>
#include <ogdf/basic/Graph.h>
#include <QCommandLineParser>
#include <QFile>
#include <QVariant>
#include "cabletype.h"

class QByteArray;
template<typename T> class QVector;

namespace ogdf {
class GraphAttributes;
}


namespace util {

enum NodeType {
    Turbine = 1,
    Substation = 2
};

NodeType shapeToType(ogdf::Shape shape);
ogdf::Shape typeToShape(NodeType type);



namespace json {

QVariant parse(QByteArray json);
QVariant parse(QString json);
QVariant parse(std::string json);

}




template<typename F>
void forallNodesInBothGraphs(const ogdf::Graph &g1, const ogdf::Graph &g2, F f) {
    using namespace ogdf;
    ogdf::node v1 = g1.firstNode();
    ogdf::node v2 = g2.firstNode();
    for(; v1 && v2; v1 = v1->succ(), v2 = v2->succ())
        f(v1, v2);
}

template<typename F>
void forallEdgesInBothGraphs(const ogdf::Graph &g1, const ogdf::Graph &g2, F f) {
    using namespace ogdf;
    ogdf::edge e1 = g1.firstEdge();
    ogdf::edge e2 = g2.firstEdge();
    for(; e1 && e2; e1 = e1->succ(), e2 = e2->succ())
        f(e1, e2);
}


void deepCopyGraphAttributes(const ogdf::Graph &g, const ogdf::Graph &gCopy, const ogdf::GraphAttributes &ga, ogdf::GraphAttributes &gaCopy);

template<typename T>
void deepCopyEdgeArray(const ogdf::Graph &g, const ogdf::Graph &gCopy, const ogdf::EdgeArray<T> &arr, ogdf::EdgeArray<T> &arrCopy) {
    arrCopy.init(gCopy);
    forallEdgesInBothGraphs(g, gCopy, [&arr,&arrCopy](ogdf::edge e1, ogdf::edge e2){
        arrCopy[e2] = arr[e1];
    });
}


ogdf::GraphAttributes makeOut(ogdf::Graph &g, const ogdf::GraphAttributes &ga);
void initOut(ogdf::Graph &g, const ogdf::GraphAttributes &ga, ogdf::GraphAttributes &gaOut);
void saveOut(const ogdf::GraphAttributes &gaOut, QString filename);


ogdf::Graph makeCompleteGraph(int N, bool doubleDirected);


QString assignmentToString(QVector<int> assignment);
QString assignmentHash(QVector<int> assignment);





QByteArray readFile(QString fileName);

double distance(double x1, double y1, double x2, double y2);

void initInput(ogdf::Graph &g, ogdf::GraphAttributes &ga, ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge);




// -------------------------------------------------------------------------------------------


class CommandLineParser
{
    QCommandLineParser parser;

protected:
    CommandLineParser() {
        parser.addHelpOption();
    }

    void showHelp() {
        parser.showHelp();
    }

    template<typename T>
    void addPositionalArgument(bool process, T& target, int &i,
                               QString name, QString description, bool fileNameCheckExists = false)
    {
        if (process) {
            if (parser.positionalArguments().count() <= i) {
                cerr << "Argument " << name.toStdString() << " is mandatory." << endl;
                parser.showHelp();
            }
            QVariant value(parser.positionalArguments()[i]);
            if (!value.canConvert<T>()) {
                cerr << "Invalid value for argument " << name.toStdString() << "." << endl;
                parser.showHelp();
            }
            if (fileNameCheckExists) {
                if (!QFile::exists(value.toString())) {
                    cerr << "File given by argument " << name.toStdString() << " does not exist:" << endl << '\t' << value.toString().toStdString() << endl;
                    parser.showHelp();
                }
            }
            target = value.value<T>();
        } else {
            parser.addPositionalArgument(name, description);
        }
        ++i;
    }

    template<typename T>
    void addOption(bool process, T& target,
                   QString name, QString description, QString valueName)
    {
        if (process) {
            if (!parser.isSet(name)) {
                cerr << "Argument " << name.toStdString() << " is mandatory." << endl;
                parser.showHelp();
            }
            QVariant value(parser.value(name));
            if (!value.canConvert<T>()) {
                cerr << "Invalid value for argument " << name.toStdString() << "." << endl;
                parser.showHelp();
            }
            target = value.value<T>();
        } else {
            parser.addOption({name, description, valueName});
        }
    }

    template<typename T>
    void addOption(bool process, T& target,
                   QString name, QString description, QString valueName, T defaultValue)
    {
        if (process) {
            QVariant value(parser.value(name));
            if (!value.canConvert<T>()) {
                cerr << "Invalid value for argument " << name.toStdString() << "." << endl;
                parser.showHelp();
            }
            target = value.value<T>();
        } else {
            parser.addOption({name, description, valueName, QVariant(defaultValue).toString()});
        }
    }

    void process(const QStringList &args) {
        parser.process(args);
    }
};




}

#endif // UTIL_H

