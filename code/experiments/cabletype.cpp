#include "cabletype.h"
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QVector>

CableType CableType::fromJson(QJsonValue json) {
    if (json.isObject()) {
        auto obj = json.toObject();
        return { obj["cost"].toDouble(), obj["capacity"].toDouble() };
    } else if (json.isArray()) {
        auto obj = json.toArray();
        return { obj[0].toDouble(), obj[1].toDouble() };
    } else {
        qFatal("Wrong JSON input for cable types");
    }
}

std::vector<CableType> CableType::vectorFromJson(QJsonArray array)
{
    std::vector<CableType> cableTypes;
    for (auto &&json : array)
        cableTypes.push_back(CableType::fromJson(json));
    return cableTypes;
}

int CableType::findCable(const std::vector<CableType> &cableTypes, double flow)
{
    for (int i = 0; i < int(cableTypes.size()); ++i)
        if (cableTypes[i].capacity >= flow)
            return i;
    return -1;
}
