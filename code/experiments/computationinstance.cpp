#include "computationinstance.h"
#include "model.h"
#include "util.h"
#include "computation.h"
#include <QDebug>
#include <QElapsedTimer>
#include <QTimer>
#include <QVarLengthArray>
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/basic/Array2D.h>
#include <ogdf/graphalg/MinimumCut.h>
#include <fn.h>

using namespace ogdf;

static int nextId = 0;



template<typename R, typename T>
bool contains(R && range, T && value) {
    return std::find(range.begin(), range.end(), value) != range.end();
}





ComputationInstance::ComputationInstance(Model *&model, const ogdf::Graph &g, const GraphAttributes &ga,
                                         const ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge,
                                         int seed, Computation *master, ComputationParameters params, const Rep *initialRep) :
    g(g), ga(this->g, ga.attributes()), cableTypesPerEdge(cableTypesPerEdge), rand(seed),
    master(master), params(params),
    m(model), gaOut(), turbineIndex(this->g), twinEdge(this->g) //, attraction(this->g)
{
    m_id = nextId++;
    setAutoDelete(false);

    util::deepCopyGraphAttributes(g, this->g, ga, this->ga);
    util::deepCopyEdgeArray(g, this->g, cableTypesPerEdge, this->cableTypesPerEdge);
    gaOut = util::makeOut(this->g, this->ga);
    initArrays();

    N = this->g.numberOfNodes();
    T = turbines.size();

    temp = params.initialTemperature;

    if (initialRep) {
        current = *initialRep;
        currentEnergy = saEvaluate(current);
    } else {
        current = saInit(this->rand);
        currentEnergy = saEvaluate(current);
    }

    currentBest = current;
    currentBestEnergy = currentEnergy;
}

int ComputationInstance::id() const
{
    return m_id;
}

void ComputationInstance::run()
{
    running = true;

    // Loop for one "round"
    int iterationsThisRound = 0;
    bool done = false;
    while (!(done = (temp <= params.stopTemperature || iterations >= params.stopIterations || activityEMA < params.stopActivity))
           && iterationsThisRound < params.iterationsPerRound
           && !wasKilled())
    {
        // Decrease temperature
        double deltaTemp = params.deltaTemperature;
        if (params.dynamicTemperature)
            deltaTemp *= activity();
        temp -= temp * deltaTemp;

        // Recover
        if (params.recover) {
            if (iterationsSinceLastImprovement > params.recover) {
                current = currentBest;
                currentEnergy = currentBestEnergy;
                iterationsSinceLastImprovement = 0;
            }
        }

        // Find neighbor solution
        Rep next = saNeighbor(rand, current);
        double newEnergy = saEvaluate(next);
        double delta = (newEnergy - currentEnergy) / (currentEnergy);
        bool worse = delta > std::numeric_limits<double>::epsilon();
        bool accept = !worse;

        if (worse) {
            double probability = min(1.0, exp(-delta / temp));
            accept = std::bernoulli_distribution(probability)(rand);

            // If the solution got accepted although worse, notify the master.
            // Maybe he wants to clone this instance (depending on the acceptance probability),
            // this tries to keep "good" solutions alive.
            if (accept) {
                if (master)
                    master->aboutToGetWorse(this, probability);
            }

            // Probability tracking
            activityEMA
                    = params.activityEMAAlpha * probability
                    + (1.0-params.activityEMAAlpha) * activityEMA;
        }

        if (accept) {
            current = next;
            currentEnergy = newEnergy;
            currentAssignment = sinkIndicesForNodes(current);
        }

        // "currentEnergy < currentBestEnergy"
        if (currentEnergy - currentBestEnergy <= -std::numeric_limits<double>::epsilon()) {
            currentBest = current;
            currentBestEnergy = currentEnergy;
            currentBestAssignment = currentAssignment;
            iterationsSinceLastImprovement = 0;
        }

        iterations++;
        iterationsThisRound++;
        iterationsSinceLastImprovement++;
        iterationsSinceLastRender++;

        if (m)
            maybeRender();
    }

    if (m)
        m->setIterations(m->iterations() + iterationsSinceLastRender);

    iterationsSinceLastRender = 0;
    running = false;

    if (master)
        master->instancePaused(this, done);
}

bool ComputationInstance::isRunning() const
{
    return running;
}

void ComputationInstance::kill()
{
    killed = true;
}

bool ComputationInstance::wasKilled() const
{
    return killed;
}

ComputationInstance *ComputationInstance::clone(int reseed) const
{
    // The clone should restart at the representation currently best known
    ComputationInstance *clone = new ComputationInstance(m, g, ga, cableTypesPerEdge, reseed, master, params, &currentBest);
    clone->temp = temp;
    clone->activityEMA = activityEMA;

    return clone;
}

int ComputationInstance::numberOfIterations() const
{
    return iterations;
}

double ComputationInstance::energy() const
{
    return currentBestEnergy;
}

double ComputationInstance::temperature() const
{
    return temp;
}

double ComputationInstance::activity() const
{
    return activityEMA;
}

QVector<int> ComputationInstance::assignment() const
{
    return currentBestAssignment;
}

bool ComputationInstance::mature() const
{
    return iterations > params.maturity;
}

bool ComputationInstance::penaltiesApplied() const
{
    return currentBest.penaltiesApplied;
}

void ComputationInstance::renderIntoModel()
{
    if (isRunning())
        toBeRendered = true;
    else
        renderBest();
}

void ComputationInstance::saveSolution(QString filename)
{
    util::saveOut(gaOut, filename);
}

void ComputationInstance::manualAction()
{
    /*
    int v = 346;
    int xl = nodeTable[v];
    int xh = nodeTable[v+1];
    qDebug() << xl << xh;
    for (int x = xl; x < xh; ++x)
        qDebug() << x << "->" << current.nodeEdgeQueue[x]->target()->index();
        */
    // (b is now to be pushed to map[a])
    /*
    std::vector<int> chain = fn::filter(fn::seq(0, N), [&](int i){ return map[i] < map[a] && map[i] > map[b]; });
    std::sort(chain.begin(), chain.end(), [&](int i, int j){ return map[i] > map[j]; });
    for (int i : fn::seq(1, N))
        swap(map[i], map[i-1]);
        */
}

double ComputationInstance::pairwiseDifference(const ComputationInstance *a, const ComputationInstance *b)
{
    if (a->numberOfIterations() == 0 || b->numberOfIterations() == 0 ||
            a->energy() > 1e100 || b->energy() > 1e100) {
        return std::numeric_limits<double>::max();
    }

    EdgeArray<double> aFlow(a->g, 0.0);
    NodeArray<double> aSink(a->g, 0.0);
    a->computeFlow(a->currentBest, aFlow, aSink);

    EdgeArray<double> bFlow(b->g, 0.0);
    NodeArray<double> bSink(b->g, 0.0);
    b->computeFlow(b->currentBest, bFlow, bSink);

    return fn::sum(fn::map(fn::seq(0, a->g.numberOfEdges()), [&](int i){
        double flowDiff = aFlow[i] - bFlow[i];
        return flowDiff * flowDiff;
    }));
}

double ComputationInstance::pairwiseCompatibility(const ComputationInstance *a, const ComputationInstance *b)
{
    if (a->numberOfIterations() == 0 || b->numberOfIterations() == 0 ||
            a->energy() > 1e100 || b->energy() > 1e100) {
        return std::numeric_limits<double>::max();
    }

    return pairwiseSinkDiffs(a, b, nullptr);
}

std::pair<ComputationInstance *, ComputationInstance *> ComputationInstance::crossOver(const ComputationInstance *a, const ComputationInstance *b, int seed, double tempFactor)
{
    const Rep & aRep = a->currentBest;
    const Rep & bRep = b->currentBest;

    // Partition the substations
    std::vector<int> substationPartition; // will contain substation indices of the ones in one of the two partitions
    pairwiseSinkDiffs(a, b, &substationPartition);

    // Compute sinks
    NodeArray<node> aSinkOfNode(a->g, nullptr);
    a->computeSinkForEachNode(aRep, aSinkOfNode);
    NodeArray<node> bSinkOfNode(b->g, nullptr);
    b->computeSinkForEachNode(bRep, bSinkOfNode);

    // ----------------------

    // WEIGHT

    // Initialize weight field using sink partition
    std::vector<double> weight(a->N, 0.0);
    for (int i = 0; i < a->N; ++i) {
        node aSink = aSinkOfNode[a->nodes[i]];
        node bSink = bSinkOfNode[b->nodes[i]];
        int aSinkIndex = a->substationIndex(aSink);
        int bSinkIndex = b->substationIndex(bSink);
        // case 1: they are the same sink, and that sink is in the partition
        if (aSinkIndex == bSinkIndex && contains(substationPartition, aSinkIndex)) {
            weight[i] = 1.0;
        }
        // case 2: they are the same sink, and that sink is not in the partition
        else if (aSinkIndex == bSinkIndex && !contains(substationPartition, aSinkIndex)) {
            weight[i] = 0.0;
        }
        // case 3: they are not in the same sink
        else {
            weight[i] = 0.5;
        }
    }

    // ----------------------

    // NEW REPS

    Rep rep1 {
        Array<int>(a->N),
        Array<bool>(a->g.numberOfEdges()),
        0,
        std::numeric_limits<double>::max(),
        std::vector<int>(a->substations.size(), 0),
        true,
        std::vector<edge>(a->nodeEdges.size())
    };
    Rep rep2 {
        Array<int>(a->N),
        Array<bool>(a->g.numberOfEdges()),
        0,
        std::numeric_limits<double>::max(),
        std::vector<int>(a->substations.size(), 0),
        true,
        std::vector<edge>(a->nodeEdges.size())
    };

    // ----------------------

    // MAP

    // Compute new map, real-valued (with node index in a pair)
    std::vector<std::pair<int, float> > mapReal(a->N);
    for (int i = 0; i < a->N; ++i) {
        auto w = weight[i];
        auto value = w * aRep.map[i] + (1.0 - w) * bRep.map[i];
        mapReal[i] = std::make_pair(i, value);
    }
    // Sort by value
    std::sort(mapReal.begin(), mapReal.end(), [](std::pair<int,float> a, std::pair<int,float> b){
        return a.second < b.second;
    });
    // Looking at node indices now gives a rank, which results in the actual map
    for (int i = 0; i < a->N; ++i) {
        int j = mapReal[i].first;
        rep1.map[j] = i;
        rep2.map[j] = (a->N - 1) - i;
    }

    // ----------------------

    ComputationInstance *clone1 = new ComputationInstance(a->m, a->g, a->ga, a->cableTypesPerEdge, seed, a->master, a->params, &rep1);
    ComputationInstance *clone2 = new ComputationInstance(a->m, a->g, a->ga, a->cableTypesPerEdge, seed, a->master, a->params, &rep2);

    // ----------------------

    // EDGE CUTS

    edge e;
    forall_edges(e, a->g) {
        int v = e->source()->index();
        int w = e->target()->index();

        edge ea = a->g.searchEdge(a->nodes[v], a->nodes[w]);
        edge eb = b->g.searchEdge(b->nodes[v], b->nodes[w]);
        edge e1 = clone1->g.searchEdge(clone1->nodes[v], clone1->nodes[w]);
        edge e1t = clone1->twinEdge[e1];
        edge e2 = clone2->g.searchEdge(clone2->nodes[v], clone2->nodes[w]);
        edge e2t = clone2->twinEdge[e2];

        bool cut1 = false, cut2 = false;
        if (weight[v] == 0.0 && weight[w] == 0.0) {
            cut1 = aRep.cuts[ea->index()];
            cut2 = bRep.cuts[eb->index()];
        } else if (weight[v] == 1.0 && weight[w] == 1.0) {
            cut1 = bRep.cuts[eb->index()];
            cut2 = aRep.cuts[ea->index()];
        }

        clone1->current.cuts[e1->index()] = clone1->current.cuts[e1t->index()] = cut1;
        clone2->current.cuts[e2->index()] = clone2->current.cuts[e2t->index()] = cut2;
    }
    clone1->currentBest = clone1->current;
    clone2->currentBest = clone2->current;

    // ----------------------

    // Mean values for temp and activity
    clone1->temp = (a->temp + b->temp) / 2 * tempFactor;
    clone2->temp = (a->temp + b->temp) / 2 * tempFactor;
    clone1->activityEMA = (a->activityEMA + b->activityEMA) / 2;
    clone2->activityEMA = (a->activityEMA + b->activityEMA) / 2;

    return std::make_pair(clone1, clone2);
}

void ComputationInstance::maybeRender()
{
    if (toBeRendered) {
        toBeRendered = false;
        renderBest();
        //renderCurrent();
    }
}

void ComputationInstance::renderBest()
{
    render(currentBest, currentBestEnergy, temp);
}

void ComputationInstance::renderCurrent()
{
    render(current, currentEnergy, temp);
}

void ComputationInstance::render(const Rep &rep, double energy, double temperature)
{
    if (m) {
        m->setThread(m_id);

        // We update the model, but showing the solution is expensive, so we only compute the solution drawing if the model was not changed since the last screen update
        bool showSolution = m->isClean();
        m->setTemperature(temperature);
        m->setEnergy(energy);

        // Copy intermediate result in the model to display it
        if (showSolution) {
            EdgeArray<double> flowOnEdge(g, 0.0);
            NodeArray<double> sinkReceived(g, 0.0);
            bool ok = computeFlow(rep, flowOnEdge, sinkReceived);
            Q_ASSERT(ok);

            edge e;
            forall_edges(e, g) {
                double flow = flowOnEdge[e];
                const auto &cableTypes = cableTypesPerEdge[e];
                if (flow > 0) {
                    Q_ASSERT(flowOnEdge[twinEdge[e]] == 0);
                    int c = CableType::findCable(cableTypes, flow);
                    if (c == -1) c = cableTypes.size() - 1;
                    gaOut.label(e) = std::to_string(c);
                    gaOut.intWeight(e) = cableTypes[c].capacity;
                } else {
                    gaOut.label(e) = rep.cuts[e->index()] ? "-1" : "";
                    gaOut.intWeight(e) = 0.0;
                }
                gaOut.strokeWidth(e) = flow;
            }

            node v;
            forall_nodes(v, g) {
                gaOut.weight(v) = rep.map[v->index()];
                gaOut.label(v) = std::to_string(sinkReceived[v]);
            }

            m->setGraph(g, gaOut);
        }

        m->setIterations(m->iterations() + iterationsSinceLastRender);
        iterationsSinceLastRender = 0;
    }
}

edge ComputationInstance::findEdgeCut(const Rep &rep, bool cut)
{
    int n = cut ? rep.numCuts : (g.numberOfEdges() / 2 - rep.numCuts);
    if (n == 0)
        return nullptr;
    int k = std::uniform_int_distribution<int>(0, n-1)(rand);
    int i = 0;
    edge e;
    forall_edges(e, g) {
        if (rep.cuts[e->index()] == cut) {
            if (i == k) return e;
            ++i;
        }
    }
    Q_UNREACHABLE();
}

void ComputationInstance::setEdgeCut(ComputationInstance::Rep &rep, edge e, bool cut) const
{
    Q_ASSERT(rep.cuts[e->index()] != cut);
    rep.cuts[e->index()] = cut;
    rep.numCuts += cut ? 1 : -1;
}

void ComputationInstance::initArrays() {
    turbineIndex.fill(-1);
    node v;
    // Count turbines
    int i = 0;
    forall_nodes(v, g) {
        if (util::shapeToType(ga.shape(v)) == util::Turbine)
            ++i;
    }

    // Turbines + substations
    substations.resize(g.numberOfNodes() - i);
    turbines.resize(i);
    i = 0;
    int j = 0;
    forall_nodes(v, g) {
        if (util::shapeToType(ga.shape(v)) == util::Turbine) {
            turbineIndex[v] = i;
            turbines[i] = v;
            ++i;
        } else {
            substations[j] = v;
            ++j;
        }
    }

    // Nodes, Edges and Node table
    nodes.resize(g.numberOfNodes());
    nodeTable.resize(g.numberOfNodes() + 1);
    forall_nodes(v, g) {
        nodes[v->index()] = v;
        nodeTable[v->index()] = nodeNeighbors.size();
        adjEntry a;
        forall_adj(a, v) {
            edge e = a->theEdge();
            if (e->source() == v) {
                nodeEdges.push_back(e);
                nodeNeighbors.push_back(e->target()->index());
            }
        }
    }
    nodeTable[g.numberOfNodes()] = nodeNeighbors.size();

    // Twin edges
    edge e;
    forall_edges(e, g) {
        // For edge e = (v,w), find the directed edge (w,v)
        node v = e->source(), w = e->target();
        edge f;
        forall_adj_edges(f, v) {
            if(f->source() == w)
                twinEdge[e] = f;
        }
    }

    // Attraction
    /*
    if (substations.size() > 1) {
        auto computeAvgNearestDistance = [&](ogdf::Array<ogdf::node> nodes){
            return fn::sum(fn::map(nodes, [&](node s1){
                return fn::min(fn::map(nodes, [&](node s2){
                    if (s1 == s2) return std::numeric_limits<double>::max();
                    double dx = ga.x(s1) - ga.x(s2);
                    double dy = ga.y(s1) - ga.y(s2);
                    return sqrt(dx * dx + dy * dy);
                }));
            })) / nodes.size();
        };
        double avgNearestTurbineDistance = computeAvgNearestDistance(turbines);
        double avgNearestSubstationDistance = computeAvgNearestDistance(substations);
        double globalAttractionFactor = params.attr * g.numberOfNodes() / avgNearestTurbineDistance;
        NodeArray<std::vector<std::pair<double,double>>> forcesAtPoint(g);
        forall_nodes(v, g) {
            double fx = 0.0, fy = 0.0;
            for (node w : substations) {
                double dx = ga.x(w) - ga.x(v);
                double dy = ga.y(w) - ga.y(v);
                double distance = sqrt(dx * dx + dy * dy);
                double strength = pow(avgNearestSubstationDistance / distance, 2);
                fx += (dx / distance) * strength;
                fy += (dy / distance) * strength;
            }
            forcesAtPoint[v].emplace_back(fx, fy);
        }
        forall_edges(e, g) {
            node v = e->source(), w = e->target();
            double dx = ga.x(w) - ga.x(v);
            double dy = ga.y(w) - ga.y(v);
            // normalize (dx,dy)
            double distance = sqrt(dx * dx + dy * dy);
            dx /= distance, dy /= distance;
            // component of force vector in direction of edge = dot product of force vector with direction vector of edge
            double fx, fy;
            for (int i = 0; i < substations.size(); ++i) {
                std::tie(fx, fy) = forcesAtPoint[v][i];
                double dot = dx * fx + dy * fy;
                attraction[e].push_back(dot * globalAttractionFactor);
            }
        }
    }
    */
}

bool ComputationInstance::isTarget(node v) const
{
    return util::shapeToType(ga.shape(v)) != util::Turbine;
}

int ComputationInstance::substationIndex(node v) const
{
    auto it = std::find(substations.begin(), substations.end(), v);
    if (it == substations.end())
        return -1;
    return it - substations.begin();
}

ogdf::edge ComputationInstance::findBestEdge(const ComputationInstance::Rep &rep, node v, const std::vector<bool> &visited) const
{
    int xl = nodeTable[v->index()], xh = nodeTable[v->index() + 1];
    auto begin = rep.nodeEdgeQueue.begin() + xl, end = rep.nodeEdgeQueue.begin() + xh;
    //qDebug() << v->index();
    //for (edge e : fn::makeRange(begin, end))
    //    qDebug() << '\t' << e->source()->index() << " -> " << e->target()->index();
    auto it = std::find_if(begin, end, [&](edge e){
        edge twin = twinEdge[e];
        return rep.cuts[e->index()] == false && rep.cuts[twin->index()] == false
            && !visited[e->target()->index()];
    });
    return (it == end) ? nullptr : *it;

    //----------------------------------
    // Implementation without tables:
    //----------------------------------
    /*
    edge bestEdge = nullptr;
    double bestValue = std::numeric_limits<double>::lowest();

    int pSrc = rep.map[v->index()];

    adjEntry a;
    forall_adj(a, v) {
        edge e = a->theEdge();
        edge twin = twinEdge[e];
        if (rep.cuts[e->index()] == false && rep.cuts[twin->index()] == false && e->source() == v) {
            if (!visited[e->target()]) {
                // Potential difference
                int pTrg = rep.map[e->target()->index()];
                int pDiff = pTrg - pSrc;
                // Value is the "potential slope" = potential difference / edge length
                double value = pDiff / ga.doubleWeight(e);
                // Add attraction
                value += attraction[e];

                if (value > bestValue) {
                    bestValue = value;
                    bestEdge = e;
                }
            }
        }
    }
    return bestEdge;
    */
}

bool ComputationInstance::findPath(const ComputationInstance::Rep &rep, int t, std::vector<edge> &outPath, std::vector<bool> &visited) const
{
    outPath.clear();

    node n = turbines[t]; // start at the turbine node
    while (!isTarget(n)) {
        visited[n->index()] = true;
        // find next node by investigating priorities of unvisited neighborhood
        edge e = findBestEdge(rep, n, visited);
        if (!e) {
            // No path found
            for (edge e : outPath)
                visited[e->source()->index()] = false;
            outPath.clear();
            return false;
        }
        outPath.push_back(e);
        n = e->target();
    }
    for (edge e : outPath)
        visited[e->source()->index()] = false;
    return true;
}

bool ComputationInstance::computeFlow(const ComputationInstance::Rep &rep, ogdf::EdgeArray<double> &flowOnEdge, ogdf::NodeArray<double> &sinkReceived) const
{
    std::vector<edge> path;
    std::vector<bool> visited(N, false);

    for (int t = 0; t < T; ++t) {
        double rating = 1.0;
        bool ok = findPath(rep, t, path, visited);
        if (!ok)
            return false;
        for (edge e : path) {
            flowOnEdge[e] += rating;
        }
        if (!path.empty()) {
            edge last = *path.rbegin();
            node sink = last->target();
            sinkReceived[sink] += rating;
        }
    }
    for (int i = 0; i < substations.size(); ++i)
        rep.substReceived[i] = sinkReceived[substations[i]];

    // Pairs of nodes with two-directional flow: find absolute flow value and its direction (on one of both edges)
    edge e;
    forall_edges(e, g) {
        edge f = twinEdge[e];
        if (e->index() < f->index()) {
            double sum = flowOnEdge[e] - flowOnEdge[f];
            if (sum > 0) {
                flowOnEdge[e] = sum;
                flowOnEdge[f] = 0;
            } else {
                flowOnEdge[e] = 0;
                flowOnEdge[f] = -sum;
            }
        }
    }

    return true;
}

bool ComputationInstance::computeSinkForEachNode(const ComputationInstance::Rep &rep, ogdf::NodeArray<node> &sinkOfNode) const
{
    std::vector<edge> path;
    std::vector<bool> visited(N);

    for (int t = 0; t < T; ++t) {
        bool ok = findPath(rep, t, path, visited);
        if (!ok)
            return false;
        if (!path.empty()) {
            edge last = *path.rbegin();
            node sink = last->target();
            sinkOfNode[turbines[t]] = sink;
        }
    }

    return true;
}

QVector<int> ComputationInstance::sinkIndicesForNodes(const ComputationInstance::Rep &rep) const
{
    ogdf::NodeArray<node> sinkOfNode(g);
    if (!computeSinkForEachNode(rep, sinkOfNode))
        return {};

    QVector<int> sinkIndicesOfNode(T);
    for (int t = 0; t < T; ++t) {
        node sink = sinkOfNode[turbines[t]];
        sinkIndicesOfNode[t] = substationIndex(sink);
    }
    return sinkIndicesOfNode;
}

double ComputationInstance::pairwiseSinkDiffs(const ComputationInstance *a, const ComputationInstance *b, std::vector<int> * substationPartition)
{
    const Rep & aRep = a->currentBest;
    const Rep & bRep = b->currentBest;

    // Compute flows
    EdgeArray<double> aFlow(a->g, 0.0);
    NodeArray<double> aSink(a->g, 0.0);
    a->computeFlow(aRep, aFlow, aSink);
    EdgeArray<double> bFlow(b->g, 0.0);
    NodeArray<double> bSink(b->g, 0.0);
    b->computeFlow(bRep, bFlow, bSink);

    // Compute sinks
    NodeArray<node> aSinkOfNode(a->g, nullptr);
    a->computeSinkForEachNode(aRep, aSinkOfNode);
    NodeArray<node> bSinkOfNode(b->g, nullptr);
    b->computeSinkForEachNode(bRep, bSinkOfNode);

    // Compute sink diff graph
    int S = a->substations.size();
    ogdf::Graph sinkDiffGraph = util::makeCompleteGraph(S, true);
    ogdf::EdgeArray<double> sinkDiffCounts(sinkDiffGraph, 0.0);
    std::vector<node> sinkDiffNodes;
    ogdf::NodeArray<int> indexOfSinkDiffNode(sinkDiffGraph);
    {
        node v;
        forall_nodes(v, sinkDiffGraph) {
            indexOfSinkDiffNode[v] = sinkDiffNodes.size();
            sinkDiffNodes.push_back(v);
        }
    }

    for (int t : fn::seq(0, a->T)) {
        // To which substation is turbine t assigned in each solution?
        auto as = aSinkOfNode[a->turbines[t]->index()];
        auto bs = bSinkOfNode[b->turbines[t]->index()];

        if (as && bs) {
            // Find substation indices (NOT node index!)
            auto asi = a->substationIndex(as);
            auto bsi = b->substationIndex(bs);

            if (asi != bsi) {
                // Find substation nodes in the sink diff graph
                auto asd = sinkDiffNodes[asi];
                auto bsd = sinkDiffNodes[bsi];

                // Add one to the sink diff graph
                auto sinkDiffEdge = sinkDiffGraph.searchEdge(asd, bsd);
                sinkDiffCounts[sinkDiffEdge]++;
            }
        }
    }

    // Compute bisection cut
    ogdf::MinCut sinkDiffCut(sinkDiffGraph, sinkDiffCounts);

    // The minimum cut
    double value = sinkDiffCut.minimumCut();

    if (substationPartition) {
        List<node> partition;
        sinkDiffCut.partition(partition);

        for (node v : partition) {
            (*substationPartition).push_back(indexOfSinkDiffNode[v]);
        }
    }

    return value;
}

ComputationInstance::Rep ComputationInstance::saInit(std::mt19937 &rand)
{
    Rep rep {
        Array<int>(N),
        Array<bool>(g.numberOfEdges()),
        0,
        std::numeric_limits<double>::max(),
        std::vector<int>(substations.size(), 0),
        true,
        std::vector<edge>(nodeEdges.size())
    };

    // Initially there should be no edge cuts
    edge e;
    forall_edges(e, g) {
        rep.cuts[e->index()] = false;
    }

    // Find distance to nearest substation for each node
    std::vector<std::pair<int,double>> distanceToSubstation;
    for (int i = 0; i < N; ++i) {
        node v = nodes[i];
        // Find nearest substation
        double dist = fn::min(fn::map(substations, [&](node w){
            double dx = ga.x(w) - ga.x(v);
            double dy = ga.y(w) - ga.y(v);
            return sqrt(dx * dx + dy * dy);
        }));
        distanceToSubstation.emplace_back(i, dist);
    }
    std::sort(distanceToSubstation.begin(), distanceToSubstation.end(), [](std::pair<int,double> a, std::pair<int,double> b){
        return a.second > b.second;
    });
    for (int i = 0; i < N; ++i) {
        rep.map[distanceToSubstation[i].first] = i;
    }


    // Randomly swap entries in priority map until it results in valid paths
    auto pickNodeIndex = std::uniform_int_distribution<int>(0, N-1);
    auto pickNodeIndexPair = [&](std::mt19937 &rand){
        int a = pickNodeIndex(rand), b;
        do
            b = pickNodeIndex(rand);
        while (a == b);
        return std::make_pair(a, b);
    };
    std::vector<bool> visited(N);
    bool ok = false;
    int ctr = 0;
    while (!ok) {
        rep.computeTables(this);
        ok = true;
        std::vector<edge> path;
        for (int t = 0; t < T && ok; ++t)
            ok &= findPath(rep, t, path, visited);
        ctr++;
        if (ctr > 1000) {
            qDebug() << "Representation is failing too often, giving up";
            rep.cost = std::numeric_limits<double>::max();
            return rep;
        }
        if (!ok) {
            int a, b;
            std::tie(a, b) = pickNodeIndexPair(rand);
            swap(rep.map[a], rep.map[b]);
        }
    }
    return rep;
}

ComputationInstance::Rep ComputationInstance::saNeighbor(std::mt19937 &rand, const ComputationInstance::Rep &curr)
{
    auto pickMethod = std::uniform_int_distribution<int>(0, 2);
    auto pickNodeIndex = std::uniform_int_distribution<int>(0, N-1);
    auto pickNodeIndexPair = [&](std::mt19937 &rand){
        int a = pickNodeIndex(rand), b;
        do
            b = pickNodeIndex(rand);
        while (a == b);
        return std::make_pair(a, b);
    };

    Rep neighbor;

    // Apply mutation operation, but make sure that a path can still be found
    bool ok = false;
    int ctr = 0;
    while (!ok) {
        // For every new try, begin with the current state
        neighbor = curr;
        auto & map = neighbor.map;

        // Pick a method to be used as the mutation operation
        int method = pickMethod(rand);

        // (1) Perform swaps
        if (method == 1) {
            for (int s = 0; s < swapsPerMutation; ++s) {
                // Choose two distinct indices randomly; swap their priorities
                int a, b;
                std::tie(a, b) = pickNodeIndexPair(rand);
                swap(map[a], map[b]);
            }
        }

        // (2) Choose two distinct indices randomly; push the lower to the priority of the higher and adjust the priorities of all the nodes in between
        else if (method == 2) {
            int a, b;
            std::tie(a, b) = pickNodeIndexPair(rand);
            if (map[a] < map[b])
                swap(a, b);
            // (b is now to be pushed to map[a])
            std::vector<int> chain = fn::filter(fn::seq(0, N), [&](int i){ return map[i] <= map[a] && map[i] >= map[b]; });
            if (std::uniform_int_distribution<int>(0, 1)(rand))
                std::sort(chain.begin(), chain.end(), [&](int i, int j){ return map[i] > map[j]; });
            else
                std::sort(chain.begin(), chain.end(), [&](int i, int j){ return map[i] < map[j]; });
            for (int i : fn::seq(1, int(chain.size())))
                swap(map[chain[i]], map[chain[i-1]]);
        }

        // (3) Add / remove an edge cut, or two
        else {
            double targetRatio = 0.1 * sqrt(g.numberOfEdges()/2 * N) / (g.numberOfEdges() / 2);
            double actualRatio = double(neighbor.numCuts) / (g.numberOfEdges() / 2);
            double probabilityAdd = pow(1.0 - actualRatio, log(0.5) / log(1.0 - targetRatio));
            //qDebug() << probabilityAdd;
            bool addCut = std::bernoulli_distribution(probabilityAdd)(rand);
            int count = std::uniform_int_distribution<int>(1, ceil(sqrt(N)))(rand);
            for (int i = 0; i < count; ++i) {
                edge e = findEdgeCut(neighbor, !addCut);
                if (e)
                    setEdgeCut(neighbor, e, addCut);
            }
        }

        // Update tables
        neighbor.computeTables(this);

        // Check if valid paths can be found for all turbines
        EdgeArray<double> flowOnEdge(g, 0.0);
        NodeArray<double> sinkReceived(g, 0.0);
        ok = computeFlow(neighbor, flowOnEdge, sinkReceived);
        neighbor.penaltiesApplied = false;
        if (ok) {
            double newCost = 0;

            // Cost per edge: the cost for laying the cable
            edge e;
            forall_edges(e, g) {
                double flow = flowOnEdge[e];
                const auto &cableTypes = cableTypesPerEdge[e];
                if (flow > 0) {
                    Q_ASSERT(flowOnEdge[twinEdge[e]] == 0.0);
                    int c = CableType::findCable(cableTypes, flow);
                    double cost = (c >= 0) ? cableTypes[c].cost : cableTypes[cableTypes.size()-1].cost;
                    double capacity = (c >= 0) ? cableTypes[c].capacity : cableTypes[cableTypes.size()-1].capacity;
                    if (flow > capacity)  {
                        double exceeded = max(0.0, flow - capacity);
                        neighbor.penaltiesApplied = true;
                        cost += pow(exceeded, params.penaltyCableExponent) * params.penaltyCableFactor / temp;
                    }
                    newCost += cost;
                }
            }

            // Cost per substation: add penalty if capacity exceeded
            for (node s : substations) {
                double actual = sinkReceived[s];
                double capacity = ga.weight(s);
                if (actual > capacity)  {
                    double exceeded = max(0.0, actual - capacity);
                    neighbor.penaltiesApplied = true;
                    newCost += pow(exceeded, params.penaltySubstationExponent) * params.penaltySubstationFactor / temp;
                }
            }
            neighbor.cost = newCost;
        }

        ctr++;
        if (ctr > 1000) {
            qDebug() << "Representation is failing too often, giving up";
            neighbor.cost = std::numeric_limits<double>::max();
            return neighbor;
        }
    }
    return neighbor;
}

double ComputationInstance::saEvaluate(const ComputationInstance::Rep &rep)
{
    return rep.cost;
}

void ComputationInstance::Rep::computeTables(const ComputationInstance*that)
{
    std::copy(that->nodeEdges.begin(), that->nodeEdges.end(), nodeEdgeQueue.begin());
    std::vector<int> tmpIndices(that->nodeEdges.size());
    int i = 0;
    std::generate(tmpIndices.begin(), tmpIndices.end(), [&]{ return i++; });

    std::vector<double> nodePriorities(that->nodeEdges.size());     // for each edge: current priority = slope + attraction, not considering edge cuts

    std::vector<double> substationAttractionFactors(that->substations.size());
    if (that->substations.size() > 1) {
        for (int i = 0; i < that->substations.size(); ++i) {
            substationAttractionFactors[i] = 1.0 / (1.0 + pow(double(substReceived[i]) / that->ga.weight(that->substations[i]), 2.0));
        }
    }

    for (int i = 0; i < that->N; ++i) {
        int pSrc = map[i];
        int xl = that->nodeTable[i], xh = that->nodeTable[i+1]; // range of adjacent node indices
        for (int x = xl; x < xh; ++x) {
            edge e = that->nodeEdges[x];
            int j = that->nodeNeighbors[x];
            // Potential difference
            int pTrg = map[j];
            double pDiff = double(pTrg - pSrc) / that->N; // normalized potential difference in interval [-1..1]
            // we only want positive potential diffs
            pDiff += 1.0;
            // Value is the "potential slope" = potential difference / edge length
            double value = pDiff / that->ga.doubleWeight(e);
            // Add attraction
            /*
            if (that->substations.size() > 1) {
                for (int i = 0; i < that->substations.size(); ++i)
                    value += that->attraction[e][i] * substationAttractionFactors[i];
            }
            */
            nodePriorities[x] = value;
        }
        // Sort edges by their priority (descending!) => edge queue
        std::sort(tmpIndices.begin() + xl, tmpIndices.begin() + xh, [&](int a, int b){
            return nodePriorities[a] > nodePriorities[b];
        });
        for (int x = xl; x < xh; ++x) {
            nodeEdgeQueue[x] = that->nodeEdges[tmpIndices[x]];
        }
    }
}
