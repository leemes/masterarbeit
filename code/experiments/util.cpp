#include "util.h"
#include <QString>
#include <QVariant>
#include <QJsonDocument>
#include <QVector>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/fileformats/GraphIO.h>
#include <QJsonArray>
#include <QCryptographicHash>
#include <fn.h>

using namespace ogdf;


namespace util {

NodeType shapeToType(Shape shape)
{
    switch (shape) {
    case shEllipse: return Substation;
    default: return Turbine;
    }
}

Shape typeToShape(NodeType type)
{
    switch (type) {
    case Substation: return shEllipse;
    default: return shRect;
    }
}


namespace json {

QVariant parse(QByteArray json)
{
    return QJsonDocument::fromJson(json).toVariant();
}

QVariant parse(QString json)
{
    return parse(json.toUtf8());
}

QVariant parse(std::string json)
{
    return parse(QByteArray::fromStdString(json));
}

}


void deepCopyGraphAttributes(const Graph &g, const Graph &gCopy, const GraphAttributes &ga, GraphAttributes &gaCopy)
{
    using namespace ogdf;
    using GA = GraphAttributes;

#define COPY_NODE_ATTRIB(attrib) \
        forallNodesInBothGraphs(g, gCopy, [&](node v, node vCopy){ gaCopy.attrib(vCopy) = ga.attrib(v); })
#define COPY_EDGE_ATTRIB(attrib) \
        forallEdgesInBothGraphs(g, gCopy, [&](edge e, edge eCopy){ gaCopy.attrib(eCopy) = ga.attrib(e); })

    // NODE ATTRIBUTES:
    if (ga.attributes() & gaCopy.attributes() & GA::nodeGraphics) {
        COPY_NODE_ATTRIB(x);
        COPY_NODE_ATTRIB(y);
        COPY_NODE_ATTRIB(width);
        COPY_NODE_ATTRIB(height);
        COPY_NODE_ATTRIB(shape);
    }
    if (ga.attributes() & gaCopy.attributes() & GA::nodeLabel) {
        COPY_NODE_ATTRIB(label);
    }
    if (ga.attributes() & gaCopy.attributes() & GA::nodeWeight) {
        COPY_NODE_ATTRIB(weight);
    }

    // EDGE ATTRIBUTES:
    if (ga.attributes() & gaCopy.attributes() & GA::edgeStyle) {
        COPY_EDGE_ATTRIB(strokeWidth);
    }
    if (ga.attributes() & gaCopy.attributes() & GA::edgeLabel) {
        COPY_EDGE_ATTRIB(label);
    }
    if (ga.attributes() & gaCopy.attributes() & GA::edgeIntWeight) {
        COPY_EDGE_ATTRIB(intWeight);
    }
    if (ga.attributes() & gaCopy.attributes() & GA::edgeDoubleWeight) {
        COPY_EDGE_ATTRIB(doubleWeight);
    }

#undef COPY_NODE_ATTRIB
#undef COPY_EDGE_ATTRIB
}


GraphAttributes makeOut(Graph &g, const GraphAttributes &ga)
{
    using GA = GraphAttributes;

    GA gaOut(g, GA::nodeGraphics | GA::nodeStyle | GA::nodeLabel | GA::nodeWeight
          | GA::edgeStyle | GA::edgeLabel | GA::edgeIntWeight | GA::edgeDoubleWeight);
    initOut(g, ga, gaOut);
    return gaOut;
}

void initOut(Graph &g, const GraphAttributes &ga, GraphAttributes &gaOut) {
    gaOut.setDirected(true);

    node v;
    forall_nodes(v, g) {
        gaOut.x(v) = ga.x(v);
        gaOut.y(v) = ga.y(v);
        gaOut.shape(v) = ga.shape(v);
        gaOut.label(v) = "";
        gaOut.weight(v) = ga.weight(v);
    }

    edge e;
    forall_edges(e, g) {
        gaOut.doubleWeight(e) = ga.doubleWeight(e);
        gaOut.label(e) = "";
        gaOut.strokeWidth(e) = 0.0;
    }
}

void saveOut(const GraphAttributes &gaOut, QString filename)
{
    using GA = GraphAttributes;

    const Graph & g = gaOut.constGraph();
    Graph gSave = g; // Copy the graph

    GA gaSave(gSave, GA::nodeGraphics | GA::nodeStyle | GA::nodeLabel | GA::nodeWeight
          | GA::edgeGraphics | GA::edgeStyle | GA::edgeLabel | GA::edgeIntWeight | GA::edgeDoubleWeight);
    //gaSave.setDirected(false);
    deepCopyGraphAttributes(g, gSave, gaOut, gaSave);

    node v;
    forall_nodes(v, gSave) {
        gaSave.label(v) = std::to_string(gaSave.weight(v));
    }

    edge e;
    std::vector<edge> delEdges;
    forall_edges(e, gSave) {
        int flow = gaSave.strokeWidth(e);
        if (flow == 0) {
            delEdges.push_back(e);
        }
    }
    for (edge e : delEdges) {
        gSave.delEdge(e);
    }

    forall_edges(e, gSave) {
        int type = std::stoi(gaSave.label(e));
        int cap = gaSave.intWeight(e);
        int flow = gaSave.strokeWidth(e);
        int color = 255 - (type + 1) * 85;
        gaSave.strokeWidth(e) = type * 3 + 4;
        gaSave.strokeColor(e) = Color(color, color, color);
        gaSave.label(e) = std::to_string(flow);
    }

    // Output
    GraphIO::writeGML(gaSave, filename.toStdString());
}


Graph makeCompleteGraph(int N, bool doubleDirected)
{
    Graph g;
    std::vector<node> V;
    V.reserve(N);
    for (int i = 0; i < N; ++i) {
        V.push_back(g.newNode());
        for (int j = 0; j < i; ++j) {
            g.newEdge(V[i], V[j]);
            if (doubleDirected)
                g.newEdge(V[j], V[i]);
        }
    }
    return g;
}

QString assignmentToString(QVector<int> assignment)
{
    if (assignment.isEmpty())
        return "[no assignment]";

    QString str = fn::map(assignment, [](int s){return '0'+s;});

    return assignmentHash(assignment) + ":" + str;
}
QString assignmentHash(QVector<int> assignment)
{
    if (assignment.isEmpty())
        return "[no assignment]";

    QByteArray bytes = fn::map(assignment, [](int s){return char(s);});

    return QCryptographicHash::hash(bytes, QCryptographicHash::Md5).toHex().left(7);
}

QByteArray readFile(QString fileName)
{
    QFile f(fileName);
    f.open(f.ReadOnly);
    return f.readAll();
}

void initInput(Graph &g, GraphAttributes &ga, EdgeArray<std::vector<CableType> > &cableTypesPerEdge)
{
    ga.setDirected(true);

    {
        // preprocess nodes: parse "label", write it in "weight" (turbine rating / substation capacity)
        node v;
        forall_nodes(v, g) {
            string label = ga.label(v);
            ga.weight(v) = 1.0;
            if (label != "") {
                try {
                    double weight = stod(label);
                    ga.weight(v) = weight;
                } catch (const std::invalid_argument &) {
                    qFatal("Invalid node label: %s", label.c_str());
                }
            }
        }
    }

    {
        // duplicate all edges
        List<edge> edges;
        g.allEdges(edges);
        for (auto e : edges) {
            auto e2 = g.newEdge(e->target(), e->source());
            ga.label(e2) = ga.label(e);
        }

        // preprocess edges:
        // - parse edge labels as json (list of cable types)
        // - precompute edge lengths and write this in doubleWeight attribute
        edge e;
        forall_edges(e, g) {
            // cable types:
            string label = ga.label(e);
            QJsonArray json = QJsonDocument::fromJson(QByteArray(label.data(), label.size())).array();
            cableTypesPerEdge[e] = CableType::vectorFromJson(json);
            ga.label(e) = "";

            // edge lengths:
            double dx = ga.x(e->target()) - ga.x(e->source());
            double dy = ga.y(e->target()) - ga.y(e->source());
            double length = sqrt(dx * dx + dy * dy);
            ga.doubleWeight(e) = length;
        }
    }
}




}
