#ifndef GENERATOR_H
#define GENERATOR_H

#include <random>
#include <QString>


struct GeneratorParams {
    int n;
    QString target;

    // Shape
    double bmin, bmax;

    // Nodes
    int tmin, tmax;
    int smin, smax;
    double stmin, stmax;

    // Substation capacities
    double cmin, cmax, cvar;

    // Edges
    int kNearestNeighbors;
    double maxDetour;
};


class Generator
{
    GeneratorParams params;
    std::mt19937 rand;

    static constexpr double outputScale = 100.0;

    void generateOne(int id, int seed);

public:
    Generator(GeneratorParams params, int seed = 0);

    void generate();
};

#endif // GENERATOR_H
