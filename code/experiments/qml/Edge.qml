import QtQuick 2.0

Item {
    property real x1
    property real y1
    property real x2
    property real y2
    property int cable
    property real capacity
    property real flow

    opacity: 0.5

    x: (x1+x2)/2
    y: (y1+y2)/2

    Item {
        id: wrapper
        property real dx: x2-x1
        property real dy: y2-y1

        property bool isCut: cable == -1

        anchors.centerIn: parent

        width: isCut ? 3 : Math.sqrt(dx*dx+dy*dy)
        height: isCut ? 30 : Math.min(24, Math.max(2, capacity)) + 3

        rotation: Math.atan2(dy, dx) * 180 / Math.PI

        BorderImage {
            id: bi
            visible: wrapper.isCut == false
            property real xScale: .7
            property real yScale: .5
            property real usage: flow / capacity
            property string image: usage > 0 ? Math.min(9, Math.floor(usage * 10)).toString() : "empty"

            x: (root.anim - .5) * 32 * xScale

            width: parent.width / xScale
            height: parent.height / yScale

            transform: Scale{
                xScale: bi.xScale
                yScale: bi.yScale
            }

            source: "qrc:/img/edge-" + image + ".png"
            horizontalTileMode: BorderImage.Repeat
            border { top: 3; bottom: 3 }

            Rectangle {
                color: "red"
                visible: bi.usage > 1
                anchors.fill: parent
            }
        }

        Rectangle {
            color: "red"
            visible: wrapper.isCut
            anchors.fill: parent
        }
    }
}

