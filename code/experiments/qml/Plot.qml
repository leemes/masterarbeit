import QtQuick 2.0

Item {
    function push(value) {
        canvas.push(value)
    }

    Canvas {
        id: canvas
        anchors.fill: parent

        property var values: [];
        property real min: 0;
        property real max: 0;
        property bool p: false;

        function push(value) {
            values.push(value)
            if (values.length > 400)
                values.shift()
            values = values

            min = max = values[0]
            for (var i = 1; i < values.length; ++i) {
                min = Math.min(min, values[i]);
                max = Math.max(max, values[i]);
            }

            // TESTING
            min = 36228 //128000
            max = 45000 //160000

            p = true
        }

        onPChanged: if (p) requestPaint()

        Timer {
            interval: 100; running: true; repeat: true
            onTriggered: canvas.p = false
        }

        onPaint: {
            if (values.length >= 2) {
                var ctx = canvas.getContext('2d');

                ctx.clearRect(region.x, region.y, region.width, region.height)

                ctx.strokeStyle = Qt.rgba(0, .5, 0, 1);
                ctx.lineWidth = 2;
                ctx.beginPath();
                var yForValue = function(value) { return (max - value) / (max - min) * height };
                ctx.moveTo(0, yForValue(data[0]));
                for (var i = 0; i < values.length; ++i)
                    ctx.lineTo(i * width / (values.length - 1), yForValue(values[i]));
                ctx.stroke();
            }
        }
    }
}
