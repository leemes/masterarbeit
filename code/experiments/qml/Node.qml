import QtQuick 2.0

Item {
    property alias index: index.text
    property alias label: text.text
    property int rating
    property int type: 1

    readonly property int typeTurbine: 1
    readonly property int typeSubstation: 2

    width: 1
    height: 1

    Rectangle {
        width: (type == typeTurbine) ? 32 : 48
        height: width
        radius: (type == typeTurbine) ? 5 : (width / 2)
        x: -width / 2
        y: -width / 2
        color: (type == typeTurbine) ? "#ffcc00" : "#00ccff"
        border { width: 1; color: Qt.rgba(0,0,0,0.75) }

        Text {
            id: text
            anchors.centerIn: parent
            font.pointSize: 11
        }

        Text {
            id: index
            anchors.bottom: parent.top
            anchors.left: parent.left
            font.pointSize: 7
        }

        Text {
            visible: type == typeSubstation
            id: substationRating
            anchors.horizontalCenter: parent.horizontalCenter
            y: 30
            font.pointSize: 10
            font.bold: true
            text: rating
            color: "white"
        }
    }
}

