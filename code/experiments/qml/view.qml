import QtQuick 2.1

Rectangle {
    id: root

    // GLOBAL
    property real anim
    NumberAnimation {
        target: root
        property: "anim"
        loops: Animation.Infinite
        duration: 1000
        easing.type: Easing.Linear
        from: 0
        to: 1
        running: true
    }


    property int showThread: -1

    // Zoom and Scrolling:
    property real zoom: .7
    property real dx: 0
    property real dy: 0

    // The graph
    Repeater {
        model: m.graphEdges
        Edge {
            x1: modelData[0] * zoom + dx
            y1: modelData[1] * zoom + dy
            x2: modelData[2] * zoom + dx
            y2: modelData[3] * zoom + dy
            cable: modelData[4]
            capacity: modelData[5]
            flow: modelData[6]
        }
    }
    Repeater {
        model: m.graphNodes
        Node {
            index: modelData[0]
            x: modelData[1] * zoom + dx
            y: modelData[2] * zoom + dy
            type: modelData[3]
            label: modelData[4]
            rating: modelData[5]
        }
    }

    // Labels top left
    Text {
        id: textTemperature
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10
        text: "T = " + m.temperature
    }
    Text {
        id: textEnergy
        anchors.top: textTemperature.bottom
        anchors.left: parent.left
        anchors.margins: 10
        text: "E = " + m.energy
    }
    Text {
        id: textIterations
        anchors.top: textEnergy.bottom
        anchors.left: parent.left
        anchors.margins: 10
        text: (m.iterations > 10000 ? (Math.round(m.iterations / 1000) + "K") : m.iterations) + " iterations"
    }
    Text {
        id: textShowThread
        anchors.top: textIterations.bottom
        anchors.left: parent.left
        anchors.margins: 10
        font.pointSize: 20
        text: showThread
        visible: showThread != -1
    }


    ThreadMatrix {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 10
        threads: m.threads
        matrix: m.threadMatrix
    }


    Plot {
        width: parent.width
        height: 200
        anchors.bottom: parent.bottom

        property real plotValueOverTime: m.energy
        onPlotValueOverTimeChanged: push(plotValueOverTime)
    }

    Component.onCompleted: {
        m.statsPushed.connect(function(x) {
            // console.log("STATS: " + x)
        });
    }

    // KEYS
    focus: true
    Keys.onSpacePressed: {
        c.manualAction()
    }
    Keys.onPressed: {
        if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9) {
            showThread = event.key - Qt.Key_1;
            if (event.key === Qt.Key_0) showThread = 9;
            c.showThread(showThread);
        }
    }
    /*
    Keys.onReleased: {
        if (event.key >= Qt.Key_0 && event.key <= Qt.Key_9) {
            showThread = -1;
            c.showThread(showThread);
        }
    }*/
}

