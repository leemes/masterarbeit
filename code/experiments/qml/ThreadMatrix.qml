import QtQuick 2.0

Column {
    id: root
    property var threads
    property var matrix

    Row {
        spacing: 2

        Repeater {
            model: threads

            delegate: Rectangle {
                color: "red"
                width: 50; height: 50

                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    text: Math.round(modelData[1])
                    font.pixelSize: 10
                }
            }
        }
    }


    Repeater {
        model: matrix

        Row {
            spacing: 2

            Repeater {
                model: modelData

                delegate: Rectangle {
                    color: "yellow"
                    width: 50; height: 50

                    Column {
                        anchors.centerIn: parent
                        Text {
                            text: modelData[0]
                            font.pixelSize: 10
                        }
                        Text {
                            text: modelData[1]
                            font.pixelSize: 10
                        }
                    }
                }
            }
        }
    }
}
