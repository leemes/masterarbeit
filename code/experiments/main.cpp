#include <QGuiApplication>
#include <QDebug>
#include <ogdf/fileformats/GraphIO.h>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QQuickView>
#include <QQmlContext>
#include <fn.h>
#include "util.h"
#include "model.h"
#include <QCommandLineParser>
#include <QThreadPool>
#include <QTimer>
#include "computation.h"
#include "computationinstance.h"
#include "cabletype.h"
#include "lpsolverwrapper.h"
#include "generator.h"
#include <utility>
#include <memory>


using namespace ogdf;
using G = Graph;
using GA = GraphAttributes;


struct CommandLineArguments : protected util::CommandLineParser
{
    QString modeStr;
    enum Mode {
        Generate,
        LP,
        Solve
    } mode;

    bool view;

    int seed;

    GeneratorParams gen;

    QString source;
    QString config;
    int maxtime;
    double tinit, tstep, tstop;
    bool tdyn;
    int iterstop;
    double actstop;
    int iinit, imax;
    //double attr;
    double pce, pcf, pse, psf;
    int recover;
    int maturity;
    int crosscount;
    double crossratio;
    double crosstemp;
    int r;
    int j;
    int o;
    int oid;
    bool od;
    bool ss;


    CommandLineArguments(const QCoreApplication & app) {
        // MODE
        modeStr = app.arguments().count() > 1 ? app.arguments().at(1) : "";
        if (modeStr == "generate")
            mode = Generate;
        else if (modeStr == "lp")
            mode = LP;
        else if (modeStr == "solve")
            mode = Solve;
        else {
            cerr << "Invalid mode." << endl;
            cout << "Usage: experiments <mode> [...]" << endl << endl;
            cout << "Modes:" << endl;
            cout << "  generate    Generate instances" << endl;
            cout << "  lp          Solve an instance using Linear Programming" << endl;
            cout << "  solve       Solve an instance using Simulated Annealing" << endl << endl;
            cout << "To show options for a mode, run" << endl;
            cout << "  experiments <mode> --help" << endl;
            exit(0);
        }
        auto args = app.arguments();
        args.removeAt(1);
        args[0] = "experiments " + modeStr;

        // OPTIONS
        for (int process = 0; process < 2; ++process) {
            if (process)
                this->process(args);
            int i = 0;

            // General arguments for all modes:
            addOption(process, seed, "seed", "Randomness seed", "N", 0);

            // Basic arguments for generator mode:
            if (mode == Generate) {
                addPositionalArgument(process, gen.target, i, "target", "Target graph file pattern (GML), \"%1\" is replaced by index");
                addOption(process, gen.n, "n", "Number of instances to be generated", "N", 1);
                addOption(process, gen.bmin, "bmin", "Ellipse aspect ratio, minimum [0..1], default: 0.3", "N", 0.3);
                addOption(process, gen.bmax, "bmax", "Ellipse aspect ratio, maximum [0..1], default: 1", "N", 1.0);
                addOption(process, gen.tmin, "tmin", "Number of turbines, minimum", "N", 100);
                addOption(process, gen.tmax, "tmax", "Number of turbines, maximum", "N", 1000);
                addOption(process, gen.smin, "smin", "Number of substations, minimum", "N", 1);
                addOption(process, gen.smax, "smax", "Number of substations, maximum", "N", 100);
                addOption(process, gen.stmin, "stmin", "Quotient s/t, minimum [0..1], default: 0.02", "N", 0.02);
                addOption(process, gen.stmax, "stmax", "Quotient s/t, maximum [0..1], default: 0.025", "N", 0.025);
                addOption(process, gen.cmin, "cmin", "Quotient (sum of subst.cap.)/t, minimum [1..], default: 1.0", "N", 1.0);
                addOption(process, gen.cmax, "cmax", "Quotient (sum of subst.cap.)/t, maximum [1..], default: 1.1", "N", 1.1);
                addOption(process, gen.cvar, "cvar", "Variance of subst.cap. (as ratio of avg), default: 0", "N", 0.0);
                addOption(process, gen.kNearestNeighbors, "knearest", "Number of nearest neighbors to connect each node to, default: 6", "N", 6);
                addOption(process, gen.maxDetour, "maxdetour", "Maximum detour (ratio compared to euclidean distance) for any pair of nodes, default: 1.1", "N", 1.1);
            }

            // Basic arguments for solve & lp modes:
            if (mode == Solve || mode == LP) {
                addPositionalArgument(process, source, i, "source", "Source graph file (GML)", true);
                addPositionalArgument(process, config, i, "config", "Environmental configuration file (JSON)", true);
                addOption(process, maxtime, "maxtime", "Maximum running time in seconds, default: 0 (infinite)", "t", 0);
                addOption(process, view, "v", "View (1: yes, 0: no), default: 0", "0/1", false);
                addOption(process, ss, "ss", "save solution (as `out.gml'), slows down computation! (1: yes, 0: no), default: 0", "0/1", false);
            }

            // Additional arguments for solve mode:
            if (mode == Solve) {
                addOption(process, tinit, "tinit", "Temperature, initial [0..1], default: 0.01", "T", 0.01);
                addOption(process, tstep, "tstep", "Temperature decreasing factor [0..1] (in each iteration: temperature *= (1 - tstep) * activity), default: 1e-5", "T", 1e-5);
                addOption(process, tdyn, "tdyn", "Temperature decreasing factor is multiplied with activity? (dynamic temperature curve), default: 1", "0/1", true);
                addOption(process, tstop, "tstop", "*EITHER* stop when reaching temperature <T> [0..1]", "T", 0.0);
                addOption(process, iterstop, "iterstop", "*OR* stop after <N> iterations", "N", 0);
                addOption(process, actstop, "actstop", "*OR* stop when activity reaches <N>", "N", 0.0);
                //addOption(process, attr, "attr", "Substation attraction factor [0..1], default: 0.0", "N", 0.0);
                addOption(process, pce, "pce", "Penalty for cable capacity, exponent, default: 2", "N", 2.0);
                addOption(process, pcf, "pcf", "Penalty for cable capacity, factor, default: 100", "N", 1000.0);
                addOption(process, pse, "pse", "Penalty for substation capacity, exponent, default: 2", "N", 2.0);
                addOption(process, psf, "psf", "Penalty for substation capacity, factor, default: 100", "N", 100.0);
                addOption(process, recover, "recover", "Recover currently best known solution after <N> iterations, default: 10000", "N", 10000);
                addOption(process, maturity, "maturity", "Never kill instances with less than <N> iterations, default: 10000", "N", 10000);
                addOption(process, iinit, "iinit", "Number of instances, initial", "N", 1);
                addOption(process, imax, "imax", "Number of instances, maximum", "N", 1);
                addOption(process, crosscount, "crosscount", "Number of pairs of crossings to be computed, default: 0 (crossings are disabled)", "N", 0);
                addOption(process, crossratio, "crossratio", "Ratio of maxtime used for computing crossings, when enabled, default: 0.1 (expects maxtime != 0)", "N", 0.1);
                addOption(process, crosstemp, "crosstemp", "Initial temperature for crossings, as factor of avg original threads, default: 2 (= sum of temp of original threads)", "N", 2.0);
                addOption(process, r, "r", "Round size (no. of iterations per round)", "N", 1000);
                addOption(process, j, "j", "Physical threads", "N", 1);
                addOption(process, o, "o", "Output mode [1: details of first instance, 2: summary of all instances]", "mode", 2);
                addOption(process, oid, "oid", "Output ID prefixed to every line [-1: no prefix]", "ID", -1);
                addOption(process, od, "od", "Output debug info [0: no, 1: yes]", "0/1", false);
            }
        }
    }
};



int main(int argc, char *argv[])
{
    // Make QGuiApplication or QCoreApplication depending on the presence of the argument "-v"
    auto app = [&] () -> std::unique_ptr<QCoreApplication> {
        QStringList args;
        for (char **it = argv; it < argv+argc; ++it)
            args << QString(*it);
        if (args.contains("-v") || args.contains("-v1"))
            return std::unique_ptr<QCoreApplication>(new QGuiApplication(argc, argv));
        else
            return std::unique_ptr<QCoreApplication>(new QCoreApplication(argc, argv));
    }();

    // Process command line arguments
    CommandLineArguments args(*app);


    // Mode of operation:
    if (args.mode == CommandLineArguments::Generate) {
        Generator gen(args.gen, args.seed);
        gen.generate();
        return 0;
    }

    else {
        // LOAD WIND FARM
        G g;
        GA ga(g, GA::nodeGraphics | GA::nodeLabel | GA::nodeWeight
              | GA::edgeLabel | GA::edgeIntWeight | GA::edgeDoubleWeight);
        ogdf::GraphIO::readGML(ga, g, args.source.toStdString());
        ogdf::EdgeArray<std::vector<CableType> > cableTypesPerEdge(g);
        util::initInput(g, ga, cableTypesPerEdge);

        // #########################
        QJsonObject config = QJsonDocument::fromJson(util::readFile(args.config)).object();
        auto cableTypes = CableType::vectorFromJson(config["cableTypes"].toArray());
        edge e;
        forall_edges(e, g) {
            double length = ga.doubleWeight(e);
            cableTypesPerEdge[e] = fn::convert<std::vector<CableType>>(fn::map(cableTypes, [=](CableType c){
                return CableType{c.cost * length, c.capacity};
            }));
        }
        // #########################

        // OUR IMPLEMENTATION
        if (args.mode == CommandLineArguments::Solve) {
            ComputationParameters params;
            params.seed = args.seed;
            params.initialTemperature = args.tinit;
            params.deltaTemperature = args.tstep;
            params.dynamicTemperature = args.tdyn;
            params.stopTemperature = args.tstop;
            params.stopIterations = args.iterstop == 0 ? std::numeric_limits<int>::max() : args.iterstop;
            params.stopActivity = args.actstop;
            //params.attr = args.attr;
            params.penaltyCableExponent = args.pce;
            params.penaltyCableFactor = args.pcf;
            params.penaltySubstationExponent = args.pse;
            params.penaltySubstationFactor = args.psf;
            params.recover = args.recover;
            params.maturity = args.maturity;
            params.initialInstances = args.iinit;
            params.maxInstances = args.imax;
            if (args.crosscount != 0 && args.maxtime == 0)
                qFatal("When enabling crossings, maxtime has to be set");
            params.startCrossingsAfterSeconds = args.maxtime * (1.0 - args.crossratio);
            params.crossingsCount = args.crosscount;
            params.crossingsTempFactor = args.crosstemp;
            params.iterationsPerRound = args.r;
            params.physicalThreads = args.j;
            ComputationOutput out;
            out.mode = (ComputationOutput::Mode)args.o;
            out.id = args.oid;
            out.debug = args.od;
            out.saveSolution = args.ss;
            Computation *computation = new Computation(g, ga, cableTypesPerEdge, params, out);
            QRunnable *computationRunnable = computation;
            QObject *computationObject = computation;
            QThreadPool::globalInstance()->start(computationRunnable, -1);
            QTimer timer;
            QObject::connect(&timer, &QTimer::timeout, [=]{ computation->renderIntoModel(); });
            timer.setInterval(1000);
            timer.start();
            QObject::connect(computationObject, SIGNAL(done()), &*app, SLOT(quit()));

            if (args.maxtime) {
                QTimer::singleShot(args.maxtime * 1000, computationObject, SLOT(stop()));
            }

            // VIEW
            if (args.view) {
                Model m;
                m.setGraph(g, ga);
                computation->setModel(&m);
                QQuickView v;
                v.rootContext()->setContextProperty("m", &m);
                v.setResizeMode(QQuickView::SizeRootObjectToView);
                v.setSource(QUrl("qrc:/qml/view.qml"));
                v.show();
                v.setGeometry(0, 27, 1920, 800);
                v.rootContext()->setContextProperty("c", computationObject);

                return app->exec();
            } else {
                return app->exec();
            }
        }


        // EXACT IMPLEMENTATION (LINEAR PROGRAM)
        if (args.mode == CommandLineArguments::LP) {
            auto lpSolver = new LPSolverWrapper(g, ga, cableTypesPerEdge, args.maxtime, args.ss);
            QThreadPool::globalInstance()->start(lpSolver, -1);
            QObject::connect(lpSolver, SIGNAL(done()), &*app, SLOT(quit()));

            // VIEW
            if (args.view) {
                Model m;
                m.setGraph(g, ga);
                lpSolver->setModel(&m);
                QQuickView v;
                v.rootContext()->setContextProperty("m", &m);
                v.setResizeMode(QQuickView::SizeRootObjectToView);
                v.setSource(QUrl("qrc:/qml/view.qml"));
                v.show();
                v.setGeometry(0, 27, 1920, 800);

                return app->exec();
            } else {
                return app->exec();
            }
        }
    }
}
