#include "computationfixedassignment.h"
#include "computationinstance.h"
#include "util.h"
#include <fn.h>
#include <QDebug>
#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/basic/Graph_d.h>

using namespace ogdf;


ComputationFixedAssignment::ComputationFixedAssignment(Model *&model, const Graph &g, const GraphAttributes &ga,
                                                       const EdgeArray<std::vector<CableType> > &cableTypesPerEdge,
                                                       int seed, ComputationParameters params, QVector<int> assignment) :
    g(g), ga(ga), cableTypesPerEdge(cableTypesPerEdge), seed(seed),
    params(params), m(model)
{
    setAutoDelete(false);
    initInstances(assignment);
}

void ComputationFixedAssignment::run()
{
    for (int i = 0; i < 40; ++i) {
        for (auto instance : instances)
            instance->run();

        qDebug() << "   round" << i << "--> energy =" << energy();
    }
}

double ComputationFixedAssignment::energy() const
{
    return fn::sum(fn::map(instances, &ComputationInstance::energy));
}

void ComputationFixedAssignment::initInstances(QVector<int> assignment)
{
    qDebug() << util::assignmentToString(assignment);

    // List substations and turbines in individual vectors
    QVector<node> substations;
    QVector<node> turbines;
    QVector<node> nodes;
    node v;
    forall_nodes(v, g) {
        if (util::shapeToType(ga.shape(v)) == util::Turbine)
            turbines.push_back(v);
        else
            substations.push_back(v);
        nodes.push_back(v);
    }

    // Adjacency lists
    std::vector<std::vector<int> > neighbors(nodes.size());
    forall_nodes(v, g) {
        int vi = v->index();
        edge e;
        forall_adj_edges(e, v) {
            node w = e->target();
            int wi = w->index();
            if (w != v) {
                neighbors[vi].push_back(wi);
                neighbors[wi].push_back(vi);
            }
        }
    }



    bool ok = false;
    int counter = 0;

    while (!ok) {

        // Create one subgraph for every substation
        std::vector<Graph> subgraphs(substations.size());
        std::vector<GraphAttributes> subgraphs_a(substations.size());
        std::vector<EdgeArray<std::vector<CableType> > > subgraphs_ctpes(substations.size());
        std::vector<NodeArray<int> > subgraphs_originalIndexOfNode(substations.size());

        for (node s : substations)
        {
            int sIndex = substations.indexOf(s);
            Graph & sg = subgraphs[sIndex];

            // Create nodes in the subgraph
            QVector<node> newNodeByOldIndex(g.numberOfNodes(), nullptr);
            NodeArray<int> & originalIndexOfNode = subgraphs_originalIndexOfNode[sIndex];
            originalIndexOfNode = NodeArray<int>(sg);
            node v;
            forall_nodes(v, g) {
                int turbineIndex = turbines.indexOf(v);
                if (v == s || turbineIndex != -1) {
                    bool check = (v == s || assignment[turbineIndex] == sIndex);
                    if (check) {
                        node newV = sg.newNode();
                        newNodeByOldIndex[v->index()] = newV;
                        originalIndexOfNode[newV] = v->index();
                    }
                }
            }

            // Create edges in the subgraph
            QVector<edge> newEdgeByOldIndex(g.numberOfEdges(), nullptr);
            edge e;
            forall_edges(e, g) {
                node newS = newNodeByOldIndex[e->source()->index()];
                node newT = newNodeByOldIndex[e->target()->index()];
                if (newS && newT) {
                    edge newE = sg.newEdge(newS, newT);
                    newEdgeByOldIndex[e->index()] = newE;
                }
            }

            // Copy graph attributes and cable types
            GraphAttributes & sga = subgraphs_a[sIndex];
            sga = GraphAttributes(sg, ga.attributes());
            EdgeArray<std::vector<CableType> > & sctpe = subgraphs_ctpes[sIndex];
            sctpe = EdgeArray<std::vector<CableType> >(sg);
            forall_nodes(v, g) {
                node newV = newNodeByOldIndex[v->index()];
                if (newV) {
                    sga.x(newV) = ga.x(v);
                    sga.y(newV) = ga.y(v);
                    sga.width(newV) = ga.width(v);
                    sga.height(newV) = ga.height(v);
                    sga.shape(newV) = ga.shape(v);
                    sga.label(newV) = ga.label(v);
                    sga.weight(newV) = ga.weight(v);
                }
            }
            forall_edges(e, g) {
                edge newE = newEdgeByOldIndex[e->index()];
                if (newE) {
                    sga.label(newE) = ga.label(e);
                    sga.intWeight(newE) = ga.intWeight(e);
                    sga.doubleWeight(newE) = ga.doubleWeight(e);
                    sctpe[newE] = cableTypesPerEdge[e];
                }
            }
        }

        // Test graphs for connectedness,
        // swap nodes in assignment to fix that
        ok = true;
        for (node s : substations) {
            int sIndex = substations.indexOf(s);
            Graph & sg = subgraphs[sIndex];
            NodeArray<int> & originalIndexOfNode = subgraphs_originalIndexOfNode[sIndex];

            Graph::CCsInfo ccs(sg);
            if (ccs.numberOfCCs() != 1) {
                ok = false;

                // find smallest cc
                int cc = fn::min(fn::seq(0, ccs.numberOfCCs()), [&](int cc){return ccs.numberOfNodes(cc);});

                node v = ccs.v(ccs.startNode(cc));
                int vi = originalIndexOfNode[v];
                // Find best neighboring node for swapping
                int wi = fn::max(neighbors[vi], [&](int wi){
                    // Count edges added to our subgraph, i.e. neighbors which connect to substation <sIndex>
                    return fn::sum(fn::map(neighbors[wi], [&](int xi){
                        int turbineIndex = turbines.indexOf(nodes[xi]);
                        if (turbineIndex != -1)
                            return assignment[turbineIndex] == sIndex ? 1 : 0;
                        else return 0;
                    }));
                });

                // Swap two entries in assignment
                int vTurbine = turbines.indexOf(nodes[vi]);
                int wTurbine = turbines.indexOf(nodes[wi]);
                std::swap(assignment[vTurbine], assignment[wTurbine]);

                qDebug() << sIndex << "not connected, " << "cc" << cc << "has" << ccs.numberOfNodes(cc) << "nodes";
            }
            GraphAttributes & sga = subgraphs_a[sIndex];
            ogdf::GraphIO::writeGML(sga, QString("debug-try-%1-substation-%2.gml").arg(counter).arg(sIndex).toStdString());
        }

        counter++;

        if (ok) {
            for (node s : substations) {
                int sIndex = substations.indexOf(s);
                Graph & sg = subgraphs[sIndex];
                GraphAttributes & sga = subgraphs_a[sIndex];
                EdgeArray<std::vector<CableType> > & sctpe = subgraphs_ctpes[sIndex];


                ogdf::GraphIO::writeGML(sga, QString("debug-%1.gml").arg(sIndex).toStdString());

                // Create computation instance
                auto instance = new ComputationInstance(m, sg, sga, sctpe, seed, nullptr, params);
                instances.push_back(instance);
            }
        }
    }

    qDebug() << instances.size() << "instances out of" << substations.size();
}
