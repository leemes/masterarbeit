#include "generator.h"
#include <random>
#include <functional>
#include <utility>
#include <iomanip>
#include <set>
#include <unordered_set>
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/fileformats/GraphIO.h>
#include <QDebug>
#include "util.h"
#include "fn.h"

using namespace ogdf;

struct Point {
    double x, y;
};

template<typename T, typename RNG>
T randLog(T min, T max, RNG && rng) {
    auto minLog = log(min);
    auto maxLog = log(max);
    auto resultLog = std::uniform_real_distribution<>(minLog, maxLog)(rng);
    return exp(resultLog);
}



Generator::Generator(GeneratorParams params, int seed) :
    params(params), rand(seed)
{

}

void Generator::generate()
{
    for (int i = 0; i < params.n; ++i) {
        int seed = rand();
        generateOne(i, seed);
    }
}

void Generator::generateOne(int id, int seed)
{
    std::mt19937 rand(seed);

    // Params
    double b = randLog(params.bmin, params.bmax, rand);
    int t, s;
    double st;
    do {
        t = randLog(params.tmin, params.tmax, rand);
        s = randLog(params.smin, params.smax, rand);
        st = double(s) / t;
    } while (st != qBound(params.stmin, st, params.stmax));
    double capacityRatio = randLog(params.cmin, params.cmax, rand);
    double capacitySum = t * capacityRatio;
    double capacityAvg = capacitySum / s;
    double capacityVar = capacityAvg * params.cvar;
    double capacityMin = capacityAvg - capacityVar;
    double capacityMax = capacityAvg + capacityVar;


    // Shape
    int n = t + s;
    double xExtend = sqrt(n / b);
    double yExtend = sqrt(n * b);
    double xRadius = xExtend / 2;
    double yRadius = yExtend / 2;
    auto randPoint = [&]{
        double r = std::uniform_real_distribution<>(0.0, 1.0)(rand);
        double phi = std::uniform_real_distribution<>(0.0, 2.0 * M_PI)(rand);
        double x = sqrt(r) * cos(phi) * xRadius;
        double y = sqrt(r) * sin(phi) * yRadius;
        return Point{x, y};
    };
    auto distance = [](Point a, Point b) {
        double dx = a.x - b.x;
        double dy = a.y - b.y;
        return sqrt(dx * dx + dy * dy);
    };
    auto tooNear = [&](Point a, double maxDistance){
        return [&,a,maxDistance](Point b) {
            return distance(a, b) < maxDistance;
        };
    };
    auto randPointNotTooNear = [&](QVector<Point> points, double & maxDistance){
        while (true) {
            auto p = randPoint();
            if (std::find_if(points.begin(), points.end(), tooNear(p, maxDistance)) == points.end())
                return p;
            else
                maxDistance *= (1 - 1e-5);
        }
    };
    auto randPointsPDS = [&](int count, double density){
        QVector<Point> points;
        double maxDistance = density;
        while (points.count() < count) {
            auto p = randPointNotTooNear(points, maxDistance);
            points << p;
        }
        return points;
    };

    //-------------------------------------------------------------------------

    // The graph
    Graph g;
    GraphAttributes ga(g, GraphAttributes::nodeGraphics | GraphAttributes::nodeStyle | GraphAttributes::nodeLabel | GraphAttributes::edgeGraphics | GraphAttributes::edgeStyle);
    ga.setDirected(false);

    //-------------------------------------------------------------------------

    // NODES

    QVector<node> nodes;

    // Generate turbines
    QVector<Point> turbines = randPointsPDS(t, 1.0);
    for (Point p : turbines) {
        auto v = g.newNode();
        nodes << v;
        ga.x(v) = (p.x + xRadius) * outputScale;
        ga.y(v) = (p.y + yRadius) * outputScale;
        ga.shape(v) = util::typeToShape(util::Turbine);
        ga.width(v) = ga.height(v) = 10;
        ga.fillColor(v) = Color::Slateblue;
    }


    // Generate capacities such that their sum matches the target sum
    QVector<int> capacities;
    if (s == 1) {
        capacities << capacityAvg;
    } else {
        for (int i = 0; i < s; ++i) {
            double t = double(i) / double(s-1);
            double cap = (1-t) * capacityMin + t * capacityMax;
            capacities << qRound(cap);
        }
    }


    // Generate substations
    QVector<Point> substations = randPointsPDS(s, sqrt(double(t) / s));
    for (Point p : substations) {
        auto v = g.newNode();
        nodes << v;
        ga.x(v) = (p.x + xRadius) * outputScale;
        ga.y(v) = (p.y + yRadius) * outputScale;
        ga.shape(v) = util::typeToShape(util::Substation);
        ga.fillColor(v) = Color::Orange;
        int cap = capacities.takeLast();
        ga.label(v) = std::to_string(cap);
    }

    QVector<Point> location = turbines + substations;

    //-------------------------------------------------------------------------

    // EDGES

    // Phase 1: Start by connecting each node to its <k> nearest neighbors

    for (int v = 0; v < n; ++v) {
        // List all other nodes together with their (euclidean) distance
        std::vector<std::pair<int,double>> vec;
        for (int w = 0; w < n; ++w) {
            if (v != w) {
                auto dist = distance(location[v], location[w]);
                vec.emplace_back(w, dist);
            }
        }
        // Find the nearest k neighbors
        int k = std::min(n-1, params.kNearestNeighbors);
        std::partial_sort(vec.begin(), vec.begin() + k, vec.end(), [](std::pair<int,double> a, std::pair<int,double> b){
            return a.second < b.second;
        });
        for (int w : fn::map(fn::makeRange(vec.begin(), vec.begin() + k), fn::getKey)) {
            if (!g.searchEdge(nodes[w], nodes[v])) // avoid doubly-directed edges
                g.newEdge(nodes[v], nodes[w]);
        }
    }


    // Phase 2: Add shortcut for paths of two edges, if the detour exceeds <maxDetour>

    // List all neighbors for each node
    auto getCurrentNeighbors = [&](int v) {
        std::vector<int> neighborhood;
        adjEntry a;
        forall_adj(a, nodes[v]) {
            node n = a->twinNode();
            neighborhood.push_back(n->index());
        }
        return neighborhood;
    };
    std::vector<std::vector<int>> neighbors = fn::map(fn::seq(0,n), getCurrentNeighbors);

    // List all paths consisting of two edges
    struct PairHash {
        inline std::size_t operator()(const std::pair<int,int> & v) const {
            return (v.first * 123653) ^ v.second;
        }
    };
    std::unordered_set<std::pair<int,int>, PairHash> paths;
    for (int v = 0; v < n; ++v) {
        auto neighborhood = neighbors[v];
        // Iterate over all 2-edge-paths (u,v,w)
        for (int u : neighborhood) {
            for (int w : neighborhood) {
                if (u != v && v != w && u != w) {
                    if (!g.searchEdge(nodes[u], nodes[w])) {
                        paths.insert(std::minmax(u, w));
                    }
                }
            }
        }
    }

    // Remove all paths for which a direct connection is already "good enough"
    for (int v = 0; v < n; ++v) {
        auto neighborhood = neighbors[v];
        // Iterate over all 2-edge-paths (u,v,w)
        for (int u : neighborhood) {
            for (int w : neighborhood) {
                if (u != v && v != w && u != w) {
                    if (!g.searchEdge(nodes[u], nodes[w])) {
                        // If detour is good enough, remove it from the set
                        double distanceIndirect = distance(location[u], location[v]) + distance(location[v], location[w]);
                        double distanceDirect = distance(location[u], location[w]);
                        if (distanceIndirect < params.maxDetour * distanceDirect)
                            paths.erase(std::minmax(u, w));
                    }
                }
            }
        }
    }

    // Add shortcuts to the remaining paths
    for (auto p : paths) {
        edge e = g.newEdge(nodes[p.first], nodes[p.second]);
        ga.strokeColor(e) = Color::Gray;
    }


    //-------------------------------------------------------------------------

    // Output
    std::string filename = params.target.arg(id).toStdString();
    GraphIO::writeGML(ga, filename);

    using namespace std;
    cout << setw(10) << id
         << setw(10) << b
         << setw(10) << t
         << setw(10) << s
         << setw(10) << capacityRatio
         << "  " << filename
         << endl;
}
