#ifndef COMPUTATION_H
#define COMPUTATION_H

#include <QRunnable>
#include <ogdf/basic/Array.h>
#include <ogdf/basic/NodeArray.h>
#include <QVector>
#include <QHash>
#include <QList>
#include <QQueue>
#include "cabletype.h"
#include <QThreadPool>
#include <QMutex>
#include <QElapsedTimer>

class Model;
class ComputationInstance;
class ComputationFixedAssignment;

namespace ogdf {
class Graph;
class GraphAttributes;
}



struct ComputationParameters
{
    int seed = 0;

    // temperature curve
    double initialTemperature;
    double deltaTemperature = 1e-5;
    bool dynamicTemperature;
    double stopTemperature;
    double activityEMAAlpha = 0.001;
    double stopActivity = 0.01;
    int stopIterations;

    int recover; // after how many iterations with no improvement over the all-time best solution, the computation restarts at the all-time best solution
    int maturity; // after how many iterations an instance is considered "mature"

    // Attraction
    //double attr;

    // penalties
    double penaltySubstationExponent;
    double penaltySubstationFactor;
    double penaltyCableExponent;
    double penaltyCableFactor;

    // multiple instances
    int initialInstances;
    int maxInstances;
    int iterationsPerRound;

    // parallelism
    int physicalThreads;

    // crossings
    double startCrossingsAfterSeconds;
    int crossingsCount;
    double crossingsTempFactor;
};

struct ComputationOutput
{
    int id;
    enum Mode {
        ModeSingleInstance = 1,
        ModeMultipleInstances = 2
    } mode;
    bool debug;
    bool saveSolution;
};


class Computation : public QObject, public QRunnable, private QMutex
{
    Q_OBJECT

    ogdf::Graph &g;
    const ogdf::GraphAttributes &ga;
    ogdf::EdgeArray<std::vector<CableType> > cableTypesPerEdge;

    ComputationParameters params;
    ComputationOutput out;

    std::mt19937 seedGenerator;
    QElapsedTimer timer;
    Model *m;
    int m_showThread = -1;

    QThreadPool threads;
    QList<ComputationInstance*> runningInstances;
    QQueue<ComputationInstance*> idleInstances;
    QVector<ComputationInstance*> allInstances;
    QVector<ComputationInstance*> doneInstances;
    bool shouldTerminate;

    QHash<QVector<int>, double> assignmentEnergy;
    double globalBestEnergy;

    QVector<ComputationFixedAssignment*> fixedAssignments;

public:
    Computation(ogdf::Graph &g, const ogdf::GraphAttributes &ga, const ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge, ComputationParameters params, ComputationOutput out);

    void run();

    void setModel(Model *);
    void renderIntoModel();

    // Callbacks from computation instances:
    void aboutToGetWorse(ComputationInstance *, double probability);
    void instancePaused(ComputationInstance *, bool done);

public slots:
    void manualAction();
    void showThread(int index);

    void stop();

signals:
    void done();

    // (private)
    void startNextAsync();

private:
    void spawn();
    Q_SLOT void startNext();
    void killWorst();
    ComputationInstance *findWorstWithProtection();
    ComputationInstance *findBest();
    void crossBestNPairs(int n);

    void startSolverForAssignment(QVector<int> assignment);

    void pushStats();
    void updateThreadsInfo();
    void debugInstances();
    void printInfo(ComputationInstance *instance);
    void printFinalResult();
};

#endif // COMPUTATION_H
