#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QMutex>
#include <QVariantList>

namespace ogdf {
class Graph;
class GraphAttributes;
}

class Model : public QObject, private QMutex
{
    Q_OBJECT
    Q_PROPERTY(int iterations READ iterations WRITE setIterations NOTIFY changed)
    Q_PROPERTY(int thread READ thread WRITE setThread NOTIFY changed)
    Q_PROPERTY(double temperature READ temperature WRITE setTemperature NOTIFY changed)
    Q_PROPERTY(double energy READ energy WRITE setEnergy NOTIFY changed)
    Q_PROPERTY(QVariantList graphNodes READ graphNodes WRITE setGraphNodes NOTIFY changed)
    Q_PROPERTY(QVariantList graphEdges READ graphEdges WRITE setGraphEdges NOTIFY changed)
    Q_PROPERTY(QVariantList threads READ threads WRITE setThreads NOTIFY changed)
    Q_PROPERTY(QVariantList threadMatrix READ threadMatrix WRITE setThreadMatrix NOTIFY changed)

    bool dirty = false;

    int m_iterations = 0;
    int m_thread = 0;
    double m_temperature = 1.0;
    double m_energy = std::numeric_limits<double>::max();
    QVariantList m_graphNodes;
    QVariantList m_graphEdges;
    QVariantList m_threads;
    QVariantList m_threadMatrix;

public:
    explicit Model(QObject *parent = 0);

    bool isClean() const { return !dirty; }

    int iterations() const;
    int thread() const;
    double temperature() const;
    double energy() const;
    QVariantList graphNodes();
    QVariantList graphEdges();
    QVariantList threads() const;
    QVariantList threadMatrix() const;

signals:
    void changed();
    void statsPushed(qreal);

public slots:
    void setIterations(int arg);
    void setThread(int thread);
    void setTemperature(double temperature);
    void setEnergy(double arg);
    void setGraphNodes(QVariantList graphNodes);
    void setGraphEdges(QVariantList graphEdges);
    void setGraph(const ogdf::Graph &g, const ogdf::GraphAttributes &ga);
    void setThreads(QVariantList threads);
    void setThreadMatrix(QVariantList threadMatrix);

    void pushStats(qreal);

signals:
    void internalBecameDirty();

protected:
    void markDirty();

protected slots:
    void signalChanges();
};

#endif // MODEL_H
