#ifndef COMPUTATIONFIXEDASSIGNMENT_H
#define COMPUTATIONFIXEDASSIGNMENT_H

#include <QRunnable>
#include <ogdf/basic/Array.h>
#include <ogdf/basic/NodeArray.h>
#include <ogdf/basic/GraphAttributes.h>
#include <QVector>
#include <QMutex>
#include "cabletype.h"
#include "simulatedannealing.h"
#include "computation.h"

class Model;

namespace ogdf {
class Graph;
}

class ComputationFixedAssignment : public QRunnable, private QMutex
{
    const ogdf::Graph &g;
    const ogdf::GraphAttributes &ga;
    ogdf::EdgeArray<std::vector<CableType> > cableTypesPerEdge;
    int seed;
    ComputationParameters params;
    Model *&m;

    // The instances of the solvers for single substation cable layout problems
    std::vector<ComputationInstance*> instances;

public:
    ComputationFixedAssignment(Model *&model, const ogdf::Graph &g, const ogdf::GraphAttributes &ga,
                               const ogdf::EdgeArray<std::vector<CableType> > & cableTypesPerEdge,
                               int seed, ComputationParameters params, QVector<int> assignment);

    void run();

    double energy() const;

private:
    void initInstances(QVector<int> assignment);
};

#endif // COMPUTATIONFIXEDASSIGNMENT_H
