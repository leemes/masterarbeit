#include "computation.h"
#include "computationfixedassignment.h"
#include "computationinstance.h"
#include <QDebug>
#include <QMutexLocker>
#include <fn.h>
#include "model.h"
#include "util.h"
#include <qhash.h>
#include <iomanip>
using namespace std;


#if (QT_VERSION < QT_VERSION_CHECK(5, 7, 0))
template<typename T>
uint qHash(const QVector<T> & vector, uint seed) {
    return qHashRange(vector.begin(), vector.end(), seed);
}
#endif



Computation::Computation(ogdf::Graph &g, const ogdf::GraphAttributes &ga, const ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge, ComputationParameters params, ComputationOutput out) :
    g(g), ga(ga), cableTypesPerEdge(cableTypesPerEdge),
    params(params), out(out),
    seedGenerator(params.seed), m(nullptr),
    globalBestEnergy(std::numeric_limits<double>::max())
{
    setAutoDelete(false);
    threads.setMaxThreadCount(params.physicalThreads + 1);
    connect(this, SIGNAL(startNextAsync()), SLOT(startNext()), Qt::QueuedConnection);
}

void Computation::run()
{
    QThread::currentThread()->setObjectName("Master");

    shouldTerminate = false;
    timer.start();

    for (int i = 0; i < params.initialInstances; ++i) {
        spawn();
        startNext();
    }

    if (params.crossingsCount) {
        // Wait for crossings to be started
        while (timer.elapsed() / 1000.0 < params.startCrossingsAfterSeconds) {
            QThread::msleep(100);
        }

        // Remember the "original" instances
        auto original = allInstances;

        // Start crossings
        crossBestNPairs(params.crossingsCount);

        // Kill the original instances, such that the rest of the running time only the crossings are evaluated
        for (auto instance : original) {
            instance->kill();
        }
    }

    while (!shouldTerminate) {
        QThread::msleep(100);
    }

    // Stop all running threads and wait for them
    for (auto instance : allInstances) {
        instance->kill();
    }
    while (threads.activeThreadCount() > 0) {
        QThread::msleep(100);
    }

    printFinalResult();

    emit done();
}

void Computation::setModel(Model *model)
{
    m = model;
}

void Computation::renderIntoModel()
{
    if (m) {
        QMutexLocker synchronize(this);

        if (!allInstances.isEmpty()) {
            if (m_showThread == -1 || m_showThread >= allInstances.size()) {
                auto best = fn::min(allInstances, &ComputationInstance::energy);
                best->renderIntoModel();
            } else {
                allInstances[m_showThread]->renderIntoModel();
            }
        }

        updateThreadsInfo();
    }

    if (out.saveSolution) {
        auto best = fn::min(allInstances, &ComputationInstance::energy);
        best->saveSolution("out.gml");
    }
}

void Computation::aboutToGetWorse(ComputationInstance *instance, double probability)
{
    QMutexLocker synchronize(this);

    auto assignment = instance->assignment();
    if (out.debug) {
       /* if (!assignmentEnergy.contains(assignment))
            qDebug() << "New assignment found: thread " << instance->id() << " => " << qPrintable(util::assignmentToString(assignment)) << " with energy:" << instance->energy();
            */
    }
    assignmentEnergy[assignment] = instance->energy();


    /*
    if (instance != findWorstWithProtection()
            && probability < 0.125 * pow(1 - instance->activity(), 8) * instance->activity())
    {
        auto seed = randSeed();
        auto clone = instance->clone(seed);
        qDebug() << "Cloning: " << instance->id() << "->" << clone->id() << "  because it got worse with prob" << probability << " and activity" << instance->activity();
        killWorst();
        allInstances.append(clone);
        idleInstances.enqueue(clone);
    }
    */
}

void Computation::instancePaused(ComputationInstance *instance, bool done)
{
    QMutexLocker synchronize(this);

    //###
    //if (instance->numberOfIterations() == 100000)
    //    startSolverForAssignment(findBest()->assignment());

    // Update assignment's energy
    auto assignment = instance->assignment();
    assignmentEnergy[assignment] = instance->energy();

    // Update global best solution
    globalBestEnergy = min(globalBestEnergy, instance->energy());

    printInfo(instance);

    // Move instance from "running" to "idle" or "done" or kill it
    runningInstances.removeOne(instance);
    if (instance->wasKilled()) {
        allInstances.removeOne(instance);
        delete instance;
    } else if (done) {
        doneInstances.append(instance);
    } else {
        idleInstances.enqueue(instance);
    }

    // Any running or idle instances left?
    if (runningInstances.empty() && idleInstances.empty())
        shouldTerminate = true;
    else {
        // Start the next instance
        if (runningInstances.count() < params.physicalThreads)
            emit startNextAsync();
    }

    debugInstances();
}

void Computation::manualAction()
{
    //killWorst();
    //for (auto instance : allInstances)
    //    instance->manualAction();
    //spawn();
    //allInstances.first()->manualAction();

    allInstances[0]->saveSolution("debug.gml");
    startSolverForAssignment(allInstances[0]->assignment());
}

void Computation::showThread(int index)
{
    m_showThread = index;
    renderIntoModel();
}

void Computation::stop()
{
    shouldTerminate = true;
}

void Computation::spawn()
{
    QMutexLocker synchronize(this);

    auto seed = seedGenerator();
    auto instance = new ComputationInstance(m, g, ga, cableTypesPerEdge, seed, this, params);

    allInstances << instance;
    idleInstances.enqueue(instance);

    if (out.debug)
        qDebug() << "SPAWN NEW INSTANCE: id =" << allInstances.count() << ", seed =" << seed;

    printInfo(instance);

    debugInstances();
}

void Computation::startNext()
{
    if (!idleInstances.isEmpty() && runningInstances.count() < params.physicalThreads) {
        auto instance = idleInstances.head();
        threads.start(instance);
        idleInstances.dequeue();
        runningInstances.append(instance);
        debugInstances();
    }
}

void Computation::killWorst()
{
    if (!allInstances.isEmpty()) {
        auto worst = findWorstWithProtection();
        if (runningInstances.contains(worst)) {
            // kill deletion of the object and from lists until it is done with current computation
            worst->kill();
        } else {
            idleInstances.removeOne(worst);
            allInstances.removeOne(worst);
            delete worst;
        }
    }
}

ComputationInstance *Computation::findWorstWithProtection()
{
    // Unprotected instances
    std::vector<ComputationInstance*> unprotected;
    for (ComputationInstance* instance : allInstances)
        if (instance->activity() < .01)
            unprotected.push_back(instance);

    if (!unprotected.empty())
        return fn::max(unprotected, &ComputationInstance::energy);
    else
        return nullptr;
}

ComputationInstance *Computation::findBest()
{
    return fn::min(allInstances, &ComputationInstance::energy);
}

void Computation::crossBestNPairs(int n)
{
    // Mature instances
    std::vector<ComputationInstance*> matureInstances;
    for (ComputationInstance* instance : allInstances)
        if (instance->mature())
            matureInstances.push_back(instance);

    // Find pair with high compatibility and high difference
    if (matureInstances.size() >= 2) {
        using instance_pair = std::pair<ComputationInstance*,ComputationInstance*>;
        std::vector<instance_pair> pairs;
        for (uint i = 0; i < matureInstances.size(); ++i) {
            for (uint j = 0; j < i; ++j)
                pairs.emplace_back(matureInstances[i], matureInstances[j]);
        }

        std::sort(pairs.begin(), pairs.end(), [&](instance_pair p, instance_pair q){
            auto key = [](instance_pair p) {
                auto a = p.first;
                auto b = p.second;
                return ComputationInstance::pairwiseDifference(a, b) / ComputationInstance::pairwiseCompatibility(a, b);
            };
            return key(p) < key(q);
        });

        for (int i = 0; i < min(n, int(pairs.size())); ++i) {
            auto pair = pairs.at(i);

            auto seed = seedGenerator();
            auto instances = ComputationInstance::crossOver(pair.first, pair.second, seed, params.crossingsTempFactor);

            allInstances << instances.first;
            idleInstances.enqueue(instances.first);

            allInstances << instances.second;
            idleInstances.enqueue(instances.second);
        }
    }
    else
        qDebug() << "Not enough mature instances available";
}

void Computation::startSolverForAssignment(QVector<int> assignment)
{
    auto seed = seedGenerator();
    auto fixed = new ComputationFixedAssignment(m, g, ga, cableTypesPerEdge, seed, params, assignment);

    fixedAssignments << fixed;

    fixed->run();
}

void Computation::pushStats()
{
    if (m) {
        m->pushStats(1);
    }
}

void Computation::updateThreadsInfo()
{
    if (m) {
        m->setThreads(fn::map(allInstances, [](ComputationInstance *instance){
                          return QVariantList{
                              instance->id(),
                              instance->energy(),
                              instance->activity(),
                              instance->isRunning()
                          };
                      }));

        m->setThreadMatrix(fn::map(allInstances, [this](ComputationInstance *a){
                               return fn::convert<QVariantList>(fn::map(allInstances, [a](ComputationInstance *b){
                                   return QVariantList{
                                       ComputationInstance::pairwiseDifference(a, b),
                                       ComputationInstance::pairwiseCompatibility(a, b)
                                   };
                               }));
                           }));
    }
}

void Computation::debugInstances()
{
    if (!out.debug)
        return;


    auto dbg = qDebug();

    dbg << (QString::number(timer.elapsed() / 1000).rightJustified(3) + "s |").toStdString().c_str();

    for (int i = 0; i < allInstances.count(); ++i)
    {
        auto instance = allInstances[i];
        int j = idleInstances.indexOf(instance);
        if (j != -1)
            dbg << QString::number(j).rightJustified(3).toStdString().c_str();
        else
            dbg << "###";
    }

    /*
    if (!allInstances.isEmpty()) {
        auto best = fn::min(allInstances, &ComputationInstance::energy);
        dbg << best->energy();
    }
    */

    auto worst = findWorstWithProtection();
    for (auto instance : allInstances)
    {
        dbg << "|" << QString::number(int(instance->id())).rightJustified(3).toStdString().c_str();
        dbg << (worst == instance ? "#" : " ");
        dbg << QString::number(int(instance->energy())).rightJustified(8).toStdString().c_str();
        dbg << QString::number(instance->activity(), 'f', 6).rightJustified(9).toStdString().c_str();
    }
}

void Computation::printInfo(ComputationInstance *instance)
{
    //auto allAssignments = assignmentEnergy.keys();
    //auto bestAssignment = fn::min(allAssignments, [this](const QVector<int> & a, const QVector<int> & b){ return assignmentEnergy[a] < assignmentEnergy[b]; });

    if (shouldTerminate)
        return;


    // ID
    if (out.id != -1)
        cout << setw(16) << out.id << ' ';

    // Time
    cout << setw(10) << setprecision(3) << std::fixed << (timer.elapsed() / 1000.0) << ' ';
    cout.unsetf(ios_base::floatfield);

    // Instance
    if (out.mode != ComputationOutput::ModeSingleInstance) {
        cout << setw(4) << instance->id() << ' ';
    }

    // Data
    cout << setw(10) << instance->numberOfIterations() << ' ';
    cout << setw(20) << setprecision(11) << instance->energy() << ' ';
    cout << setw(16) << setprecision(11) << std::fixed << instance->temperature() << ' ';
    cout << setw(16) << setprecision(11) << std::fixed << instance->activity() << ' ';
    cout << setw(4) << instance->wasKilled() << ' ';
    cout << ' ' << qPrintable(util::assignmentHash(instance->assignment())) << ' ';
    cout << setw(4) << instance->penaltiesApplied() << ' ';

    cout << endl;

    /*
    cout << setw(10) << allInstances.count() << ' '
         << setw(10) << assignmentEnergy[bestAssignment] << ' '
         << qPrintable(util::assignmentToString(bestAssignment)) << ' '
         << endl;*/
}

void Computation::printFinalResult()
{
    // ID
    if (out.id != -1)
        cout << setw(16) << out.id << ' ';

    // Time
    cout << setw(10) << setprecision(3) << std::fixed << (timer.elapsed() / 1000.0) << ' ';
    cout.unsetf(ios_base::floatfield);

    // Instance
    if (out.mode != ComputationOutput::ModeSingleInstance) {
        cout << setw(4) << -1 << ' ';
    }

    // Data
    cout << setw(10) << -1 << ' ';
    cout << setw(20) << setprecision(11) << globalBestEnergy << ' ';
    cout << setw(16) << setprecision(11) << std::fixed << -1 << ' ';
    cout << setw(16) << setprecision(11) << std::fixed << -1 << ' ';
    cout << setw(4) << -1 << ' ';
    cout << ' ' << -1 << ' ';
    cout << setw(4) << -1 << ' ';

    cout << endl;
}

