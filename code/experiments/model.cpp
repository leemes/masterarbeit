#include "model.h"
#include <QMutexLocker>
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include "util.h"
#include <QDebug>

using namespace ogdf;


Model::Model(QObject *parent) : QObject(parent)
{
    m_graphNodes = {
        QVariant{QVariantList{ 100, 200 }},
        QVariant{QVariantList{ 300, 100 }}
    };
    m_graphEdges = {
        QVariant{QVariantList{ 100, 200, 300, 100, 10, 6 }}
    };

    connect(this, SIGNAL(internalBecameDirty()),
            this, SLOT(signalChanges()),
            Qt::QueuedConnection);
}

int Model::iterations() const
{
    return m_iterations;
}

int Model::thread() const
{
    return m_thread;
}

double Model::temperature() const
{
    return m_temperature;
}

double Model::energy() const
{
    return m_energy;
}

QVariantList Model::graphNodes()
{
    QMutexLocker synchronize(this);
    return m_graphNodes;
}

QVariantList Model::graphEdges()
{
    QMutexLocker synchronize(this);
    return m_graphEdges;
}

QVariantList Model::threads() const
{
    return m_threads;
}

QVariantList Model::threadMatrix() const
{
    return m_threadMatrix;
}

void Model::setIterations(int arg)
{
    if (m_iterations == arg)
        return;

    m_iterations = arg;
    markDirty();
}

void Model::setThread(int thread)
{
    if (m_thread == thread)
        return;

    m_thread = thread;
    markDirty();
}

void Model::setTemperature(double arg)
{
    if (m_temperature == arg)
        return;

    m_temperature = arg;
    markDirty();
}

void Model::setEnergy(double arg)
{
    if (m_energy == arg)
        return;

    m_energy = arg;
    markDirty();
}

void Model::setGraphNodes(QVariantList graphNodes)
{
    if (m_graphNodes == graphNodes)
        return;

    m_graphNodes = graphNodes;
    markDirty();
}

void Model::setGraphEdges(QVariantList graphEdges)
{
    if (m_graphEdges == graphEdges)
        return;

    m_graphEdges = graphEdges;
    markDirty();
}

int mystoi(std::string s) {
    try {
        return std::stoi(s);
    } catch(...) {
        return -1;
    }
}

void Model::setGraph(const ogdf::Graph &g, const ogdf::GraphAttributes &ga)
{
    QMutexLocker synchronize(this);

    m_graphNodes.clear();
    m_graphEdges.clear();
    {
        node v;
        forall_nodes(v, g) {
            m_graphNodes.push_back(QVariantList{
                                       v->index(),
                                       ga.x(v),
                                       ga.y(v),
                                       util::shapeToType(ga.shape(v)),
                                       ga.weight(v),
                                       mystoi(ga.label(v))
                                   });
        }
    }
    {
        edge e;
        forall_edges(e, g) {
            node s = e->source(), t = e->target();
            if (ga.label(e) != "") {
                m_graphEdges.push_back(QVariantList{
                                           ga.x(s),
                                           ga.y(s),
                                           ga.x(t),
                                           ga.y(t),
                                           std::stoi(ga.label(e)),  // chosen cable type, or -1 if this edge is a "cut"
                                           ga.intWeight(e),         // capacity of used cable
                                           ga.strokeWidth(e)        // flow through cable (used capacity)
                                       });
            }
        }
    }
    markDirty();
}

void Model::setThreads(QVariantList threads)
{
    if (m_threads == threads)
        return;

    m_threads = threads;
    //markDirty();
    changed();
}

void Model::setThreadMatrix(QVariantList threadMatrix)
{
    if (m_threadMatrix == threadMatrix)
        return;

    m_threadMatrix = threadMatrix;
    //markDirty();
    changed();
}

void Model::pushStats(qreal s)
{
    emit statsPushed(s);
}

void Model::markDirty()
{
    if (!dirty) {
        dirty = true;
        emit internalBecameDirty();
    }
}

void Model::signalChanges()
{
    if (dirty)
        changed();
    dirty = false;
}


