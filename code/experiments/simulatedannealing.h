#ifndef SIMULATEDANNEALING_H
#define SIMULATEDANNEALING_H

#include <utility>
#include <random>

template<
        class Representation,
        class Initialize,
        class Neighbor,
        class Evaluate,
        class Energy = double
>
class SimulatedAnnealing
{
    Initialize i;
    Neighbor n;
    Evaluate e;
    std::mt19937 &rand;

    Representation curr;
    Energy energy;
    double temperature;

public:
    SimulatedAnnealing(Initialize &&i, Neighbor &&n, Evaluate &&e, std::mt19937 &rand) :
        i(i), n(n), e(e), rand(rand)
    {}

    void init(double initialTemperature = 1) {
        curr = i(rand);
        energy = e(curr);
        temperature = initialTemperature;
    }
    std::tuple<Representation, Energy, double> step(double temperatureStepSize = 0.00001) {
        Representation next = n(rand, curr);
        Energy newEnergy = e(next);
        Energy relativeDelta = (newEnergy - energy) / (50000);
        double probability = relativeDelta < 0 ? 1.0 : exp(-relativeDelta / temperature);

        if (probability >= 1.0 || std::bernoulli_distribution(probability)(rand)) {
            curr = next;
            energy = newEnergy;
            //qDebug() << "       => accept";
        }

        temperature -= temperatureStepSize;
        return std::make_tuple(curr, energy, temperature);
    }

    std::tuple<Representation, Energy> operator()(double initialTemperature = 1, double temperatureStepSize = 0.00001) {
        init(initialTemperature);
        while(temperature > 0)
            step(temperatureStepSize);
        return std::make_tuple(curr, energy);
    }
};

template<
        class Representation,
        class Initialize,
        class Neighbor,
        class Evaluate,
        class Energy = double
>
SimulatedAnnealing<Representation, Initialize, Neighbor, Evaluate, Energy> makeSimulatedAnnealing(Initialize &&i, Neighbor &&n, Evaluate &&e, std::mt19937 &rand) {
    return { i, n, e, rand };
}

#endif // SIMULATEDANNEALING_H
