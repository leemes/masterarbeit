#ifndef LPSOLVERWRAPPER_H
#define LPSOLVERWRAPPER_H

#include <QRunnable>
#include <ogdf/basic/Array.h>
#include <ogdf/basic/NodeArray.h>
#include <ogdf/basic/GraphAttributes.h>
#include <QObject>
#include <QVector>
#include "cabletype.h"
#include "gurobi_c++.h"

class Model;

namespace ogdf {
class Graph;
}

class LPSolverWrapper : public QObject, public QRunnable
{
    Q_OBJECT

    ogdf::Graph &g;
    const ogdf::GraphAttributes &ga;
    ogdf::EdgeArray<std::vector<CableType> > cableTypesPerEdge;
    int maxtime;
    bool saveSolution;

    Model *m;

    ogdf::GraphAttributes gaOut;
    ogdf::Array<ogdf::node> turbines;
    ogdf::Array<ogdf::node> substations;
    ogdf::NodeArray<int> turbineIndex;
    ogdf::Array<ogdf::node> nodes;

    GRBModel *gurobiModel;

    // model variables / expressions also used in the render method
    vector<vector<GRBVar>> vars_f_ij;
    vector<vector<vector<GRBVar>>> vars_x_ijt;
    vector<GRBLinExpr> expr_excess_i;
    double gurobiUpper;
    double gurobiLower;
    double gurobiLastTime;

public:
    LPSolverWrapper(ogdf::Graph &g, const ogdf::GraphAttributes &ga, const ogdf::EdgeArray<std::vector<CableType> > &cableTypesPerEdge, int maxtime, bool saveSolution);

    void run();

    void setModel(Model *);

signals:
    void done();

private:
    // Initialize internal arrays
    void initArrays();
    bool isTurbine(ogdf::node v);

    bool updateLower(double time, double lower);
    bool updateUpper(double time, double upper);
    void printInfo();
    double updateOut();
    void renderIntoModel(Model *);


    class Callback : public GRBCallback {
        LPSolverWrapper *solver;
        double baseTime;
    public:
        Callback (LPSolverWrapper *solver, double baseTime) : solver(solver), baseTime(baseTime) {}
    private:
        void callback() override {
            switch (where) {
            case GRB_CB_MIP:
                if (solver->updateLower(baseTime + getDoubleInfo(GRB_CB_RUNTIME), getDoubleInfo(GRB_CB_MIP_OBJBND)))
                    abort();
                break;

            case GRB_CB_MIPSOL:
                if (solver->updateUpper(baseTime + getDoubleInfo(GRB_CB_RUNTIME), getDoubleInfo(GRB_CB_MIPSOL_OBJ)))
                    abort();
                break;

            default: return;
            }
        }
    };
};

#endif // LPSOLVERWRAPPER_H
