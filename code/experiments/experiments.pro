
# ============================== CONFIG ==============================

TEMPLATE = app
TARGET = experiments
QT += core quick
QMAKE_CXXFLAGS += -std=c++1y
QMAKE_CXXFLAGS_RELEASE += -march=native

# ============================= LIBRARIES ============================

# FN
INCLUDEPATH += ../fn

# OGDF
INCLUDEPATH += ../OGDF/include
LIBS += -L../OGDF/_release -lOGDF -lCOIN

# GUROBI
INCLUDEPATH += ../gurobi650/linux64/include
LIBS += -L../gurobi650/linux64/lib -lgurobi_c++ -lgurobi65
QMAKE_LFLAGS += -Wl,-rpath,../gurobi650/linux64/lib

# =============================== FILES ==============================

SOURCES += main.cpp \
    model.cpp \
    computation.cpp \
    util.cpp \
    cabletype.cpp \
    lpsolverwrapper.cpp \
    computationinstance.cpp \
    computationfixedassignment.cpp \
    generator.cpp

DISTFILES += \
    qml/view.qml \
    qml/Node.qml \
    qml/Edge.qml \
    qml/Plot.qml \
    qml/ThreadMatrix.qml

RESOURCES += \
    resources.qrc

HEADERS += \
    model.h \
    simulatedannealing.h \
    computation.h \
    util.h \
    cabletype.h \
    lpsolverwrapper.h \
    computationinstance.h \
    computationfixedassignment.h \
    generator.h
