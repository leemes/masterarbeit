#ifndef COMPUTATIONINSTANCE_H
#define COMPUTATIONINSTANCE_H

#include <QRunnable>
#include <ogdf/basic/Array.h>
#include <ogdf/basic/NodeArray.h>
#include <ogdf/basic/GraphAttributes.h>
#include <QVector>
#include <QMutex>
#include "cabletype.h"
#include "simulatedannealing.h"
#include "computation.h"

class Model;

namespace ogdf {
class Graph;
}

class ComputationInstance : public QRunnable, private QMutex
{
    ogdf::Graph g;
    ogdf::GraphAttributes ga;
    ogdf::EdgeArray<std::vector<CableType> > cableTypesPerEdge;
    std::mt19937 rand;

    Computation *master;
    ComputationParameters params;

    int m_id;

    Model *&m;
    ogdf::GraphAttributes gaOut;
    ogdf::Array<ogdf::node> substations;
    ogdf::Array<ogdf::node> turbines;
    ogdf::NodeArray<int> turbineIndex;
    ogdf::Array<ogdf::node> nodes;
    ogdf::EdgeArray<ogdf::edge> twinEdge;
    //ogdf::EdgeArray<std::vector<double>> attraction; // per edge and substation
    std::vector<int> nodeTable;         // for each node: first edge in other tables (here and in the Rep)
    std::vector<ogdf::edge> nodeEdges;  // for each edge: the edge object
    std::vector<int> nodeNeighbors;     // for each edge: adjacent node index
    int N, T;
    int swapsPerMutation = 1;

    // The simulated annealing algorithm
    struct Rep {
        ogdf::Array<int> map;
        ogdf::Array<bool> cuts;
        int numCuts;
        double cost;
        mutable std::vector<int> substReceived;
        bool penaltiesApplied;

        // Cache structures
        void computeTables(const ComputationInstance *that);
        std::vector<ogdf::edge> nodeEdgeQueue;  // for each edge: edge index, sorted by priorities within adjacency list of each node
    };

    // The current state
    double temp;
    Rep current;
    Rep currentBest;
    double currentEnergy;
    double currentBestEnergy;
    QVector<int> currentAssignment;
    QVector<int> currentBestAssignment;
    double activityEMA = 1.0;
    QAtomicInt killed = 0;
    QAtomicInt running = 0;
    int iterations = 0;
    int iterationsSinceLastImprovement = 0;

    // Rendering
    QAtomicInt toBeRendered = 0;
    int iterationsSinceLastRender = 0;

public:
    ComputationInstance(Model *&model, const ogdf::Graph &g, const ogdf::GraphAttributes &ga,
                        const ogdf::EdgeArray<std::vector<CableType> > & cableTypesPerEdge,
                        int seed, Computation *master, ComputationParameters params,
                        const Rep *initialRep = 0);

    int id() const;

    void run();
    bool isRunning() const;
    void kill();
    bool wasKilled() const;
    ComputationInstance *clone(int reseed) const;

    int numberOfIterations() const;
    double energy() const;
    double temperature() const;
    double activity() const;
    QVector<int> assignment() const;
    bool mature() const;
    bool penaltiesApplied() const;

    void renderIntoModel(); // function returns non-blocking; rendering happens asyncly after the next simulation step
    void saveSolution(QString filename);

    void manualAction();

    static double pairwiseDifference(const ComputationInstance *, const ComputationInstance *);
    static double pairwiseCompatibility(const ComputationInstance *, const ComputationInstance *);
    static std::pair<ComputationInstance*,ComputationInstance*> crossOver(const ComputationInstance *, const ComputationInstance *, int seed, double tempFactor);

private:
    // Initialize internal arrays
    void initArrays();

    void maybeRender();
    void renderBest();
    void renderCurrent();
    void render(const Rep &rep, double energy, double temp);
    ogdf::edge findEdgeCut(const Rep &rep, bool cut);
    void setEdgeCut(Rep &rep, ogdf::edge e, bool cut) const;

    // Determines if the given node is a possible target node
    bool isTarget(ogdf::node) const;

    // Find node in the list of substations and return its index (0...S-1)
    int substationIndex(ogdf::node) const;

    // Find the node with the maximum priority in the unvisited neighborhood of node n (may return null!)
    ogdf::edge findBestEdge(const Rep &rep, ogdf::node v, const std::vector<bool> &visited) const;

    // Decode path for turbine t (turbine index) using the priority map
    bool findPath(const Rep &rep, int t, std::vector<ogdf::edge> &outPath, std::vector<bool> &visited) const;

    // Compute the flow network according to the representation
    bool computeFlow(const Rep &rep, ogdf::EdgeArray<double> &flowOnEdge, ogdf::NodeArray<double> &sinkReceived) const;
    bool computeSinkForEachNode(const Rep &rep, ogdf::NodeArray<ogdf::node> &sinkOfNode) const;
    QVector<int> sinkIndicesForNodes(const Rep &rep) const;

    static double pairwiseSinkDiffs(const ComputationInstance *a, const ComputationInstance *b, std::vector<int> *substationPartition);

    // Initial state:
    Rep saInit(std::mt19937 &rand);

    // Move to a neighbor state:
    Rep saNeighbor(std::mt19937 &rand, const Rep &curr);

    // Evaluation (decoding the representation and computing the cost / energy of the result):
    double saEvaluate(const Rep &map);

};

#endif // COMPUTATIONINSTANCE_H
